﻿using System;
using System.Collections.Generic;
using System.Text;
using NORM.Entity;

namespace NORM.Common
{
    internal class ArrayPlus
    {
        public static List<T> ToList<T>(T[] source)
        {
            List<T> list = new List<T>();
            if (source != null)
            {
                foreach (T t in source)
                {
                    list.Add(t);
                }
            }
            return list;
        }

        public static bool Contains<T>(T[] source, T t1)
        {
            bool rvl = false;
            foreach (T t in source)
            {
                if (t.Equals(t1))
                {
                    rvl = true;
                    break;
                }
            }
            return rvl;
        }

        public static List<string> ToStringList(object[] source)
        {
            List<string> rvl = new List<string>();
            foreach (object o in source)
            {
                rvl.Add(o + "");
            }
            return rvl;
        }

        public static bool ContainsTableFieldKey(TableField[] source, string key)
        {
            bool rvl = false;
            foreach (TableField tf in source)
            {
                if (key.Equals(tf.Name))
                {
                    rvl = true;
                    return rvl;
                }
            }
            return rvl;
        }

    }
}
