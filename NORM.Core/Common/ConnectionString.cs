﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NORM.Common
{
    [Serializable]
    public class ConnectionString
    {
        public string Server { get; set; }
        public string Port { get; set; }
        public string DataBaseName { get; set; }
        public string ProviderName { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
        public string Instancename { get; set; }
        public string Encoding { get; set; }
        public string Version { get; set; }
        public string UseUTF16Encoding { get; set; }


        private string _server;
        private string _port;
        private string _database_name;
        private string _provider_name;
        private string _instancename;
        private string _userid;
        private string _password;
        private string _version;
        private string _encoding;
        private string _useUTF16Encoding;
       

        public override string ToString()
        {
            return connectionString;
        }

        public ConnectionString()
            : this(NORM.DataBase.DataBaseTypes.SqlDataBase)
        {

        }

        public ConnectionString(NORM.DataBase.DataBaseTypes databaseType)
        {
            switch (databaseType)
            {
                case DataBase.DataBaseTypes.SqlDataBase:

                    this._server = "Server=";
                    this._port = ",";
                    this._database_name = "Initial Catalog=";
                    this._userid = "User ID=";
                    this._password = "Pwd=";

                    break;

                case DataBase.DataBaseTypes.SqliteDataBase:

                    this._server = "Data Source=";
                    this._password = "Password=";
                    this._version = "Version=";
                    this._useUTF16Encoding = "UseUTF16Encoding=";

                    break;

                case DataBase.DataBaseTypes.PostgreSqlDataBase:

                    this._server = "Server=";
                    this._port = ";Port=";
                    this._database_name = "Database=";
                    this._userid = "uid=";
                    this._password = "pwd=";
                    this._encoding = "Encoding=";

                    break;

                case DataBase.DataBaseTypes.MySqlDataBase:

                    this._server = "Server=";
                    this._port = ";Port=";
                    this._database_name = "Database=";
                    this._userid = "uid=";
                    this._password = "pwd=";                    

                    break;
            }
        }

        public ConnectionString(string connString)
        {
            this.connectionString = connString;
        }

        public string connectionString
        {
            get
            {
                //string format = _server + "{0};" +                     
                //                _database_name + "{1};" +
                //                _userid + "{2};" +
                //                _password + "{3};";

                //string value = string.Format(format, (string.IsNullOrEmpty(Port) ? Server : Server + _port + Port),
                //    DataBaseName, UserId, Password);

                string value = "" +
                    _server + (string.IsNullOrEmpty(Port) ?
                    Server : Server + _port + Port);
                if (!string.IsNullOrEmpty(Version))
                    value += ";" + _version + Version;
                if (!string.IsNullOrEmpty(DataBaseName))
                    value +=";"+ _database_name + DataBaseName;
                if (!string.IsNullOrEmpty(UserId))
                    value += ";" + _userid + UserId;
                if (!string.IsNullOrEmpty(Password))
                    value += ";" + _password + Password;
                if (!string.IsNullOrEmpty(Encoding))
                    value += ";" + _encoding + Encoding;
                if (!string.IsNullOrEmpty(UseUTF16Encoding))
                    value += ";" + _useUTF16Encoding + UseUTF16Encoding;
                value += ";";

                return value;
            }
            set
            {
                string _connectionString = value;
                if (!string.IsNullOrEmpty(_connectionString))
                {
                    string[] stringArray = _connectionString.Split(';');
                    foreach (string stringString in stringArray)
                    {
                        if (!string.IsNullOrEmpty(stringString))
                        {
                            if (stringString.StartsWith("Server="))
                            {
                                this.Server = stringString.Substring(stringString.LastIndexOf("Server=") + "Server=".Length);
                                this._server = "Server=";
                                
                                if (this.Server.Contains(","))
                                {
                                    this.Port = this.Server.Substring(this.Server.LastIndexOf(",") + 1);
                                    this.Server = this.Server.Substring(0, this.Server.LastIndexOf(","));

                                    this._port = ",";
                                }

                            }
                            else if (stringString.StartsWith("Data Source="))
                            {
                                this.Server = stringString.Substring(stringString.LastIndexOf("Data Source=") + "Data Source=".Length);
                                this._server = "Data Source=";
                            }
                            else if (stringString.StartsWith("Version="))
                            {
                                this.Version = stringString.Substring(stringString.IndexOf("=") + 1);
                                this._version = "Version=";
                            }
                            else if (stringString.StartsWith("Port="))
                            {
                                this.Port = stringString.Substring(stringString.LastIndexOf("Port=") + "Port=".Length);
                                this._port = ";Port=";
                            }
                            else if (stringString.StartsWith("Initial Catalog="))
                            {
                                this.DataBaseName = stringString.Substring(stringString.LastIndexOf("Initial Catalog=") + "Initial Catalog=".Length);
                                this._database_name = "Initial Catalog=";
                            }
                            else if (stringString.StartsWith("Database=", StringComparison.OrdinalIgnoreCase))
                            {
                                this.DataBaseName = stringString.Substring(stringString.LastIndexOf("=") + 1);
                                this._database_name = "Database=";
                            }
                            else if (stringString.StartsWith("User ID="))
                            {
                                this.UserId = stringString.Substring(stringString.LastIndexOf("User ID=") + "User ID=".Length);
                                this._userid = "User ID=";
                            }
                            else if (stringString.StartsWith("uid=", StringComparison.OrdinalIgnoreCase))
                            {
                                this.UserId = stringString.Substring(stringString.LastIndexOf("=") + 1);
                                this._userid = "uid=";
                            }
                            else if (stringString.StartsWith("Pwd=", StringComparison.OrdinalIgnoreCase))
                            {
                                this.Password = stringString.Substring(stringString.IndexOf("=") + 1);
                                this._password = "Pwd=";
                            }
                            else if (stringString.StartsWith("Password=", StringComparison.OrdinalIgnoreCase))
                            {
                                this.Password = stringString.Substring(stringString.IndexOf("=") + 1);
                                this._password = "Password=";
                            }
                            else if (stringString.StartsWith("Encoding="))
                            {
                                this.Encoding = stringString.Substring(stringString.LastIndexOf("Encoding=") + "Encoding=".Length);
                                this._encoding = "Encoding=";
                            }
                            else if (stringString.StartsWith("UseUTF16Encoding="))
                            {
                                this.UseUTF16Encoding = "True";
                                this._useUTF16Encoding = "UseUTF16Encoding=";
                            }

                        }
                    }
                }
            }
        }

    }
}
