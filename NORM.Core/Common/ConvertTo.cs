﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Reflection;
using System.Text;

namespace NORM.Common
{
    /// <summary>
    /// 描述：转换辅助
    /// 作者：王晓东
    /// 更新时间：2016-08-08
    /// </summary>
    internal class ConvertTo
    {
        private static PropertyInfo GetProperty(string name,Type type)
        {
            //return CacheProperties.cacheInstance.GetProperty(type, name);
            return type.GetProperty(name);  //BindingFlags.Instance | BindingFlags.Public        
        }

        private static void SetPropertyValue<T>(PropertyInfo pi, T t,object value)
        {
            Type type = pi.PropertyType;
            if (value != null && value != DBNull.Value)
            {
                switch (type.Name)
                {
                    case "Guid": pi.SetValue(t, (Guid)value, null); break;
                    case "Int16": pi.SetValue(t, Int16.Parse(value.ToString()), null); break;
                    case "Int32": pi.SetValue(t, Int32.Parse(value.ToString()), null); break;
                    case "Int64": pi.SetValue(t, Int64.Parse(value.ToString()), null); break;
                    case "Decimal": pi.SetValue(t, Convert.ToDecimal(value), null); break;
                    case "Boolean": pi.SetValue(t, Convert.ToBoolean(value), null); break;
                    case "Double": pi.SetValue(t, Convert.ToDouble(value), null); break;
                    case "Single": pi.SetValue(t, Single.Parse(value.ToString()), null); break;
                    case "Byte[]": pi.SetValue(t, (Byte[])value, null); break;
                    case "String": pi.SetValue(t, value + "", null); break;
                    case "Char": pi.SetValue(t, Char.Parse(value.ToString()), null); break;
                    case "Nullable`1":
                        try
                        {
                            pi.SetValue(t, value, null);
                        }
                        catch (Exception ex)
                        {
                            string fultypename = type.FullName;
                            if (fultypename.IndexOf("System.Decimal") > 0)
                            {
                                NullableConverter converter = new NullableConverter(typeof(Decimal?));
                                pi.SetValue(t, (Decimal?)converter.ConvertFromString(value.ToString()), null);
                            }
                        }
                        break;
                    default: pi.SetValue(t, value, null); break;
                }
            }
        }

        /// <summary>
        /// 表转换成泛型集合
        /// </summary>
        /// <typeparam name="T">类型</typeparam>
        /// <param name="Source">数据源</param>
        /// <returns></returns>
        public static List<T> ConvertToList<T>(System.Data.DataTable Source)
        {
            List<T> dlist = new List<T>();
            Type type = typeof(T);

            //FieldInfo[] fields = type.GetFields();
            Dictionary<string, PropertyInfo> PropertiesInfoDis = new Dictionary<string, PropertyInfo>();
            PropertyInfo[] PropertiesInfo = type.GetProperties();
            foreach (var pi in PropertiesInfo)
            {
                PropertiesInfoDis.Add(pi.Name, pi);
            }

            foreach (System.Data.DataRow dr in Source.Rows)
            {
                T O = (T)Activator.CreateInstance(typeof(T));// new T();
                foreach (System.Data.DataColumn dc in Source.Columns)
                {
                    string columnName = dc.ColumnName;
                    columnName = columnName.Replace("[", "").Replace("]", "").Trim();

                    PropertyInfo pi = null;
                     if (PropertiesInfoDis.ContainsKey(columnName))
                         pi = PropertiesInfoDis[columnName]; //type.GetProperty(columnName);

                    if (pi == null)
                        continue;                    

                    var customAttributes = pi.GetCustomAttributes(true);
                    if (customAttributes.Length > 0)
                    {
                        var d_value = dr[dc.ColumnName];
                        foreach (var attribute in customAttributes)
                        {
                            if (attribute.ToString().StartsWith("Format::"))
                            {
                                string attrdescrib = attribute.ToString();
                                attrdescrib = attrdescrib.Substring(attrdescrib.IndexOf("Format::") + "Format::".Length);
                                string typeName = attrdescrib.Split(',')[0];
                                string formatting = attrdescrib.Split(',')[1];
                                string value =string.Empty;
                                switch (typeName)
                                {
                                    case "DateTime":
                                        value = Convert.ToDateTime(d_value).ToString(formatting);
                                        break;
                                    case "Int": 
                                        value = Convert.ToInt32(d_value).ToString(formatting);
                                        break;
                                    case "Int32":
                                        value = Convert.ToInt32(d_value).ToString(formatting);
                                        break;
                                    case "Int64":
                                        value = Convert.ToInt64(d_value).ToString(formatting);
                                        break;
                                    case "Double": 
                                        value = Convert.ToDouble(d_value).ToString(formatting);
                                        break;
                                    case "Decimal":  
                                        value = Convert.ToDecimal(d_value).ToString(formatting);
                                        break;
                                }
                                SetPropertyValue(pi, O, value);
                            }
                        }
                        SetPropertyValue(pi, O, d_value);
                    }
                    else
                    {
                        var d_value = dr[dc.ColumnName];
                        SetPropertyValue(pi, O, d_value);
                    }                  

                }
                dlist.Add(O);
            }
            return dlist;
        }

        /// <summary>
        /// 表转换成泛型集合
        /// </summary>
        /// <typeparam name="T">类型</typeparam>
        /// <param name="Source">数据源</param>
        /// <param name="InitItem">初始化项</param>
        /// <returns></returns>
        public static List<T> ConvertToList<T>(System.Data.DataTable Source, T InitItem)
        {
            List<T> dlist = new List<T>();
            dlist.Add(InitItem);

            Type type = typeof(T);
            foreach (System.Data.DataRow dr in Source.Rows)
            {
                T O = (T)Activator.CreateInstance(typeof(T));// new T();
                foreach (System.Data.DataColumn dc in Source.Columns)
                {
                    type.GetProperty(dc.ColumnName).SetValue(O, dr[dc.ColumnName], null);
                }
                dlist.Add(O);
            }
            return dlist;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="Source"></param>
        /// <returns></returns>
        public static List<T> ConvertToList<T>(System.Data.Common.DbDataReader Source)
        {
            List<T> dlist = new List<T>();

            Type type = typeof(T);

            #region 之前代码

            //System.Data.DataTable dt = Source.GetSchemaTable();
            //while (Source.Read())
            //{
            //    T O = Activator.CreateInstance<T>();
            //    foreach (System.Data.DataRow dr in dt.Rows)
            //    {
            //        string columnName = dr["ColumnName"] + "";
            //        string columnType = dr["DataType"] + "";
            //        var pi = _type.GetProperty(columnName);
            //        if (pi != null)
            //        {
            //            var d_value = Source[columnName];
            //            if (d_value != null && d_value != DBNull.Value)
            //            {
            //                switch (columnType)
            //                {
            //                    case "System.Guid": pi.SetValue(O, (Guid)d_value, null); break;
            //                    case "System.Int16": pi.SetValue(O, Int16.Parse(d_value.ToString()), null); break;
            //                    case "System.Int32": pi.SetValue(O, Int32.Parse(d_value.ToString()), null); break;
            //                    case "System.Int64": pi.SetValue(O, Int64.Parse(d_value.ToString()), null); break;
            //                    case "System.Decimal": pi.SetValue(O, Decimal.Parse(d_value.ToString()), null); break;
            //                    case "System.Boolean": pi.SetValue(O, Boolean.Parse(d_value.ToString()), null); break;
            //                    case "System.Double": pi.SetValue(O, Double.Parse(d_value.ToString()), null); break;
            //                    case "Byte[]": pi.SetValue(O, (Byte[])d_value, null); break;
            //                    case "System.Byte[]": pi.SetValue(O, (Byte[])d_value, null); break;
            //                    case "System.String": pi.SetValue(O, d_value + "", null); break;
            //                    case "System.Char": pi.SetValue(O, Char.Parse(d_value.ToString()), null); break;
            //                    default: pi.SetValue(O, d_value, null); break;
            //                }
            //            }
            //        }
            //    }
            //    dlist.Add(O);
            //}

            //dt.Dispose(); 

            #endregion
            
            #region 最新代码

            PropertyInfo[] PropertiesInfo = type.GetProperties();
            Dictionary<string, PropertyInfo> PropertiesInfoDis = new Dictionary<string, PropertyInfo>();
            foreach (var pi in PropertiesInfo)
            {
                PropertiesInfoDis.Add(pi.Name, pi);
            }

            while (Source.Read())
            {

                T O = Activator.CreateInstance<T>();

                for (int i = 0; i < Source.FieldCount; i++)
                {
                    string columnName = Source.GetName(i);
                    string columnType = Source.GetFieldType(i).Name;

                    PropertyInfo pi = null;
                    if (PropertiesInfoDis.ContainsKey(columnName))
                        pi = PropertiesInfoDis[columnName]; //GetProperty(columnName, _type);

                    if (pi != null)
                    {
                        var d_type = pi.PropertyType;
                        var d_value = Source[columnName];
                        if (d_value != null && d_value != DBNull.Value)
                        {
                            switch (d_type.Name)
                            {
                                case "Guid": pi.SetValue(O, (Guid)d_value, null); break;
                                case "Int16": pi.SetValue(O, Int16.Parse(d_value.ToString()), null); break;
                                case "Int32": pi.SetValue(O, Int32.Parse(d_value.ToString()), null); break;
                                case "Int64": pi.SetValue(O, Int64.Parse(d_value.ToString()), null); break;
                                case "Decimal": pi.SetValue(O, Decimal.Parse(d_value.ToString()), null); break;
                                case "Boolean": pi.SetValue(O, Boolean.Parse(d_value.ToString()), null); break;
                                case "Double": pi.SetValue(O, Double.Parse(d_value.ToString()), null); break;
                                case "Single": pi.SetValue(O, Single.Parse(d_value.ToString()), null); break;
                                case "Byte[]": pi.SetValue(O, (Byte[])d_value, null); break;
                                case "String": pi.SetValue(O, d_value + "", null); break;
                                case "Char": pi.SetValue(O, Char.Parse(d_value.ToString()), null); break;
                                case "Nullable`1":
                                    try
                                    {
                                        pi.SetValue(O, d_value, null);
                                    }
                                    catch (Exception ex)
                                    {
                                        string fultypename = d_type.FullName;
                                        if (fultypename.IndexOf("System.Decimal") > 0)
                                        {
                                            NullableConverter converter = new NullableConverter(typeof(Decimal?));
                                            pi.SetValue(O, (Decimal?)converter.ConvertFromString(d_value.ToString()), null);
                                        }
                                    }
                                    break;
                                default: pi.SetValue(O, d_value, null); break;
                            }
                        }
                    }

                }

                dlist.Add(O);

            }

            PropertiesInfoDis.Clear();
            PropertiesInfo = null;

            #endregion

            return dlist;
        } 

    }     

    public class DTLConvert<T>
    {
        public static List<T> ToList(System.Data.DataTable dt)
        {
            return ConvertTo.ConvertToList<T>(dt);
        }
    }
}
