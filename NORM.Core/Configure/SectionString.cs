﻿using System;
using System.Data;
using System.Text;
using System.Collections.Generic;

namespace NORM.Configure
{
    /// <summary>
    /// 连接字符串配置节
    /// </summary>
    public class SectionString
    {
        public string Name { set; get; }
        public string Value { set; get; }
        public string Key { set; get; }

        private SectionString()
        {
            this.Name = string.Empty;
            this.Value = string.Empty;
            this.Key = string.Empty;
        }

        public SectionString(string SectionString)
            : this()
        {
            if (SectionString.Contains("="))
            {
                string[] values = SectionString.Split('=');
                this.Key = this.Name = values[0];
                if (values.Length > 2)
                    this.Value = values[1] + "=";
                else
                    this.Value = values[1];
            }
        }

        public override string ToString()
        {
            return this.Name + "=" + this.Value;
        }

    }    

}
