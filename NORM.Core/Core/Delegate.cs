﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NORM
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TResult">返回类型</typeparam>
    /// <returns></returns>
    public delegate TResult Func<TResult>();

    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="T"></typeparam>
    /// <typeparam name="TResult">返回类型</typeparam>
    /// <param name="arg">参数</param>
    /// <returns></returns>
    public delegate TResult Func<T, TResult>(T arg);

}
