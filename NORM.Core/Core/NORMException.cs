﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NORM
{
    public class NORMException : Exception
    {
        public NORMException(string ExceptionType, string Message)
            : base(string.Format("NORM提示: \"{0}错误: {1}\";", (object)ExceptionType, (object)Message))
        {
            //if (ExceptionType.Equals("OQLExceptin"))
            //    NORM.Logger.LoggerHelper.Debug(Message, (object)ExceptionType + "错误");
        }

        public NORMException(ExceptionType ExceptionType, string Message)
            : base(string.Format("NORM提示: \"{0}错误: {1}\";", (object)NORMException.ExceptionTypeToString(ExceptionType), (object)Message))
        {
            //if (ExceptionType.Equals(ExceptionType.OQLExceptin))
            //    NORM.Logger.LoggerHelper.Debug(Message, (object)ExceptionType + "错误");
        }

        private static string ExceptionTypeToString(ExceptionType ExceptionType)
        {
            string str = string.Empty;
            switch (ExceptionType)
            {
                case ExceptionType.Default:
                    str += "系统";
                    break;
                case ExceptionType.DataBaseExceptoin:
                    str += "数据库操作";
                    break;
                case ExceptionType.OQLExceptin:
                    str += "OQL语法";
                    break;
                case ExceptionType.ConfigException:
                    str += "获取配置";
                    break;
            }
            return str;
        }
    }
}
