﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NORM
{
    internal class LockItem
    {               
        public object Value { get; set; }
    }

    internal class Locker
    {
        private static LockItem lockone = new LockItem();
        private static object _instance = null;
        public static object instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (lockone)
                    {
                        if (_instance == null)
                        {
                            _instance = new object();
                        }
                    }
                }
                return _instance;
            }
        }
    }
}
