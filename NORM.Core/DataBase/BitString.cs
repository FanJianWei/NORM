﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NORM.DataBase
{
    internal class BitString
    {
        private bool _bl = false;
        public BitString(bool bl)
        {
            this._bl = bl;
        }
        public override string ToString()
        {
            string rvl = "0";
            if (this._bl)
                rvl = "1";
            return rvl;
        }

    }
}
