﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NORM.DataBase
{
    public class DataBaseContext
    {
        private static DataBaseContext _instance = null;
        private static object lockobject = new object();

        public static DataBaseContext Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (lockobject)
                    {
                        if (_instance == null)
                        {
                            _instance = new DataBaseContext();
                        }
                    }
                }
                //_instance._dba.sqlstatementclear();
                return _instance;
            }
        }
               
        private System.Data.DataTable QueryTable(string sql, DataBase db)
        {
            return db.QueryTable(sql, null);
        }

        private System.Data.DataTable QueryTable(string sql, OsqlParameter[] parameters, DataBase db)
        {
            return db.QueryTable(sql, parameters);
        }        

        public bool Excute(string sql, DataBase db)
        {
            return db.Execute(System.Data.CommandType.Text, sql, null) > 0 ? true : false;
        }

        public bool Excute(string sql, OsqlParameter[] parameters, DataBase db)
        {
            return db.Execute(System.Data.CommandType.Text, sql, parameters) > 0 ? true : false;
        }       

        public System.Data.DataTable QueryToTable(string sql, DataBase db)
        {
            return Instance.QueryTable(sql, db);
        }

        public System.Data.DataTable QueryToTable(string sql, OsqlParameter[] parameters, DataBase db)
        {
            return Instance.QueryTable(sql, parameters, db);
        }       

        public IEnumerable<T> Query<T>(string sql, DataBase db) where T : class
        {
            var dt = Instance.QueryTable(sql, db);
            return NORM.Common.ConvertTo.ConvertToList<T>(dt);
        }

        public IEnumerable<T> Query<T>(string sql, OsqlParameter[] parameters, DataBase db) where T : class
        {
            var dt = Instance.QueryTable(sql, parameters, db);
            return NORM.Common.ConvertTo.ConvertToList<T>(dt);
        }
    }
}
