﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NORM.DataBase
{
    /// <summary>
    /// DataBase 实例创建工厂
    /// </summary>
    public class DataBaseFactory
    {
        private static DataBase _db = null;
        private static object lockobj = new object();

        /// <summary>
        /// 创建 DataBase 实例
        /// </summary>
        /// <param name="connectionString">连接字符串</param>
        /// <param name="dbType">数据库类型枚举</param>
        /// <returns>DataBase 实例</returns>
        public static DataBase Create(string connectionString, DataBaseTypes dbType)
        {
            DataBase db = null;            
            switch (dbType)
            {
                case DataBaseTypes.SqlDataBase:
                    db = new SqlDataBase(connectionString);
                    break;
                case DataBaseTypes.SqliteDataBase:
                    db = new SQLiteDataBase(connectionString);
                    break;
                case DataBaseTypes.PostgreSqlDataBase:
                    db = new PostgreSqlDataBase(connectionString);
                    break;
                case DataBaseTypes.MySqlDataBase:
                    db = new MySqlDataBase(connectionString);
                    break;
                default:
                    db = new SQLiteDataBase(connectionString);
                    break;
            }
            return db;
        }

        /// <summary>
        /// 创建 DataBase 实例
        /// </summary>
        /// <param name="connectionString">连接字符串</param>
        /// <param name="providerName">数据库类型 如：System.Data.Sql </param>
        /// <returns></returns>
        public static DataBase Create(string connectionString,string providerName)
        {
            DataBase db = null;
            switch (providerName)
            {
                case "System.Data.Sql":
                    db = new SqlDataBase(connectionString);
                    break;
                case "System.Data.Sqlite":
                    db = new SQLiteDataBase(connectionString);
                    break;
                case "System.Data.PostgreSql":
                    db = new PostgreSqlDataBase(connectionString);
                    break;
                case "System.Data.MySql":
                    db = new MySqlDataBase(connectionString);
                    break;
                default:
                    db = new SQLiteDataBase(connectionString);
                    break;
            }
            return db;
        }

        /// <summary>
        /// 创建 DataBase 实例
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        public static DataBase Create(string name)
        {
            DataBase db = null;
            string providerName = System.Configuration.ConfigurationManager.ConnectionStrings[name].ProviderName;
            string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings[name].ConnectionString;

            switch (providerName)
            {
                case "System.Data.SQL":
                    db = new SqlDataBase(connectionString);
                    break;
                case "System.Data.SQLite":
                    db = new SQLiteDataBase(connectionString);
                    break;
                case "System.Data.PostgreSql":
                    db = new PostgreSqlDataBase(connectionString);
                    break;
                case "System.Data.MySql":
                    db = new MySqlDataBase(connectionString);
                    break;   
                default:
                    db = new SQLiteDataBase(connectionString);
                    break;
            }

            return db;
        }

        /// <summary>
        /// 创建 DataBase 实例
        /// </summary>
        /// <returns></returns>
        public static DataBase Create()
        {
            DataBase db = null;
            string providerName = string.Empty;            
            string connectionString = string.Empty;

            #region 读取配置信息
            try
            {
                providerName = System.Configuration.ConfigurationManager.ConnectionStrings["default"].ProviderName;
                connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["default"].ConnectionString;
            }
            catch (Exception ex)
            {
                string appath = AppDomain.CurrentDomain.BaseDirectory + "ConnectionSettings.config";
                if (!System.IO.File.Exists(appath))
                {
                    System.IO.DirectoryInfo dinfo = new System.IO.DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);
                    appath = dinfo.Parent.FullName + "\\ConnectionSettings.config";
                }
                if (!System.IO.File.Exists(appath))
                {
                     throw new Exception("未获取到配置文件中的相关配置");
                }
                try
                {
                    int recordCount = 0;
                    System.Xml.XmlDocument xmldoc = new System.Xml.XmlDocument();
                    xmldoc.Load(appath);
                    System.Xml.XmlNodeList nodes = xmldoc.SelectSingleNode("//connectionStrings").ChildNodes;
                    foreach (System.Xml.XmlNode node in nodes)
                    {
                        if (node.Name == "add" && node.Attributes["name"].Value.Equals("default"))
                        {
                            recordCount++;
                            connectionString = node.Attributes["connectionString"].Value;
                            providerName = node.Attributes["providerName"].Value;
                            break;
                        }
                    }
                    if (recordCount < 1)
                    {
                        throw new Exception("未获取到配置文件中的相关配置");
                    }
                }
                catch (Exception ee)
                {
                    throw new Exception("未获取到配置文件中的相关配置");
                }
            }
            #endregion

            switch (providerName)
            {
                case "System.Data.SQL":
                    db = new SqlDataBase(connectionString);
                    break;
                case "System.Data.SQLite":
                    db = new SQLiteDataBase(connectionString);
                    break;
                case "System.Data.PostgreSql":
                    db = new PostgreSqlDataBase(connectionString);
                    break;
                case "System.Data.MySql":
                    db = new MySqlDataBase(connectionString);
                    break;  
                default:
                    db = new SQLiteDataBase(connectionString);
                    break;
            }

            return db;
        }

        /// <summary>
        /// 获取配置文件 WebConfig / AppConfig 里 default 默认配置连接字符串
        /// </summary>
        public static DataBase Default
        {
            get
            {
                if (_db == null)
                {                    
                    lock (lockobj)
                    {
                        if (_db == null)
                        {
                            _db = Create();
                        }
                    }
                }
                return _db;
            }
        }
    
    }
}
