﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NORM.DataBase
{
    public enum DataBaseSchema
    {
        DataBase=0,
        Table=1,
        View=2,
        Column=3,
        TableAndView=4
    }
}
