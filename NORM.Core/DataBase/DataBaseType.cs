﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NORM.DataBase
{
    public enum DataBaseTypes
    {
        SqlDataBase,
        SqliteDataBase,
        PostgreSqlDataBase,
        MySqlDataBase
    }
}
