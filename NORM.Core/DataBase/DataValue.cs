﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NORM.SQLObject
{
    public class DataValue
    {
        public static object DBNull { get { return "NULL"; } }
        public static object StringEmpty { get { return string.Empty; } }
        public static object NewGuid { get { return Guid.NewGuid(); } }
        public static object GuidEmpty { get { return Guid.Empty; } }
    }
}
