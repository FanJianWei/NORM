﻿using NORM.Common;
using NORM.SQLObject;
using System;
using System.Collections.Generic;
using System.Text;

namespace NORM.DataBase
{
    public interface IDataBase
    {
        /// <summary>
        /// 开启事务
        /// </summary>
        void BeginTransaction();

        /// <summary>
        /// 提交事务
        /// </summary>
        /// <returns></returns>
        int Commit();

        /// <summary>
        /// 事务回滚
        /// </summary>
        void RollBack();

        /// <summary>
        /// 事务是否完成
        /// </summary>
        /// <returns></returns>
        bool TransCompleteState();


        /// <summary>
        /// 设置执行命令
        /// </summary>
        /// <param name="oql"></param>
        void SetCommand(OQL oql);

        /// <summary>
        /// 测试连接
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        bool TestConnect(out string message);
        
    } 
}
