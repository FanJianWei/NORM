﻿
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Text;

namespace NORM.DataBase
{
    public class OsqlCommand
    {
        private string _commandtext;
        internal string CommandText
        {
            set { _commandtext = value; }
            get { return _commandtext; }
        }

        private DbParameter[] _parameters;

        internal DbParameter[] Parameters
        {
            set { _parameters = value; }
            get { return _parameters; }
        }

    }
}
