﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace NORM.DataBase
{
    public class OsqlParameter 
    {
        public string Name { set; get; }
        public OsqlParameterDataType DataType { set; get; }
        public object Value { set; get; }
        public int Size { set; get; }
        public ParameterDirection Direction { set; get; }

        public OsqlParameter()
        {
            this.Size = -1;
            this.Direction = ParameterDirection.Input;
            this.DataType = OsqlParameterDataType.String;
        }

        public OsqlParameter(string Name, object Value)
            : this()
        {
            this.Name = Name;
            this.Value = Value;
            if (Value != null)
            {
                Type _valueType = Value.GetType();
                switch (_valueType.Name)
                {
                    case "Int16":
                        this.DataType = OsqlParameterDataType.Int;
                        this.Size = 4;
                        break;
                    case "Int32":
                    case "Int64":
                        this.DataType = OsqlParameterDataType.Int;
                        break;
                    case "String":
                        this.DataType = OsqlParameterDataType.String;
                        break;
                    case "Char":
                        this.DataType = OsqlParameterDataType.Char;
                        this.Size = 1;
                        break;                   
                    case "Decimal":
                        this.DataType = OsqlParameterDataType.Decimal;
                        break;
                    case "double":
                    case "Double":
                        this.DataType = OsqlParameterDataType.Double;
                        break;
                    case "float":
                    case "Float":
                        this.DataType = OsqlParameterDataType.Numeric;
                        break;
                    case "DateTime":
                        this.DataType = OsqlParameterDataType.DateTime;
                        break;
                    case "Boolean":
                        this.DataType = OsqlParameterDataType.Bool;
                        break;
                    case "Guid":
                        this.DataType = OsqlParameterDataType.Guid;
                        break;
                    case "Byte[]":
                        this.DataType = OsqlParameterDataType.Byte;
                        break;
                }
            }
            
        }        

        public OsqlParameter(string Name, OsqlParameterDataType DataType, object Value)
            : this()
        {
            this.Name = Name;
            this.Value = Value;
            this.DataType = DataType;
        }

    }

    internal class OOsqlParameter : System.Data.Common.DbParameter
    {
        public override DbType DbType { get; set; }
        public override ParameterDirection Direction { get; set; }
        public override bool IsNullable { get; set; }
        public override object Value { get; set; }
        public override string ParameterName { get; set; }
        public override int Size { get; set; }
        public override DataRowVersion SourceVersion { get; set; }
        public override bool SourceColumnNullMapping { get; set; }
        public override string SourceColumn { get; set; }
        public override void ResetDbType()
        {

        }

        public OOsqlParameter(string Name, object Value)
        {
            this.ParameterName = Name;
            this.Value = Value;
            this.Direction = ParameterDirection.Input;
        }

        public OOsqlParameter(string Name, DbType DbType, object Value)
        {
            this.ParameterName = Name;
            this.DbType = DbType;
            this.Value = Value;
            this.Direction = ParameterDirection.Input;
        }


    }

    public enum OsqlParameterDataType
    {
        Text,
        /// <summary>
        /// 
        /// </summary>
        VarChar,
        /// <summary>
        /// 
        /// </summary>
        String,
        /// <summary>
        /// 
        /// </summary>
        Char,
        /// <summary>
        /// 
        /// </summary>
        Int,
        /// <summary>
        /// 长整形
        /// </summary>
        Integer,
        /// <summary>
        /// 
        /// </summary>
        Double,
        /// <summary>
        /// 
        /// </summary>
        Decimal,
        /// <summary>
        /// 
        /// </summary>
        Numeric,
        /// <summary>
        /// 
        /// </summary>
        Bool,
        /// <summary>
        /// 短日期类型
        /// 如 2015-12-10
        /// </summary>
        Date,
        /// <summary>
        /// 短日期类型
        /// 如 09:34:32
        /// </summary>
        Time,
        /// <summary>
        /// 日期类型
        /// </summary>
        DateTime,        
        /// <summary>
        /// 长日期时间格式 如 2015-12-10 18:23:32.231
        /// </summary>
        DateTime2,
        /// <summary>
        /// GUID
        /// </summary>
        Guid,
        /// <summary>
        /// 字节
        /// </summary>
        Byte,
        /// <summary>
        /// 文件流
        /// </summary>
        Stream,
        /// <summary>
        /// 
        /// </summary>
        Bit,
        /// <summary>
        /// 游标类型
        /// </summary>
        Refcursor
    }
}
