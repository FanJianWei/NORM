﻿using NORM.Common;
using NORM.Configure;
using NORM.Entity;
using NORM.SQLObject;
using Npgsql;
using NpgsqlTypes;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Reflection;
using System.Text;

namespace NORM.DataBase
{
    internal class PostgreSqlDataBase : DataBase, IDataBase
    {
        protected bool TransactionBegin = false;
        protected PostgreSqlHelper postgresql = new PostgreSqlHelper();

        private Npgsql.NpgsqlTransaction tran;
        private Npgsql.NpgsqlConnection conn = new Npgsql.NpgsqlConnection();

        internal PostgreSqlDataBase()
        {
            this._sql = string.Empty;
            try
            {
                this._connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["default"].ConnectionString;
                //解析连接字符串，解密
                if (string.IsNullOrEmpty(this._connectionString))
                    throw new Exception("连接字符串不能为空");
                SectionString[] SectionsString = Parse(this._connectionString);
                this._connectionString = Join(SectionsString);
                this._dataBaseType = DataBaseTypes.PostgreSqlDataBase;
                //初始化 conn
                this.conn = new NpgsqlConnection(_connectionString);
            }
            catch (Exception ex)
            {
                throw new NORMException(ExceptionType.ConfigException, ex.Message);
            }
        }

        internal PostgreSqlDataBase(string connectionString)
        {
            this._sql = string.Empty;
            this._connectionString = connectionString; //System.Configuration.ConfigurationManager.ConnectionStrings["default"].ConnectionString;
            //解析连接字符串，解密
            SectionString[] SectionsString = Parse(this._connectionString);
            this._connectionString = Join(SectionsString);
            this._dataBaseType = DataBaseTypes.PostgreSqlDataBase;
            //初始化 conn
            this.conn = new NpgsqlConnection(_connectionString);
           
        }

        #region 私有方法

        int execute(OQL oql)
        {
            int val = -1;

            SetCommand(oql);

            if (TransactionBegin)
                val = postgresql.Execute(conn, CommandType.Text, Command.CommandText, Command.Parameters);
            else
                val = postgresql.Execute(this._connectionString, CommandType.Text, Command.CommandText, Command.Parameters);

            return val;
        }

        bool insert<T>(T model) where T : EntityBase
        {
            bool rvl = false;
            if (model != null)
            {
                OQL oql = OQL.Insert(model).Values().End;

                if (execute(oql) > 0)
                {
                    rvl = true;
                }

            }
            return rvl;
        }

        bool update<T>(T model) where T : EntityBase
        {
            bool rvl = false;

            if (model != null)
            {
                OQL oql = OQL.Update(model).Set().End;

                if (execute(oql) > 0)
                {
                    rvl = true;
                }
            }            

            return rvl;
        }

        bool delete<T>(T model) where T : EntityBase
        {
            bool rvl = false;

            if (model != null)
            {
                List<Condition> conditions = new List<Condition>();

                Type _type = model.GetType();
                PropertyInfo[] properties = CacheProperties.cacheInstance.GetProperties(_type);  //_type.GetProperties();
                foreach (PropertyInfo pi in properties)
                {
                    if (pi.Name != "TableName" && pi.Name != "Properties")
                    {
                        object valud_d = pi.GetValue(model, null);
                        if (valud_d != null && DBNull.Value != valud_d)
                        {
                            Condition c = new Condition();
                            c.Field = pi.Name;
                            c.Value = valud_d;
                            conditions.Add(c);
                        }
                    }
                }

                OQL oql = OQL.Delete(model).Where(conditions.ToArray()).End;

                if (execute(oql) > 0)
                {
                    rvl = true;
                }

            }

            return rvl;
        }

        #endregion

        #region 设置Command命令
        public void SetCommand(OQL oql)
        {        
            _sql = oql.ToString();

            foreach (var model in oql.modelArray)
            {
                string classTypeName = "";
                object[] attrs = model.GetType().GetCustomAttributes(false);// typeof(T)
                if (attrs != null && attrs.Length > 0)
                {
                    foreach (object attr in attrs)
                    {
                        if (attr.GetType().Equals(typeof(DescribeAttribute)))
                        {
                            classTypeName = attr.ToString();
                            break;
                        }
                    }
                }
                switch (classTypeName)
                {
                    case "View":
                        break;
                }
            }

            Command =new OsqlCommand();

            Command.CommandText = _sql.Replace('[', '"').Replace(']', '"');

            List<NpgsqlParameter> _params = null;   

            if (oql.Parameters != null)
            {
                OsqlParameter[] osqlparams = oql.Parameters;
                switch (DataBaseType)
                {
                    case DataBaseTypes.PostgreSqlDataBase:
                        _params = new List<NpgsqlParameter>();
                        foreach (OsqlParameter p in osqlparams)
                        {
                            NpgsqlParameter p1 = new NpgsqlParameter("@" + p.Name, p.Value);
                            _params.Add(p1);
                        }
                        Command.Parameters = _params.ToArray();
                        break;
                }
            }

            base.validateOqlend(oql);
            //oql.Dispose();

        }

        public void SetCommand(string sql, params DbParameter[] cmdParms)
        {
            this._sql = sql;

            Command = new OsqlCommand();
            Command.CommandText = _sql;

            List<NpgsqlParameter> _params = null;

            if (cmdParms != null)
            {
                switch (DataBaseType)
                {
                    case DataBaseTypes.SqliteDataBase:
                        _params = new List<NpgsqlParameter>();
                        foreach (var p in cmdParms)
                        {
                            NpgsqlParameter p1 = new NpgsqlParameter("@" + p.ParameterName, p.Value);
                            _params.Add(p1);
                        }
                        Command.Parameters = _params.ToArray();
                        break;
                }
            }

        }

        #endregion

        #region 数据库对象

        public override DataTable GetDataBaseObject()
        {
            return postgresql.GetDbSchema(this._connectionString);
        }

        public override DataTable GetDataBaseObject(string collectionName)
        {
            return postgresql.GetDbSchema(this._connectionString, collectionName);
        }

        public override DataTable GetTableObject(string name, DataBaseSchema schema)
        {         
            DataTable dt = new DataTable();
            switch (schema)
            {
                case DataBaseSchema.Table:
                    dt = postgresql.GetTableColumns(this._connectionString, name);
                    break;
                case DataBaseSchema.View:
                    dt = postgresql.GetTableColumns(this._connectionString, name);
                    break;
            }
            return dt;
        }

        public override DataTable GetTableObject(string name)
        {
            return postgresql.GetTableColumns(this._connectionString, name);
        }

        public override DataTable GetTableIndexes(string tableName)
        {
            string sql = "select indexrelname as index_name, '' as index_description, '' as index_keys from pg_stat_all_indexes where relname='" + tableName + "' and indexrelname like 'idx_%' ";
            return postgresql.GetDataTable(this._connectionString, CommandType.Text, sql, null);
        }

        public override DataTable GetTableTrigger(string tableName)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.AppendLine("SELECT trigger_name");
            //strSql.AppendLine(",event_manipulation");
            //strSql.AppendLine(",action_statement");
            //strSql.AppendLine(",action_timing");
            strSql.AppendLine(",'未知' as trigger_state");
            strSql.AppendLine(",'' as trigger_createdate");
            strSql.AppendLine(",'' as trigger_updatedate");
            strSql.AppendLine("FROM information_schema.triggers ");
            strSql.AppendLine("WHERE event_object_table = '"+tableName+"' ");
            strSql.AppendLine("GROUP BY event_object_table,trigger_name ");
            strSql.AppendLine("ORDER BY event_object_table ");
            //strSql.AppendLine(",event_manipulation ");
            return postgresql.GetDataTable(this._connectionString, CommandType.Text, strSql.ToString(), null);
        }

        public override DataTable GetTableConstraint(string tableName)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.AppendLine("SELECT ");
            strSql.AppendLine("tc.constraint_name,");
            strSql.AppendLine("tc.constraint_type,");
            //strSql.AppendLine("tc.table_name, kcu.column_name,");
            strSql.AppendLine("ccu.column_name AS constraint_keys ");
            //strSql.AppendLine("ccu.table_name AS foreign_table_name,");
            //strSql.AppendLine("tc.is_deferrable,tc.initially_deferred");
            strSql.AppendLine("FROM ");
            strSql.AppendLine("information_schema.table_constraints AS tc ");
            strSql.AppendLine("JOIN information_schema.key_column_usage AS kcu ON tc.constraint_name = kcu.constraint_name ");
            strSql.AppendLine("JOIN information_schema.constraint_column_usage AS ccu ON ccu.constraint_name = tc.constraint_name ");
            strSql.AppendLine("WHERE tc.table_name = '" + tableName + "' ");

            return postgresql.GetDataTable(this._connectionString, CommandType.Text, strSql.ToString(), null);
        }

        #endregion

        #region 事务操作

        public override void BeginTransaction()
        {
            conn = new NpgsqlConnection(this._connectionString);
            if (conn.State == ConnectionState.Closed)
                conn.Open();
            tran = conn.BeginTransaction();
            postgresql.tran = tran;
            TransactionBegin = true;
            _surplusTransTime++;
        }

        public override int Commit()
        {
            try
            {
                tran.Commit();
                postgresql.tran = null;
            }
            catch (Exception ex)
            {

            }
            TransactionBegin = false;
            _surplusTransTime--;
            if (conn.State == ConnectionState.Open)
                conn.Close();
            return 1;
        }

        public override void RollBack()
        {
            try
            {
                tran.Rollback();
                postgresql.tran = null;
            }
            catch
            {

            }
            TransactionBegin = false;
            _surplusTransTime--;
            if (conn.State != ConnectionState.Closed)
                conn.Close();
        }

        public override bool TestConnect(out string message)
        {
            var rvl = base.TestConnect(out message);
            try
            {
                if (conn != null && conn.State != ConnectionState.Open)
                {
                    conn.Open();
                    message = "连接成功";
                    rvl = true;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                if (conn != null && conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }
            return rvl;
        }

        #endregion

        #region  执行SQL脚本

        public override int Execute(CommandType cmdType, string cmdText, params OsqlParameter[] cmdParms)
        {
            //return postgresql.Execute(_connectionString, cmdType, cmdText, cmdParms);

            DbParameter[] parameters = ParametersTransport(cmdParms);

            SetCommand(cmdText, parameters);

            int val = -1;
            if (TransactionBegin)
                val = postgresql.Execute(conn, cmdType, cmdText, parameters);
            else
                val = postgresql.Execute(this._connectionString, cmdType, cmdText, parameters);

            return val;
        }

        public override DataSet QueryDataSet(CommandType cmdType, string cmdText, params OsqlParameter[] cmdParms)
        {
            //return postgresql.GetDataSet(_connectionString, cmdType, cmdText, cmdParms);

            DbParameter[] parameters = ParametersTransport(cmdParms);

            SetCommand(cmdText, parameters);

            DataSet val = null;
            if (TransactionBegin)
                val = postgresql.GetDataSet(conn, cmdType, cmdText, parameters);
            else
                val = postgresql.GetDataSet(this._connectionString, cmdType, cmdText, parameters);

            return val;
        }

        public override DataTable QueryTable(string cmdText, OsqlParameter[] cmdParms = null)
        {
            return QueryDataSet(CommandType.Text, cmdText, cmdParms).Tables[0];
        }

        public override DataTable QueryTable(CommandType cmdType, string cmdText, OsqlParameter[] cmdParms = null)
        {
            return QueryDataSet(cmdType, cmdText, cmdParms).Tables[0];
        }

        public override bool UpdateDataSet(DataSet dataSet, string srcTable)
        {
            bool rvl = false;
            if (TransactionBegin)
                rvl = postgresql.UpdateDataSet(conn, dataSet, srcTable);
            else
                rvl = postgresql.UpdateDataSet(this._connectionString, dataSet, srcTable);
            return rvl;
        }

        #endregion

        #region 执行OQL

        public override int Execute(OQL oql)
        {            
            return this.execute(oql);
        }

        public override bool Insert<T>(T model)
        {
            return this.insert<T>(model);
        }

        public override bool Update<T>(T model)
        {
            return this.update<T>(model);
        }

        public override bool Delete<T>(T model)
        {
            return this.delete<T>(model);
        }

        public override bool Delete<T>(object PrimaryKeyValue)
        {
            T model = (T)Activator.CreateInstance(typeof(T));
            System.Reflection.PropertyInfo pi = typeof(T).GetProperty(model.PrimaryKey[0]);
            if (pi == null) throw new Exception("没有设置主键");            
          
            OQL oql = OQL.Delete(model).Where(new Condition[] { new Condition() { Field = pi.Name, Value = PrimaryKeyValue } }).End;

            return this.execute(oql) > 0 ? true : false;
        }

        public override List<T> QueryList<T>(OQL oql, PageLimit page)
        {
            SetCommand(oql);  

            string _total_sql = "SELECT COUNT(1) FROM (" +
                Command.CommandText.TrimEnd(';') + " ) tn ";

            var count = postgresql.GetScalar(conn, CommandType.Text, _total_sql, Command.Parameters);

            int recordCount = 0;
            if (!Int32.TryParse(count + "", out recordCount))
            {
                return new List<T>();
            }

            page.RecordCount = recordCount;
            page.PageCount = (int)(decimal.Ceiling((decimal)(page.RecordCount * 1.0 / page.PageSize)));

            if (page.PageIndex <= 1) page.PageIndex = 1;
            if (page.PageIndex >= page.PageCount) page.PageIndex = page.PageCount;

            int startIndex = (page.PageIndex - 1) * page.PageSize;
            int endIndex = startIndex + page.PageSize;

            if (Command.CommandText.Contains("Limit") && oql.ChildOql == null)
                throw new NORMException(ExceptionType.DataBaseExceptoin, "分页查询中不能再含有Limit");

            Command.CommandText = Command.CommandText.TrimEnd(';') + " Limit " + page.PageSize + " Offset  " + startIndex + "";

            DataTable dt = new DataTable();
            if (TransactionBegin)
                dt = postgresql.GetDataTable(conn, CommandType.Text, Command.CommandText, Command.Parameters);
            else
                dt = postgresql.GetDataTable(_connectionString, CommandType.Text, Command.CommandText, Command.Parameters);

            var list = ConvertTo.ConvertToList<T>(dt);

            page.DataSourceView = dt.DefaultView;
            oql.RecordCount = page.RecordCount;

            return list;
        }

        public override List<T> QueryList<T>(OQL oql)
        {
            SetCommand(oql);

            DataTable dt = new DataTable();
            if (TransactionBegin)
                dt = postgresql.GetDataTable(conn, CommandType.Text, Command.CommandText, Command.Parameters);
            else
                dt = postgresql.GetDataTable(this._connectionString, CommandType.Text, Command.CommandText, Command.Parameters);

            var list = ConvertTo.ConvertToList<T>(dt);
            oql.RecordCount = list.Count;

            return list;
        }

        public override T QueryObject<T>(OQL oql)
        {
            SetCommand(oql);

            T t = null; //(T)Activator.CreateInstance(typeof(T));// new T();
            Command.CommandText = Command.CommandText.Replace("Limit 1", "").TrimEnd(';') + " Limit 1";
            DataTable dt = new DataTable();
            if (TransactionBegin)
                dt = postgresql.GetDataTable(conn, CommandType.Text, Command.CommandText, Command.Parameters);
            else
                dt = postgresql.GetDataTable(_connectionString, CommandType.Text, Command.CommandText, Command.Parameters);

            List<T> list = ConvertTo.ConvertToList<T>(dt);
            foreach (T _t in list)
                t = _t;

            return t;           
        }

        public override T QueryModel<T>(object PrimaryKeyValue)
        {
            T t = (T)Activator.CreateInstance(typeof(T));// new T();

            if (t.PrimaryKey.Count <= 0)
                throw new Exception(t.TableName + "表还没有设置主键");

            OQL oql = new OQL(t).Select().Where(new Condition[] { 
                new Condition(){Field = t.PrimaryKey[0].ToString(), Comparison = "=", Value =PrimaryKeyValue } })
                .OrderBy(new string[] { t.PrimaryKey[0].ToString() }).End;

            SetCommand(oql); 
 
            t = QueryObject<T>(oql);

            return t;
        }

        public override DataTable QueryTable(OQL oql, PageLimit page = null)
        {
            DataTable dt = null;
            try
            {
                SetCommand(oql);

                if (page != null)
                {
                    string _total_sql = "SELECT COUNT(1) FROM (" +
                   Command.CommandText.TrimEnd(';') + " ) tn ";

                    var count = postgresql.GetScalar(conn, CommandType.Text, _total_sql, Command.Parameters);

                    int recordCount = 0;
                    if (!Int32.TryParse(count + "", out recordCount))
                    {
                        return new DataTable();
                    }

                    page.RecordCount = recordCount;
                    page.PageCount = (int)(decimal.Ceiling((decimal)(page.RecordCount * 1.0 / page.PageSize)));

                    if (page.PageIndex <= 1) page.PageIndex = 1;
                    if (page.PageIndex >= page.PageCount) page.PageIndex = page.PageCount;

                    int startIndex = (page.PageIndex - 1) * page.PageSize;
                    int endIndex = startIndex + page.PageSize;

                    Command.CommandText = Command.CommandText.TrimEnd(';') + " Limit " + page.PageSize + " Offset  " + startIndex + "";

                }

                if (TransactionBegin)
                    dt = postgresql.GetDataTable(conn, CommandType.Text, Command.CommandText, Command.Parameters);
                else
                    dt = postgresql.GetDataTable(this._connectionString, CommandType.Text, Command.CommandText, Command.Parameters);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        #endregion

        #region 参数转换

        private DbParameter[] ParametersTransport(OsqlParameter[] cmdParms)
        {
            NpgsqlParameter[] parameters = new NpgsqlParameter[cmdParms.Length];

            for (int i = 0; i < cmdParms.Length; i++)
            {
                OsqlParameter parameter = cmdParms[i];
                parameters[i] = new NpgsqlParameter(parameter.Name, parameter.Value);
                NpgsqlDbType parameterDbType = NpgsqlDbType.Varchar;
                switch (parameter.DataType)
                {
                    case OsqlParameterDataType.VarChar:
                    case OsqlParameterDataType.String:
                        parameterDbType = NpgsqlDbType.Varchar;
                        break;
                    case OsqlParameterDataType.Char:
                        parameterDbType = NpgsqlDbType.Char;
                        break;
                    case OsqlParameterDataType.Int:
                        parameterDbType = NpgsqlDbType.Integer;
                        break;
                    case OsqlParameterDataType.Guid:
                        parameterDbType = NpgsqlDbType.Varchar;
                        break;
                    case OsqlParameterDataType.Numeric:
                        parameterDbType = NpgsqlDbType.Numeric;
                        break;
                    case OsqlParameterDataType.DateTime:
                    case OsqlParameterDataType.DateTime2:
                        parameterDbType = NpgsqlDbType.Timestamp;
                        break;
                    case OsqlParameterDataType.Date:
                        parameterDbType = NpgsqlDbType.Date;
                        break;
                    case OsqlParameterDataType.Time:
                        parameterDbType = NpgsqlDbType.Time;
                        break;
                    case OsqlParameterDataType.Refcursor:
                        parameterDbType = NpgsqlDbType.Refcursor;
                        break;                   
                }
                parameters[i].NpgsqlDbType = parameterDbType;
                parameters[i].Direction = parameter.Direction;
            }
            return parameters;
        }

        #endregion

    }
}
