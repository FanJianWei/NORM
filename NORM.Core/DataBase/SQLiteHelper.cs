﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Text;
using System.Data.SQLite;

namespace NORM.DataBase
{
    #region SQLiteHelper

    /// <summary>
    /// Sqlite 数据库操作类
    /// </summary>
    internal class SQLiteHelper
    {
        public SQLiteTransaction tran = null;

        public int Execute(string connectionString, List<string> cmdTextList)
        {
            return 0;
        }

        public int Execute(string connectionString, CommandType cmdType, string cmdText, params DbParameter[] cmdParms)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    PrepareCommand(cmd, conn, null, cmdType, cmdText, cmdParms);
                    return cmd.ExecuteNonQuery();
                }
            }
        }

        public int Execute(DbConnection conn, CommandType cmdType, string cmdText, params DbParameter[] cmdParms)
        {
            using (SQLiteCommand cmd = new SQLiteCommand())
            {
                PrepareCommand(cmd, conn, tran, cmdType, cmdText, cmdParms);
                return cmd.ExecuteNonQuery();
            }
        }

        public object GetScalar(string connectionString, CommandType cmdType, string cmdText, params DbParameter[] cmdParms)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    PrepareCommand(cmd, conn, null, cmdType, cmdText, cmdParms);
                    return cmd.ExecuteScalar();
                }
            }
        }

        public object GetScalar(DbConnection conn, CommandType cmdType, string cmdText, params DbParameter[] cmdParms)
        {
            using (SQLiteCommand cmd = new SQLiteCommand())
            {
                PrepareCommand(cmd, conn, tran, cmdType, cmdText, cmdParms);
                return cmd.ExecuteScalar();
            }
        }

        public DataSet GetDataSet(string connectionString, CommandType cmdType, string cmdText, params DbParameter[] cmdParms)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    PrepareCommand(cmd, conn, null, cmdType, cmdText, cmdParms);
                    using (SQLiteDataAdapter da = new SQLiteDataAdapter())
                    {
                        da.SelectCommand = cmd;
                        DataSet ds = new DataSet();
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }

        public DataSet GetDataSet(DbConnection conn, CommandType cmdType, string cmdText, params DbParameter[] cmdParms)
        {
            using (SQLiteCommand cmd = new SQLiteCommand())
            {
                PrepareCommand(cmd, conn, tran, cmdType, cmdText, cmdParms);
                using (SQLiteDataAdapter da = new SQLiteDataAdapter())
                {
                    da.SelectCommand = cmd;
                    DataSet ds = new DataSet();
                    da.Fill(ds);
                    return ds;
                }
            }
        }

        public DataTable GetDataTable(string connectionString, CommandType cmdType, string cmdText, params DbParameter[] cmdParms)
        {
            return GetDataSet(connectionString, cmdType, cmdText, cmdParms).Tables[0];
        }

        public DataTable GetDataTable(DbConnection conn, CommandType cmdType, string cmdText, params DbParameter[] cmdParms)
        {
            return GetDataSet(conn, cmdType, cmdText, cmdParms).Tables[0];
        }

        public DbDataReader GetReader(string connectionString, CommandType cmdType, string cmdText, params DbParameter[] cmdParms)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    PrepareCommand(cmd, conn, null, cmdType, cmdText, cmdParms);
                    return cmd.ExecuteReader(CommandBehavior.CloseConnection);
                }
            }
        }

        public DbDataReader GetReader(DbConnection conn, CommandType cmdType, string cmdText, params DbParameter[] cmdParms)
        {
            using (SQLiteCommand cmd = new SQLiteCommand())
            {
                PrepareCommand(cmd, conn, tran, cmdType, cmdText, cmdParms);
                return cmd.ExecuteReader(CommandBehavior.CloseConnection);
            }
        }

        public DataTable GetDbSchema(string connectionString)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("id", typeof(Int32)));
            dt.Columns.Add(new DataColumn("table_name", typeof(string)));
            dt.Columns.Add(new DataColumn("table_type", typeof(string)));

            StringBuilder strSql = new StringBuilder();
            strSql.Append("  select [type] as [table_type] ,[tbl_name] as [table_name] from sqlite_master  ");

            DataTable dt_schema = GetDataTable(connectionString, CommandType.Text, strSql.ToString(), null);

            int index = 1;
            foreach (DataRow dr in dt_schema.Rows)
            {
                DataRow newrow = dt.NewRow();
                newrow["id"] = index;
                newrow["table_name"] = dr["table_name"] + "";
                string ttype = (dr["table_type"] + "").Trim().ToLower();
                //switch (ttype)
                //{
                //    case "u": ttype = "table"; break;
                //    case "v": ttype = "view"; break;
                //    case "p": ttype = "procedure"; break;
                //}
                newrow["table_type"] = ttype;
                dt.Rows.Add(newrow);
                index++;
            }
            dt_schema.Dispose();

            return dt;
        }

        public DataTable GetDbSchema(string connectionString, string collectionName)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("id", typeof(Int32)));
            dt.Columns.Add(new DataColumn("table_name", typeof(string)));
            dt.Columns.Add(new DataColumn("table_type", typeof(string)));
            
            switch (collectionName)
            {
                case "t":
                case "u": collectionName = "table"; break;
                case "v": collectionName = "view"; break;
                case "p": collectionName = "procedure"; break;
                case "f": collectionName = "function"; break;
            }

            StringBuilder strSql = new StringBuilder();
            strSql.Append("  select [type] as [table_type] ,[tbl_name] as [table_name] from sqlite_master  ");
            if (!string.IsNullOrEmpty(collectionName))
            {
                strSql.Append(" where [type]='" + collectionName.ToLower() + "' ");
            }

            DataTable dt_schema = GetDataTable(connectionString, CommandType.Text, strSql.ToString(), null);

            int index = 1;
            foreach (DataRow dr in dt_schema.Rows)
            {
                DataRow newrow = dt.NewRow();
                newrow["id"] = index;
                newrow["table_name"] = dr["table_name"] + "";
                string ttype = (dr["table_type"] + "").Trim().ToLower();
                //switch (ttype)
                //{
                //    case "u": ttype = "table"; break;
                //    case "v": ttype = "view"; break;
                //    case "p": ttype = "procedure"; break;
                //}
                newrow["table_type"] = ttype;
                dt.Rows.Add(newrow);
                index++;
            }
            dt_schema.Dispose();

            return dt;
        }

        public bool AddColumDescribtion(string connectionString, string tableName, string columnName, string Descrition)
        {
            return true;
        }

        public DataTable GetTables(string connectionString)
        {
            DataTable dt = new DataTable();
            dt.Columns.Add(new DataColumn("id", typeof(Int32)));
            dt.Columns.Add(new DataColumn("table_name", typeof(string)));
            dt.Columns.Add(new DataColumn("table_type", typeof(string)));

            StringBuilder strSql = new StringBuilder();
            strSql.Append("  select [type] as [table_type] ,[tbl_name] as [table_name] from sqlite_master  ");

            DataTable dt_schema = GetDataTable(connectionString, CommandType.Text, strSql.ToString(), null);

            int index = 1;
            foreach (DataRow dr in dt_schema.Rows)
            {
                DataRow newrow = dt.NewRow();
                newrow["id"] = index;
                newrow["table_name"] = dr["table_name"] + "";
                string ttype = (dr["table_type"] + "").Trim().ToLower();
                switch (ttype)
                {
                    case "u": ttype = "table"; break;
                    case "v": ttype = "view"; break;
                    case "p": ttype = "procedure"; break;
                }
                newrow["table_type"] = ttype;
                dt.Rows.Add(newrow);
                index++;
            }
            dt_schema.Dispose();

            return dt;
        }

        public DataTable GetTableColumns(string connectionString, string tableName)
        {
            DataTable dt = new DataTable(tableName);
            dt.Columns.Add(new DataColumn("colorder", typeof(Int32)));
            dt.Columns.Add(new DataColumn("ColumnName", typeof(string)));
            dt.Columns.Add(new DataColumn("TypeName", typeof(string)));
            dt.Columns.Add(new DataColumn("Length", typeof(string)));
            dt.Columns.Add(new DataColumn("CisNull", typeof(string)));
            dt.Columns.Add(new DataColumn("DefaultValue", typeof(string)));
            dt.Columns.Add(new DataColumn("Describ", typeof(string)));

            string strSql = "pragma table_info([" + tableName + "])";
            DataTable dt_columns = GetDataTable(connectionString, CommandType.Text, strSql, null);
            int index = 1;
            foreach (DataRow dr in dt_columns.Rows)
            {
                DataRow newrow = dt.NewRow();
                newrow["colorder"] = index;
                newrow["ColumnName"] = dr["name"];
                string len = dr["type"] + "";
                string typename = dr["type"] + "";
                if (typename.Contains("(") && typename.Contains(")"))
                {
                    len = len.Substring(len.LastIndexOf("(") + 1);
                    len = len.Substring(0, len.LastIndexOf(")"));
                    typename = typename.Substring(0, typename.LastIndexOf("("));
                }
                else
                {
                    len = "";
                }

                if (typename.ToLower().Equals("varchar") && string.IsNullOrEmpty(len))
                {
                    len = "50";
                }

                newrow["TypeName"] = typename;//nvarchar(50)
                newrow["Length"] = len;

                string pk = "";
                pk = dr["pk"] + "";
                if (pk == "1")
                {
                    pk = "pk,";
                }
                else
                {
                    pk = "";
                }
                string isnull = dr["notnull"] + "";
                if (isnull == "1")
                {
                    isnull = "not null";
                }
                else
                {
                    isnull = "null";
                }                 

                newrow["CisNull"] = pk + isnull + ",";
                newrow["Describ"] = "";
                newrow["DefaultValue"] = "";


                dt.Rows.Add(newrow);
                index++;
            }

            return dt;
        }

        public bool RenameTable(string connectionString, string OldName, string NewName)
        {
            return false;
        }

        public bool UpdateDataSet(string connectionString, DataSet dataSet, string srcTable)
        {
            bool rvl = false;
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                using (SQLiteDataAdapter da = new SQLiteDataAdapter(srcTable, conn))
                {
                    SQLiteCommandBuilder scb = new SQLiteCommandBuilder(da);
                    da.InsertCommand = scb.GetInsertCommand();
                    da.UpdateCommand = scb.GetUpdateCommand();
                    da.DeleteCommand = scb.GetDeleteCommand();
                    da.Update(dataSet);
                    rvl = true;
                }
            }
            return rvl;
        }

        public bool UpdateDataSet(DbConnection conn, DataSet dataSet, string srcTable)
        {
            bool rvl = false;
            using (SQLiteCommand cmd = new SQLiteCommand(srcTable, (SQLiteConnection)conn))
            {
                cmd.Transaction = tran;
                using (SQLiteDataAdapter da = new SQLiteDataAdapter(cmd))
                {
                    SQLiteCommandBuilder scb = new SQLiteCommandBuilder(da);
                    da.InsertCommand = scb.GetInsertCommand();
                    da.UpdateCommand = scb.GetUpdateCommand();
                    da.DeleteCommand = scb.GetDeleteCommand();
                    da.Update(dataSet);
                    rvl = true;
                }
            }
            return rvl;
        } 

        public DataSet GetListByPage(string connectionString, string strTab, string strFields, string strWhere, string orderby, int startIndex, int endIndex)
        {
            using (SQLiteConnection conn = new SQLiteConnection(connectionString))
            {
                using (SQLiteCommand cmd = new SQLiteCommand())
                {
                    StringBuilder strSql = new StringBuilder();
                    strSql.Append(" select " + strFields + " from " + strTab + " T ");
                    if (strWhere.Trim() != "")
                    {
                        strSql.Append(" where " + strWhere + " ");
                    }
                    if (orderby.Trim() != "")
                    {
                        strSql.Append(" order by " + orderby + " ");
                    }
                    strSql.Append(" limit " + (endIndex + 1 - startIndex) + " offset " + (startIndex - 1) + " ");

                    PrepareCommand(cmd, conn, null, CommandType.Text, strSql.ToString(), null);
                    using (SQLiteDataAdapter da = new SQLiteDataAdapter())
                    {
                        DataSet ds = new DataSet();
                        da.SelectCommand = cmd;
                        da.Fill(ds);
                        return ds;
                    }
                }
            }
        }

        /// <summary>
        /// 生成要执行的命令
        /// </summary>
        private static void PrepareCommand(DbCommand cmd, DbConnection conn, DbTransaction trans, CommandType cmdType, string cmdText, DbParameter[] cmdParms)
        {
            try
            {

                // 如果存在参数，则表示用户是用参数形式的SQL语句，可以替换
                if (cmdParms != null && cmdParms.Length > 0)
                    cmdText = cmdText.Replace("?", "@").Replace(":", "@");

                if (conn.State != ConnectionState.Open)
                    conn.Open();

                cmd.Connection = conn;
                cmd.CommandText = cmdText;
                if (trans != null)
                    cmd.Transaction = trans;
                cmd.CommandType = cmdType;

                if (cmdParms != null)
                {
                    foreach (DbParameter parm in cmdParms)
                    {
                        // 如果存在参数，则表示用户是用参数形式的SQL语句，可以替换
                        //parm.ParameterName = parm.ParameterName.Replace("?", "@").Replace(":", "@");
                        //if (parm.Value == null)
                        //    parm.Value = DBNull.Value;
                        //cmd.Parameters.Add(parm);

                        // 如果存在参数，则表示用户是用参数形式的SQL语句，可以替换
                        string name = parm.ParameterName.Replace("?", "@").Replace(":", "@");
                        object value = parm.Value == null ? DBNull.Value : parm.Value;
                        DbParameter p = new SQLiteParameter();
                        p.ParameterName = name;
                        p.DbType = parm.DbType;
                        p.Direction = parm.Direction;

                        if (p.Direction == ParameterDirection.Output
                            || p.Direction == ParameterDirection.InputOutput)
                        {
                            p = parm;
                        }
                        else
                        {
                            p.Value = value;
                            p.Size = parm.Size;
                        }

                        cmd.Parameters.Add(p);

                    }
                }
            }
            catch (Exception ex)
            {
                throw new NORMException(ExceptionType.DataBaseExceptoin, ex.Message);
            }
        }
    }

    #endregion
}
