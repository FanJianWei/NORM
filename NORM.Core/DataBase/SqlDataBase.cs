﻿using NORM.Common;
using NORM.Entity;
using NORM.SQLObject; 
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Reflection;
using System.Text;
using System.Data.SqlClient;
using NORM.Configure;

namespace NORM.DataBase
{
    internal class SqlDataBase : DataBase, IDataBase
    {
        protected bool TransactionBegin = false;
        protected SqlHelper sqlserver = new SqlHelper();

        private SqlTransaction tran;
        private SqlConnection conn = new SqlConnection();

        internal SqlDataBase()
        {
            this._sql = string.Empty;
            try
            {
                this._connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["default"].ConnectionString;
                //解析连接字符串，解密
                if (string.IsNullOrEmpty(this._connectionString))
                    throw new Exception("连接字符串不能为空");
                SectionString[] SectionsString = Parse(this._connectionString);
                this._connectionString = Join(SectionsString);
                this._dataBaseType = DataBaseTypes.SqlDataBase;
                //初始化 conn
                this.conn = new SqlConnection(_connectionString);
            }
            catch (Exception ex)
            {
                throw new NORMException(ExceptionType.ConfigException, ex.Message);
            }
        }

        internal SqlDataBase(string connectionString)
        {
            this._sql = string.Empty;
            this._connectionString = connectionString; //System.Configuration.ConfigurationManager.ConnectionStrings["default"].ConnectionString;
            //解析连接字符串，解密
            SectionString[] SectionsString = Parse(this._connectionString);
            this._connectionString = Join(SectionsString);
            this._dataBaseType = DataBaseTypes.SqlDataBase;
            //初始化 conn
            this.conn = new SqlConnection(_connectionString);
           
        }

        #region 私有方法

        int execute(OQL oql)
        {
            int val = -1;

            SetCommand(oql);

            if (TransactionBegin)
                val = sqlserver.Execute(conn, CommandType.Text, Command.CommandText, Command.Parameters);
            else
                val = sqlserver.Execute(this._connectionString, CommandType.Text, Command.CommandText, Command.Parameters);

            return val;
        }

        bool insert<T>(T model) where T : EntityBase
        {
            bool rvl = false;
            if (model != null)
            {
                OQL oql = OQL.Insert(model).Values().End;

                if (execute(oql) > 0)
                {
                    rvl = true;
                }

            }
            return rvl;
        }

        bool update<T>(T model) where T : EntityBase
        {
            bool rvl = false;

            if (model != null)
            {
                OQL oql = OQL.Update(model).Set().End;

                if (execute(oql) > 0)
                {
                    rvl = true;
                }
            }            

            return rvl;
        }

        bool delete<T>(T model) where T : EntityBase
        {
            bool rvl = false;

            if (model != null)
            {
                List<Condition> conditions = new List<Condition>();

                Type _type = model.GetType();
                PropertyInfo[] properties = CacheProperties.cacheInstance.GetProperties(_type);  //_type.GetProperties();
                foreach (PropertyInfo pi in properties)
                {
                    if (pi.Name != "TableName" && pi.Name != "Properties")
                    {
                        object valud_d = pi.GetValue(model, null);
                        if (valud_d != null && DBNull.Value != valud_d)
                        {
                            Condition c = new Condition();
                            c.Field = pi.Name;
                            c.Value = valud_d;
                            conditions.Add(c);
                        }
                    }
                }

                OQL oql = OQL.Delete(model).Where(conditions.ToArray()).End;

                if (execute(oql) > 0)
                {
                    rvl = true;
                }

            }

            return rvl;
        }

        #endregion

        #region 设置Command命令

        private string sqlcommandtextParse(string sql)
        {
            string result = string.Empty;
            //处理Limit

            string pattern = "Limit [0-9]+";
            List<string> limitArray = new List<string>();

            System.Text.RegularExpressions.Regex reg = new System.Text.RegularExpressions.Regex(pattern,
                System.Text.RegularExpressions.RegexOptions.IgnoreCase | System.Text.RegularExpressions.RegexOptions.Compiled);

            System.Text.RegularExpressions.Match math = reg.Match(sql);
            while (math.Success)
            {
                string limitStr = math.Groups[0] + "";
                limitArray.Add(limitStr);
                math = math.NextMatch();               
            }

            if(limitArray.Count >1)
                throw new NORMException(ExceptionType.DataBaseExceptoin, sql + " Limit 附近有语法错误");

            foreach (string limit in limitArray)
            {
                string _limit = limit.Replace("Limit ", "");
                sql = sql.Replace(limit, "").Replace("SELECT", "SELECT TOP " + _limit + "");
            }

            result = sql;

            return result;
        }

        public void SetCommand(OQL oql)
        {
            _sql = oql.ToString();
            if (_sql.Contains("Limit"))
            {
                if (oql.ChildOql != null && oql.ChildOql.sql.Contains("Limit"))
                    throw new NORMException(ExceptionType.DataBaseExceptoin, oql.sql + "Limit 附近有语法错误");   
                _sql = sqlcommandtextParse(_sql);                               
            }
            
            Command =new OsqlCommand();

            //foreach (var model in oql.modelArray)
            //{
            //    string classTypeName = "";
            //    object[] attrs = model.GetType().GetCustomAttributes(false);// typeof(T)
            //    if (attrs != null && attrs.Length > 0)
            //    {
            //        foreach (object attr in attrs)
            //        {
            //            if (attr.GetType().Equals(typeof(AtttribDescrib)))
            //            {
            //                classTypeName = attr.ToString();
            //                break;
            //            }
            //        }
            //    }
            //    switch (classTypeName)
            //    {
            //        case "View":
            //            _sql = _sql.Replace("[", "").Replace("]", "");
            //            break;
            //        case "Table": 
            //            break;
            //    }
            //}

            Command.CommandText = _sql;               

            List<SqlParameter> _params = null;   

            if (oql.Parameters != null)
            {
                OsqlParameter[] osqlparams = oql.Parameters;
                switch (DataBaseType)
                {
                    case DataBaseTypes.SqlDataBase:
                        _params = new List<SqlParameter>();
                        foreach (OsqlParameter p in osqlparams)
                        {
                            SqlParameter p1 = new SqlParameter("@" + p.Name, p.Value);
                            _params.Add(p1);
                        }
                        Command.Parameters = _params.ToArray();
                        break;
                }
            }          

            base.validateOqlend(oql);
            //oql.Dispose();

        }

        public void SetCommand(string sql, params DbParameter[] cmdParms)
        {
            this._sql = sql;

            Command = new OsqlCommand();
            Command.CommandText = _sql;

            List<SqlParameter> _params = null;

            if (cmdParms != null)
            {
                switch (DataBaseType)
                {
                    case DataBaseTypes.SqliteDataBase:
                        _params = new List<SqlParameter>();
                        foreach (var p in cmdParms)
                        {
                            SqlParameter p1 = new SqlParameter("@" + p.ParameterName, p.Value);
                            _params.Add(p1);
                        }
                        Command.Parameters = _params.ToArray();
                        break;
                }
            }

        }

        #endregion

        #region 数据库对象

        public override DataTable GetDataBaseObject()
        {
            return sqlserver.GetDbSchema(this._connectionString);
        }

        public override DataTable GetDataBaseObject(string collectionName)
        {
            return sqlserver.GetDbSchema(this._connectionString, collectionName);
        }

        public override DataTable GetTableObject(string name, DataBaseSchema schema)
        {         
            DataTable dt = new DataTable();
            switch (schema)
            {
                case DataBaseSchema.Table:
                    dt = sqlserver.GetTableColumns(this._connectionString, name);
                    break;
                case DataBaseSchema.View:
                    dt = sqlserver.GetTableColumns(this._connectionString, name);
                    break;
            }
            return dt;
        }       

        public override DataTable GetTableObject(string name)
        {
            return sqlserver.GetTableColumns(this._connectionString, name); 
        }

        public override DataTable GetDependencies(string name, int type)
        {
            return sqlserver.GetDependencies(this._connectionString, name, type);
        }

        public override DataTable GetTableIndexes(string tableName)
        {
            string strSql = "sp_helpindex  @objname='" + tableName + "'";
            DataSet ds = sqlserver.GetDataSet(this._connectionString, CommandType.Text, strSql, null);
            return ds != null && ds.Tables.Count > 0 ? ds.Tables[0] : null;
        }

        public override DataTable GetTableTrigger(string tableName)
        {
            StringBuilder strSql = new StringBuilder();
            strSql.Append("SELECT ").Append(System.Environment.NewLine);
            //strSql.Append("--object_name(a.parent_obj) as [表名]  ").Append(System.Environment.NewLine);
            strSql.Append("a.name as [trigger_name] ").Append(System.Environment.NewLine);
            strSql.Append(",(case when b.is_disabled=0 then '启用' else '禁用' end) as [trigger_state] ").Append(System.Environment.NewLine);
            strSql.Append(",b.create_date as [trigger_createdate] ").Append(System.Environment.NewLine);
            strSql.Append(",b.modify_date as [trigger_updatedate] ").Append(System.Environment.NewLine);
            //strSql.Append("--,c.text as [trigger_sql]  ").Append(System.Environment.NewLine);
            strSql.Append("FROM sysobjects a  ").Append(System.Environment.NewLine);
            strSql.Append("    INNER JOIN sys.triggers b ").Append(System.Environment.NewLine);
            strSql.Append("        ON b.object_id=a.id ").Append(System.Environment.NewLine);
            strSql.Append("    INNER JOIN syscomments c  ").Append(System.Environment.NewLine);
            strSql.Append("        ON c.id=a.id ").Append(System.Environment.NewLine);
            strSql.Append("WHERE a.xtype='tr' and parent_obj=object_id('[dbo].["+tableName+"]') ").Append(System.Environment.NewLine);

            return sqlserver.GetDataTable(this._connectionString,CommandType.Text, strSql.ToString(),null);
        }

        public override DataTable GetTableConstraint(string tableName)
        {
            string strSql = "exec sp_helpconstraint @objname='[dbo].[" + tableName + "]'";

            DataTable result = new DataTable();
            result.Columns.Add(new DataColumn("constraint_name", typeof(string)));
            result.Columns.Add(new DataColumn("constraint_type", typeof(string)));
            result.Columns.Add(new DataColumn("constraint_keys", typeof(string)));

            try
            {
                DataSet ds = sqlserver.GetDataSet(this._connectionString, CommandType.Text, strSql, null);
                if (ds != null && ds.Tables.Count > 0)
                {
                    foreach (DataTable dt in ds.Tables)
                    {
                        if (dt.Columns.Contains("constraint_type") && dt.Columns.Contains("constraint_name"))
                        {
                            foreach (DataRow dr in dt.Rows)
                            {
                                DataRow row = result.NewRow();
                                row["constraint_name"] = dr["constraint_name"] + "";
                                row["constraint_type"] = dr["constraint_type"] + "";
                                row["constraint_keys"] = dr["constraint_keys"] + "";

                                result.Rows.Add(row);
                            }
                        }
                    }
                }

            }
            catch 
            {
                //
            }
            
            return result;
        }

        #endregion

        #region 事务操作

        public override void BeginTransaction()
        {
            conn = new SqlConnection(this._connectionString);
            if (conn.State == ConnectionState.Closed)
                conn.Open();
            tran = conn.BeginTransaction();
            sqlserver.tran = tran;
            TransactionBegin = true;
            _surplusTransTime++;
        }

        public override int Commit()
        {
            try
            {
                tran.Commit();
                sqlserver.tran = null;
            }
            catch (Exception ex)
            {

            }
            TransactionBegin = false;
            _surplusTransTime--;
            if (conn.State == ConnectionState.Open)
                conn.Close();
            return 1;
        }

        public override void RollBack()
        {
            try
            {
                tran.Rollback();
                sqlserver.tran = null;
            }
            catch
            {

            }
            TransactionBegin = false;
            _surplusTransTime--;
            if (conn.State != ConnectionState.Closed)
                conn.Close();
        }

        public override bool TestConnect(out string message)
        {
            var rvl = base.TestConnect(out message);
            try
            {
                if (conn != null && conn.State != ConnectionState.Open)
                {
                    conn.Open();
                    message = "连接成功";
                    rvl = true;
                }
            }
            catch (Exception ex)
            {
                message = ex.Message;
            }
            finally
            {
                if (conn != null && conn.State != ConnectionState.Closed)
                {
                    conn.Close();
                }
            }
            return rvl;
        }

        #endregion

        #region  执行SQL脚本

        public override int Execute(CommandType cmdType, string cmdText, params OsqlParameter[] cmdParms)
        {
            DbParameter[] parameters = ParametersTransport(cmdParms);

            SetCommand(cmdText, parameters);

            int val = -1;
            if (TransactionBegin)
                val = sqlserver.Execute(conn, cmdType, cmdText, parameters);
            else
                val = sqlserver.Execute(this._connectionString, cmdType, cmdText, parameters);

            return val;
        }

        public override int Execute(CommandType cmdType, string cmdText, int timeout, params OsqlParameter[] cmdParms)
        {
            DbParameter[] parameters = ParametersTransport(cmdParms);

            SetCommand(cmdText, parameters);

            int val = -1;
            if (TransactionBegin)
                val = sqlserver.Execute(conn, cmdType, cmdText, timeout, parameters);
            else
                val = sqlserver.Execute(this._connectionString, cmdType, cmdText, timeout, parameters);

            return val;
        }

        public override DataSet QueryDataSet(CommandType cmdType, string cmdText, params OsqlParameter[] cmdParms)
        {
            DbParameter[] parameters = ParametersTransport(cmdParms);

            SetCommand(cmdText, parameters);

            DataSet val = null;
            if (TransactionBegin)
                val = sqlserver.GetDataSet(conn, cmdType, cmdText, parameters);
            else
                val = sqlserver.GetDataSet(this._connectionString, cmdType, cmdText, parameters);

            return val;
        }

        public override DataTable QueryTable(string cmdText, OsqlParameter[] cmdParms = null)
        {
            return QueryDataSet(CommandType.Text, cmdText, cmdParms).Tables[0];
        }

        public override DataTable QueryTable(CommandType cmdType, string cmdText, OsqlParameter[] cmdParms = null)
        {
            return QueryDataSet(cmdType, cmdText, cmdParms).Tables[0];
        } 

        public override bool UpdateDataSet(DataSet dataSet, string srcTable)
        {
            bool rvl = false;
            if (TransactionBegin)
                rvl = sqlserver.UpdateDataSet(conn, dataSet, srcTable);
            else
                rvl = sqlserver.UpdateDataSet(this._connectionString, dataSet, srcTable);
            return rvl;
        }

        #endregion

        #region 执行OQL

        public override int Execute(OQL oql)
        {            
            return this.execute(oql);
        }

        public override bool Insert<T>(T model)
        {
            return this.insert<T>(model);
        }

        public override bool Update<T>(T model)
        {
            return this.update<T>(model);
        }

        public override bool Delete<T>(T model)
        {
            return this.delete<T>(model);
        }

        public override bool Delete<T>(object PrimaryKeyValue)
        {
            T model = (T)Activator.CreateInstance(typeof(T));
            System.Reflection.PropertyInfo pi = typeof(T).GetProperty(model.PrimaryKey[0]);
            if (pi == null) throw new Exception("没有设置主键");            
          
            OQL oql = OQL.Delete(model).Where(new Condition[] { new Condition() { Field = pi.Name, Value = PrimaryKeyValue } }).End;

            return this.execute(oql) > 0 ? true : false;
        }

        public override List<T> QueryList<T>(OQL oql, PageLimit page)
        {
            SetCommand(oql);
            var list = new List<T>();
            try
            {

                string _TEMPSQL = Command.CommandText.TrimEnd(';');
                if (_TEMPSQL.Contains("ORDER BY"))
                {
                    _TEMPSQL = _TEMPSQL.Substring(0, _TEMPSQL.LastIndexOf("ORDER BY"));
                }

                string _total_sql = "SELECT COUNT(1) FROM (" +
                    _TEMPSQL + " ) tn ";

                var count = sqlserver.GetScalar(conn, CommandType.Text, _total_sql, Command.Parameters);

                int recordCount = 0;
                if (!Int32.TryParse(count + "", out recordCount))
                {
                    throw new NORMException(ExceptionType.OQLExceptin,"获取不到总记录数");
                }

                page.RecordCount = recordCount;
                page.PageCount = (int)(decimal.Ceiling((decimal)(page.RecordCount * 1.0 / page.PageSize)));
                
                if (page.PageCount <= 0) page.PageCount = 1;
                if (page.PageIndex <= 1) page.PageIndex = 1;
                if (page.PageIndex >= page.PageCount) page.PageIndex = page.PageCount;

                int startIndex = (page.PageIndex - 1) * page.PageSize + 1;
                int endIndex = startIndex + page.PageSize - 1;
                if (endIndex > page.RecordCount) endIndex = page.RecordCount;

                if (Command.CommandText.Contains("Limit") && oql.ChildOql == null)
                    throw new NORMException(ExceptionType.DataBaseExceptoin, "分页查询中不能再含有Limit");


                string SQL = "SELECT * FROM ( ";
                if (oql.Orderbies != null)
                {
                    SQL += "SELECT row_number() OVER ( ORDER BY ";
                    foreach (object o in oql.Orderbies)
                    {
                        string s = o.ToString();
                        //string[] array = o.ToString().Split(' ');
                        //string field = array[0] + "";
                        //string orderby = array[1] + "";

                        string f = string.Empty;
                        string b = string.Empty;
                        if (s.EndsWith("DESC"))
                        {
                            b = "DESC";
                            f = "[" + s.Replace("DESC", "").Trim() + "]";
                        }
                        else if (s.EndsWith("ASC"))
                        {
                            b = "ASC";
                            f = "[" + s.Replace("ASC", "").Trim() + "]";
                        }
                        else if (s.EndsWith("Asc"))
                        {
                            b = "Asc";
                            f = "[" + s.Replace("Asc", "").Trim() + "]";
                        }
                        else if (s.EndsWith("Desc"))
                        {
                            b = "Desc";
                            f = "[" + s.Replace("Desc", "").Trim() + "]";
                        }
                        else if (s.EndsWith("asc"))
                        {
                            b = "asc";
                            f = "[" + s.Replace("asc", "").Trim() + "]";
                        }
                        else if (s.EndsWith("desc"))
                        {
                            b = "desc";
                            f = "[" + s.Replace("desc", "").Trim() + "]";
                        }

                        SQL += f + " " + b + ",";

                    }
                    SQL = SQL.TrimEnd(',') + " ) AS rowId";
                }
                else
                {
                    SQL += "SELECT row_number() OVER ( ORDER BY ";
                    foreach (string s in oql.Modelobject.PrimaryKey)
                    {
                        string f = string.Empty;
                        string b = string.Empty;

                        b = "ASC";
                        f = "[" + s.ToString() + "]";
                        SQL += f + " " + b + ",";

                    }
                    SQL = SQL.TrimEnd(',') + " ) AS rowId";
                }

                SQL += "," + _TEMPSQL.Replace("SELECT", " ");
                SQL += " ) _LY WHERE rowId>=" + startIndex + " AND rowId<=" + endIndex + " ;";

                Command.CommandText = SQL;

                DataTable dt = new DataTable();
                if (TransactionBegin)
                    dt = sqlserver.GetDataTable(conn, CommandType.Text, Command.CommandText, Command.Parameters);
                else
                    dt = sqlserver.GetDataTable(_connectionString, CommandType.Text, Command.CommandText, Command.Parameters);

                list = ConvertTo.ConvertToList<T>(dt);

                //DbDataReader reader;
                //if (TransactionBegin)
                //    reader = (DbDataReader)sqlserver.GetReader(conn, CommandType.Text, Command.CommandText, Command.Parameters);
                //else
                //    reader = (DbDataReader)sqlserver.GetReader(conn, CommandType.Text, Command.CommandText, Command.Parameters);

                //var list = ConvertTo.ConvertToList<T>(reader);               
                //reader.Close();
                //reader.Dispose(); 

                page.DataSourceView = dt.DefaultView;
                oql.RecordCount = page.RecordCount;

            }
            catch (Exception ex)
            {
                throw new NORMException(ExceptionType.DataBaseExceptoin, ex.Message + "\r\n sql:" + Command.CommandText);
            }        

            return list;
        }

        public override List<T> QueryList<T>(OQL oql)
        {
            SetCommand(oql);

            //2016-01-29 之前
            //DataTable dt = new DataTable();
            //if (TransactionBegin)
            //    dt = sqlserver.GetDataTable(conn, CommandType.Text, Command.CommandText, Command.Parameters);
            //else
            //    dt = sqlserver.GetDataTable(this._connectionString, CommandType.Text, Command.CommandText, Command.Parameters);

            //var list = ConvertTo.ConvertToList<T>(dt);
            //oql.RecordCount = list.Count;


            DbDataReader reader;
            if (TransactionBegin)
                reader = (DbDataReader)sqlserver.GetReader(conn, CommandType.Text, Command.CommandText, Command.Parameters);
            else
                reader = (DbDataReader)sqlserver.GetReader(conn, CommandType.Text, Command.CommandText, Command.Parameters);
            
            var list = ConvertTo.ConvertToList<T>(reader);
            oql.RecordCount = list.Count;
            reader.Close();
            reader.Dispose();            
            
            return list;
        }

        public override T QueryObject<T>(OQL oql)
        {
            SetCommand(oql);

            T t = null; //(T)Activator.CreateInstance(typeof(T));// new T();
            if (!Command.CommandText.Contains("SELECT TOP"))
                Command.CommandText = Command.CommandText.Replace("SELECT", "SELECT TOP 1 ").Replace("Limit 1", "");
            else
                Command.CommandText = Command.CommandText.Replace("SELECT TOP " + oql._limit + " ", "SELECT TOP 1 ").Replace("Limit 1", "");
            
            DataTable dt = new DataTable();
            if (TransactionBegin)
                dt = sqlserver.GetDataTable(conn, CommandType.Text, Command.CommandText, Command.Parameters);
            else
                dt = sqlserver.GetDataTable(_connectionString, CommandType.Text, Command.CommandText, Command.Parameters);

            List<T> list = ConvertTo.ConvertToList<T>(dt);
            foreach (T _t in list)
                t = _t;

            return t;           
        }

        public override T QueryModel<T>(object PrimaryKeyValue)
        {
            T t = (T)Activator.CreateInstance(typeof(T));// new T();

            if (t.PrimaryKey.Count <= 0)
                throw new Exception(t.TableName + "表还没有设置主键");

            OQL oql = new OQL(t).Select().Where(new Condition[] { 
                new Condition(){Field = t.PrimaryKey[0].ToString(), Comparison = "=", Value =PrimaryKeyValue } })
                .OrderBy(new string[] { t.PrimaryKey[0].ToString() }).End;

            SetCommand(oql); 
 
            t = QueryObject<T>(oql);

            return t;
        }

        public override DataTable QueryTable(OQL oql, PageLimit page = null)
        {
            DataTable dt = null;
            try
            {
                SetCommand(oql);

                if (page != null)
                {
                    string _TEMPSQL = Command.CommandText.TrimEnd(';');
                    if (_TEMPSQL.Contains("ORDER BY"))
                    {
                        _TEMPSQL = _TEMPSQL.Substring(0, _TEMPSQL.LastIndexOf("ORDER BY"));
                    }

                    string _total_sql = "SELECT COUNT(1) FROM (" +
                        _TEMPSQL + " ) tn ";

                    var count = sqlserver.GetScalar(conn, CommandType.Text, _total_sql, Command.Parameters);

                    int recordCount = 0;
                    if (!Int32.TryParse(count + "", out recordCount))
                    {
                        throw new NORMException(ExceptionType.OQLExceptin, "获取不到总记录数");
                    }

                    page.RecordCount = recordCount;
                    page.PageCount = (int)(decimal.Ceiling((decimal)(page.RecordCount * 1.0 / page.PageSize)));

                    if (page.PageCount <= 0) page.PageCount = 1;
                    if (page.PageIndex <= 1) page.PageIndex = 1;
                    if (page.PageIndex >= page.PageCount) page.PageIndex = page.PageCount;

                    int startIndex = (page.PageIndex - 1) * page.PageSize + 1;
                    int endIndex = startIndex + page.PageSize - 1;
                    if (endIndex > page.RecordCount) endIndex = page.RecordCount;

                    if (Command.CommandText.Contains("Limit") && oql.ChildOql == null)
                        throw new NORMException(ExceptionType.DataBaseExceptoin, "分页查询中不能再含有Limit");


                    string SQL = "SELECT * FROM ( ";
                    if (oql.Orderbies != null)
                    {
                        SQL += "SELECT row_number() OVER ( ORDER BY ";
                        foreach (object o in oql.Orderbies)
                        {
                            string s = o.ToString();
                            //string[] array = o.ToString().Split(' ');
                            //string field = array[0] + "";
                            //string orderby = array[1] + "";

                            string f = string.Empty;
                            string b = string.Empty;
                            if (s.EndsWith("DESC"))
                            {
                                b = "DESC";
                                f = "[" + s.Replace("DESC", "").Trim() + "]";
                            }
                            else if (s.EndsWith("ASC"))
                            {
                                b = "ASC";
                                f = "[" + s.Replace("ASC", "").Trim() + "]";
                            }
                            else if (s.EndsWith("Asc"))
                            {
                                b = "Asc";
                                f = "[" + s.Replace("Asc", "").Trim() + "]";
                            }
                            else if (s.EndsWith("Desc"))
                            {
                                b = "Desc";
                                f = "[" + s.Replace("Desc", "").Trim() + "]";
                            }
                            else if (s.EndsWith("asc"))
                            {
                                b = "asc";
                                f = "[" + s.Replace("asc", "").Trim() + "]";
                            }
                            else if (s.EndsWith("desc"))
                            {
                                b = "desc";
                                f = "[" + s.Replace("desc", "").Trim() + "]";
                            }

                            SQL += f + " " + b + ",";

                        }
                        SQL = SQL.TrimEnd(',') + " ) AS rowId";
                    }
                    else
                    {
                        SQL += "SELECT row_number() OVER ( ORDER BY ";
                        foreach (string s in oql.Modelobject.PrimaryKey)
                        {
                            string f = string.Empty;
                            string b = string.Empty;

                            b = "ASC";
                            f = "[" + s.ToString() + "]";
                            SQL += f + " " + b + ",";

                        }
                        SQL = SQL.TrimEnd(',') + " ) AS rowId";
                    }

                    SQL += "," + _TEMPSQL.Replace("SELECT", " ");
                    SQL += " ) _LY WHERE rowId>=" + startIndex + " AND rowId<=" + endIndex + " ;";

                    Command.CommandText = SQL;

                }                

                if (TransactionBegin)
                    dt = sqlserver.GetDataTable(conn, CommandType.Text, Command.CommandText, Command.Parameters);
                else
                    dt = sqlserver.GetDataTable(this._connectionString, CommandType.Text, Command.CommandText, Command.Parameters);

            }
            catch (Exception ex)
            {
                throw ex;
            }
            return dt;
        }

        #endregion

        #region 数据库维护

        /// <summary>
        /// 创建数据库
        /// </summary>
        /// <param name="DataBaseName">数据库名称</param>
        /// <param name="FilePath">数据库文件存放位置</param>
        /// <param name="InitSize">初始化大小</param>
        /// <param name="MaxSize">数据库文件最大 -1 不限制大小</param>
        /// <param name="Growth">增长量 默认百分比</param>
        /// <param name="GrowthType">增长单位 默认百分比</param>
        /// <param name="Server">服务器IP地址</param>
        /// <param name="Port">网络端口号</param>
        /// <param name="UserId">数据库登陆帐户</param>
        /// <param name="Password">数据库登陆密码</param>
        /// <param name="Message">返回信息</param>
        public override void CreateDataBase(string DataBaseName, string FilePath, decimal InitSize, decimal MaxSize, decimal Growth, string GrowthType,
            string Server, string Port, string UserId, string Password, out string Message)
        {
            Message = string.Empty;
            string connectionString;
            if (Server.Contains("Local"))
            {
                connectionString = "Data Source=" + Server + ";Initial Catalog=master;Integrated Security=SSPI;";
            }
            else
            {
                string fullServer = Server;
                if (!string.IsNullOrEmpty(Port))
                {
                    fullServer += "," + Port;
                }
                connectionString = "Data Source=" + fullServer + ";Initial Catalog=master;User ID=" + UserId + ";Pwd=" + Password + ";";
            }

            if (FilePath.Contains("\\"))
                FilePath = FilePath.Substring(0, FilePath.LastIndexOf("\\"));

            using (NORM.DataBase.DataBase db = NORM.DataBase.DataBaseFactory.Create(connectionString, NORM.DataBase.DataBaseTypes.SqlDataBase))
            {
                try
                {
                    StringBuilder sqlsb = new StringBuilder();
                    sqlsb.Append("declare @cmd nvarchar(4000) ").Append(System.Environment.NewLine);                   
                    sqlsb.Append("exec sp_configure 'show advanced options', 1	--允许配置高级选项 ").Append(System.Environment.NewLine);
                    sqlsb.Append("reconfigure	--重新配置 ").Append(System.Environment.NewLine);
                    sqlsb.Append("exec sp_configure 'xp_cmdshell', 1	--启用xp_cmdshell ").Append(System.Environment.NewLine);
                    sqlsb.Append("reconfigure	--重新配置 ").Append(System.Environment.NewLine);
                    sqlsb.Append("set @cmd = 'mkdir " + FilePath + "' ").Append(System.Environment.NewLine);
                    sqlsb.Append("exec xp_cmdshell @cmd ").Append(System.Environment.NewLine);                     
                    sqlsb.Append("exec sp_configure 'xp_cmdshell', 0	--执行完成后出于安全考虑可以将xp_cmdshell关闭 ").Append(System.Environment.NewLine);
                    sqlsb.Append("reconfigure	--重新配置 ").Append(System.Environment.NewLine);
                    sqlsb.Append("exec sp_configure 'show advanced options', 0	--不允许配置高级选项 ").Append(System.Environment.NewLine);
                    sqlsb.Append("reconfigure	--重新配置 ").Append(System.Environment.NewLine);
                    var result = db.Execute(CommandType.Text, sqlsb.ToString(), null);
                }
                catch (Exception ex)
                {
                    Message = ex.Message;
                }
            }

            if (!System.IO.Directory.Exists(FilePath))
                System.IO.Directory.CreateDirectory(FilePath);

            using (NORM.DataBase.DataBase db = NORM.DataBase.DataBaseFactory.Create(connectionString, NORM.DataBase.DataBaseTypes.SqlDataBase))
            {
                try
                {
                    StringBuilder sqlsb = new StringBuilder();

                    sqlsb.Append("CREATE DATABASE " + DataBaseName + " ");
                    sqlsb.Append("ON  PRIMARY ").Append(System.Environment.NewLine);
                    sqlsb.Append("(").Append(System.Environment.NewLine);

                    sqlsb.Append("  name='" + DataBaseName + "',  -- 主数据文件的逻辑名称").Append(System.Environment.NewLine);
                    sqlsb.Append("  filename='" + FilePath + "\\" + DataBaseName + "_data.mdf', -- 主数据文件的物理名称").Append(System.Environment.NewLine);
                    sqlsb.Append("  size=" + InitSize + "mb, --主数据文件的初始大小").Append(System.Environment.NewLine);

                    if (MaxSize > 0)
                    {
                        sqlsb.Append("  maxsize=" + MaxSize + "mb, -- 主数据文件增长的最大值").Append(System.Environment.NewLine);
                    }

                    sqlsb.Append("  filegrowth=" + Growth.ToString("00") + ""+GrowthType+" --主数据文件的增长率").Append(System.Environment.NewLine);

                    sqlsb.Append(")").Append(System.Environment.NewLine);

                    sqlsb.Append("LOG ON").Append(System.Environment.NewLine);
                    sqlsb.Append("(").Append(System.Environment.NewLine);

                    sqlsb.Append("  name='" + DataBaseName + "_log',").Append(System.Environment.NewLine);
                    sqlsb.Append("  filename='" + FilePath + "\\" + DataBaseName + "_log.ldf',").Append(System.Environment.NewLine);
                    sqlsb.Append("  size=2mb,").Append(System.Environment.NewLine);
                    sqlsb.Append("  filegrowth=1mb").Append(System.Environment.NewLine);

                    sqlsb.Append(")").Append(System.Environment.NewLine);

                    var result = db.Execute(CommandType.Text, sqlsb.ToString(), null);

                    Message = "操作成功";

                }
                catch (Exception ex)
                {
                    Message = ex.Message;
                    return;
                }
            }
        }

        public override int GetLastCommandExcuteRowCount()
        {
            int val = -1;

            string sqlText = "select @@ROWCOUNT as ExecuteRowCount";

            DataSet ds = this.QueryDataSet(CommandType.Text, sqlText, null);

            if (ds.Tables.Count > 0)
            {
                val = Convert.ToInt32(ds.Tables[0].Rows[0][0]);
            }

            return val;
        }

        #endregion

        #region 参数转换

        private DbParameter[] ParametersTransport(OsqlParameter[] cmdParms)
        {
            SqlParameter[] parameters = new SqlParameter[cmdParms.Length];

            for (int i = 0; i < cmdParms.Length; i++)
            {
                OsqlParameter parameter = cmdParms[i];
                parameters[i] = new SqlParameter(parameter.Name, parameter.Value);
                SqlDbType parameterDbType = SqlDbType.VarChar;
                switch (parameter.DataType)
                {
                    case OsqlParameterDataType.VarChar:
                    case OsqlParameterDataType.String:
                        parameterDbType = SqlDbType.VarChar;
                        break;
                    case OsqlParameterDataType.Char:
                        parameterDbType = SqlDbType.Char;
                        break;
                    case OsqlParameterDataType.Int:
                        parameterDbType = SqlDbType.Int;
                        break;
                    case OsqlParameterDataType.Integer:
                        parameterDbType = SqlDbType.BigInt;
                        break;
                    case OsqlParameterDataType.Bit:
                        parameterDbType = SqlDbType.Bit;
                        break;                    
                    case OsqlParameterDataType.Guid:
                        parameterDbType = SqlDbType.UniqueIdentifier;
                        break;
                    case OsqlParameterDataType.Numeric:
                        parameterDbType = SqlDbType.Decimal;
                        break;
                    case OsqlParameterDataType.DateTime:
                        parameterDbType = SqlDbType.DateTime;
                        break;
                    case OsqlParameterDataType.DateTime2:
                        parameterDbType = SqlDbType.DateTime2;
                        break;
                    case OsqlParameterDataType.Date:
                        parameterDbType = SqlDbType.Date;
                        break;
                    case OsqlParameterDataType.Time:
                        parameterDbType = SqlDbType.Time;
                        break;
                    case OsqlParameterDataType.Text:
                        parameterDbType = SqlDbType.Text;
                        break;
                    case OsqlParameterDataType.Byte:
                        parameterDbType = SqlDbType.Binary;
                        break;
                    case OsqlParameterDataType.Stream:
                        parameterDbType = SqlDbType.Image;
                        break;
                }
                parameters[i].SqlDbType = parameterDbType;
                parameters[i].Direction = parameter.Direction;
            }
            return parameters;
        }

        #endregion

    }
}
