﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NORM.Logger
{
    /// <summary>
    /// 
    /// </summary>
    public class LoggerHelper
    {
        private int level = 0;
        private string loggerTagInfo = "NORM.Core";
        private bool WriteToLine(string Icon, string Msg, string Tag, string Trace = "")
        {
            bool rvl = false;

            try
            {
                string logfile = string.Empty;
                string txtPath = string.Empty;

                string logpathYear = DateTime.Now.Year.ToString("0000");
                string logpathMonth = DateTime.Now.ToString("yyyyMM");
                string rootPath = AppDomain.CurrentDomain.BaseDirectory.TrimEnd('\\');

                switch (level)
                {
                    case 0:

                        logfile = loggerTagInfo + ".Log";

                        txtPath = rootPath + "//" + logfile + ".txt";

                        break;

                    case 1:

                        logfile = DateTime.Now.ToString("yyyy");

                        if (!System.IO.Directory.Exists(rootPath + "//log"))
                            System.IO.Directory.CreateDirectory(rootPath + "//log");

                        txtPath = rootPath + "//log//" + logpathYear + ".txt";

                        break;

                    case 2:

                        logfile = DateTime.Now.ToString("yyyyMM");

                        if (!System.IO.Directory.Exists(rootPath + "//log"))
                            System.IO.Directory.CreateDirectory(rootPath + "//log");

                        if (!System.IO.Directory.Exists(rootPath + "//log//" + logpathYear))
                            System.IO.Directory.CreateDirectory(rootPath + "//log//" + logpathYear);


                        txtPath = rootPath + "//log//" + logpathYear + "//" + logpathMonth + ".txt";


                        break;

                    case 3:

                        logfile = DateTime.Now.ToString("yyyyMMdd");

                        if (!System.IO.Directory.Exists(rootPath + "//log"))
                            System.IO.Directory.CreateDirectory(rootPath + "//log");

                        if (!System.IO.Directory.Exists(rootPath + "//log//" + logpathYear))
                            System.IO.Directory.CreateDirectory(rootPath + "//log//" + logpathYear);

                        if (!System.IO.Directory.Exists(rootPath + "//log//" + logpathYear + "//" + logpathMonth))
                            System.IO.Directory.CreateDirectory(rootPath + "//log//" + logpathYear + "//" + logpathMonth);

                        txtPath = rootPath + "//log//" + logpathYear + "//" + logpathMonth + "//" + logfile + ".txt";

                        break;
                }



                if (!System.IO.File.Exists(txtPath))
                {
                    using (System.IO.FileStream fs = System.IO.File.Create(txtPath))
                    {
                        using (System.IO.StreamWriter sw = new System.IO.StreamWriter(fs, System.Text.Encoding.UTF8))
                        {
                            sw.WriteLine("日志文件创建于：{0} By {1}", DateTime.Now.ToString("yyyy年MM月dd日hh时mm分ss秒"), loggerTagInfo);
                            sw.WriteLine("-------------------------------------------------------------");
                            sw.Flush();
                        }
                        fs.Close();
                    }
                    WriteToLine(Icon, Msg, Tag);
                }
                else
                {
                    using (System.IO.StreamWriter sw = System.IO.File.AppendText(txtPath))
                    {
                        //sw.WriteLine("");
                        sw.WriteLine("{0}：{1} 位置：{2} 时间：{3}", Icon, Msg, Tag, DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss.fff"));

                        if (!string.IsNullOrEmpty(Trace))
                            sw.WriteLine("跟踪：{0}", Trace);
                        
                        sw.Flush();
                    }
                }

                rvl = true;

            }
            catch (Exception ex)
            {
                System.Threading.Thread.Sleep(100);
                WriteToLine(Icon, Msg, Tag, Trace);
            }

            return rvl;
        }
       
        private LoggerHelper()
        {
            //
        }

        public LoggerHelper(string TagInfo)
        {
            this.loggerTagInfo = TagInfo;
        }

        public void SetLevel(int level)
        {
            this.level = level;
        }

        private static object lockobject;
        private static LoggerHelper _instantiation;
        internal static LoggerHelper Instantiation
        {
            get {

                if (_instantiation == null)
                {
                    lockobject = new object();
                    lock (lockobject)
                    {
                        _instantiation = new LoggerHelper();
                    }
                }
                return _instantiation;
            
            }
        }

        /// <summary>
        /// 记录跟踪信息
        /// </summary>
        /// <param name="Msg"></param>
        /// <param name="Tag"></param>
        public static void Track(string Msg, string Tag)
        {
            Instantiation.WriteToLine("跟踪", Msg, Tag);
        }

        /// <summary>
        /// 记录调试信息
        /// </summary>
        /// <param name="Msg"></param>
        /// <param name="Tag"></param>
        public static void Debug(string Msg, string Tag)
        {
            Instantiation.WriteToLine("调试", Msg, Tag);
        }

        public static void Error(string Msg, string Tag, string Trace)
        {
            Instantiation.WriteToLine("错误", Msg, Tag, Trace);
        }

    }
}
