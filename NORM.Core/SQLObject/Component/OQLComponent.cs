﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NORM.SQLObject
{
    public class OQLComponent
    {
        private string _key;
        public string Key
        {
            set { _key = value; }
            get { return _key; }
        }

        private string _guid;
        public string Guid
        {
            set { _guid = value; }
            get { return Guid; }
        }
    }
}
