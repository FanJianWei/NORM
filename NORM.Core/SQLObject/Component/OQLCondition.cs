﻿using System;
using System.Collections.Generic;
using System.Text;
using NORM.Common;
using NORM.Entity;

namespace NORM.SQLObject
{
    public class OQLCondition : OQLComponent
    {
        protected OQL currentOql;

        private OQLCondition()
        {
            this.Key = "OQLCondition";
            this.Guid = "b6881d49-ada8-4693-a361-f0d344c50c51";
        }

        internal OQLCondition(OQL oql)
            : this()
        {
            this.currentOql = oql;
        }

        private bool ContainsKey(List<Condition> source, string key)
        {
            bool rvl = false;
            foreach (Condition c in source)
            {
                if (key.Equals(c.Field))
                {
                    rvl = true;
                    return rvl;
                }
            }
            return rvl;
        }

        private bool ContainsItem(List<Condition> source, Condition item)
        {
            return source.Contains(item);
        }

        public bool Compare(object Field, string CompareType, object Value)
        {
            if (currentOql != null)
            {
                string fieldstring = Field + "";
                if (currentOql.fieldStack.Count > 0)
                {
                    TableField tf = currentOql.fieldStack.Pop();
                    fieldstring = tf.Entity.TableName + "." + tf.Name;
                }

                if (!currentOql._join && fieldstring.Contains("."))
                {
                    fieldstring = fieldstring.Substring(fieldstring.LastIndexOf(".") + 1);
                }

                Condition condition = new Condition();
                condition.Field = fieldstring;
                condition.Comparison = CompareType;
                condition.Value = Value;

                List<Condition> conditions = ArrayPlus.ToList<Condition>(currentOql.Conditions);

                if (!ContainsItem(conditions, condition))
                    conditions.Add(condition);                

                currentOql.Where(conditions.ToArray());

                return true;

            }
            return false;
        }

        public bool Compare(object Field, string CompareType, object Value, string Relation)
        {
            if (currentOql != null)
            {
                string fieldstring = Field + "";
                if (currentOql.fieldStack.Count > 0)
                {
                    TableField tf = currentOql.fieldStack.Pop();
                    fieldstring = tf.Entity.TableName + "." + tf.Name;
                }

                if (!currentOql._join && fieldstring.Contains("."))
                {
                    fieldstring = fieldstring.Substring(fieldstring.LastIndexOf(".") + 1);
                }

                Condition condition = new Condition();
                condition.Field = fieldstring;
                condition.Relation = Relation;
                condition.Comparison = CompareType;
                condition.Value = Value;

                List<Condition> conditions = ArrayPlus.ToList<Condition>(currentOql.Conditions);

                if (!ContainsItem(conditions, condition))
                    conditions.Add(condition);

                currentOql.Where(conditions.ToArray());

                return true;

            }
            return false;
        }

        public bool IsNotNull(object Field)
        {
            if (currentOql != null)
            {
                string fieldstring = Field + "";
                if (currentOql.fieldStack.Count > 0)
                {
                    TableField tf = currentOql.fieldStack.Pop();
                    fieldstring = tf.Entity.TableName + "." + tf.Name;
                }

                if (!currentOql._join && fieldstring.Contains("."))
                {
                    fieldstring = fieldstring.Substring(fieldstring.LastIndexOf(".") + 1);
                }

                Condition condition = new Condition();
                condition.Field = fieldstring;
                condition.Relation = "AND";
                condition.Comparison = "IS Not";
                condition.Value = DataValue.DBNull;

                List<Condition> conditions = ArrayPlus.ToList<Condition>(currentOql.Conditions);
                conditions.Add(condition);

                currentOql.Where(conditions.ToArray());

                return true;

            }

            return false;
        }
        
        public bool IsNull(object Field)
        {
            if (currentOql != null)
            {
                string fieldstring = Field + "";
                if (currentOql.fieldStack.Count > 0)
                {
                    TableField tf = currentOql.fieldStack.Pop();
                    fieldstring = tf.Entity.TableName + "." + tf.Name;
                }

                if (!currentOql._join && fieldstring.Contains("."))
                {
                    fieldstring = fieldstring.Substring(fieldstring.LastIndexOf(".") + 1);
                }

                Condition condition = new Condition();
                condition.Field = fieldstring;
                condition.Relation = "AND";
                condition.Comparison = "IS";
                condition.Value = DataValue.DBNull;

                List<Condition> conditions = ArrayPlus.ToList<Condition>(currentOql.Conditions);
                conditions.Add(condition);

                currentOql.Where(conditions.ToArray());

                return true;

            }

            return false;
        }

    }
}
