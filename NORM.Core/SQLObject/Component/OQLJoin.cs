﻿using System;
using System.Text;

using NORM.Common;
using NORM.DataBase;
using NORM.Entity;

using System.Collections.Generic;
using System.Reflection;
using System.Diagnostics;

namespace NORM.SQLObject
{
    public class OQLJoin : OQLComponent
    {
        private OQL _mainOql;
        internal OQL mainOql
        {
            get { return _mainOql; }
            set { _mainOql = value; }
        }

        public OQLJoin(EntityBase model)
        {
            this.Key = "JoinEntity";
            this.Guid = "e509bc64-4d71-477b-be9b-93776b354ab5";
        }

        public OQL ON(object Field1, object Field2)
        {
            lock (Locker.instance)
            {
                string field1 = Field1 + "";
                string field2 = Field2 + "";

                if (this._mainOql.fieldStack.Count > 1)
                {
                    TableField tf1 = this._mainOql.fieldStack.Pop();
                    TableField tf2 = this._mainOql.fieldStack.Pop();

                    field1 = tf2.Entity.TableName + "." + tf2.Name;
                    field2 = tf1.Entity.TableName + "." + tf1.Name;
                }

                return _mainOql.JoinOnParse(field1, field2);
            }
        }

    }
}
