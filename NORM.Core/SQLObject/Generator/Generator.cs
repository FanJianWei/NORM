﻿using System;
using System.Data;

using NORM.Common;

namespace NORM.SQLObject
{
    using NORM.DataBase;

    /// <summary>
    /// 生成器
    /// </summary>
    public class Generator
    {
        public static void BuildEntity(DataBase db, string name, string spacename, string snamespace, out string result, DataBaseSchema schema = DataBaseSchema.Table)
        {
            DataTable dt = db.GetTableObject(name, schema);

            string entityType = string.Empty;
            switch (schema)
            {
                case DataBaseSchema.Table: entityType = "Table"; break;
                case DataBaseSchema.View: entityType = "View"; break;
                default: break;
            }

            StringPlus rvl = new StringPlus();
            rvl.Append("using System;").Append("\r\n").Append("\r\n");
            //rvl.Append("using NORM.Common;").Append("\r\n");
            rvl.Append("using NORM.Entity;").Append("\r\n").Append("\r\n");
            rvl.Append("namespace " + spacename + "."+snamespace+".Models").Append("\r\n");
            rvl.Append("{").Append("\r\n");
            rvl.Append("    ///名称：" + name + "").Append("\r\n");
            rvl.Append("    ///作者：wxdong").Append("\r\n");
            rvl.Append("    ///创建时间：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm") + " ").Append("\r\n");
            rvl.Append("    [Serializable]").Append("\r\n");
            rvl.Append("    [Describe(\"" + entityType + "\")]").Append("\r\n");
            rvl.Append("    public class " + name + " : EntityBase").Append("\r\n");
            rvl.Append("    {").Append("\r\n");
            rvl.Append("        public " + name + "()").Append("\r\n");
            rvl.Append("        {").Append("\r\n");
            rvl.Append("            TableName = \"" + name + "\";").Append("\r\n");
            rvl.Append("            PrimaryKey.Add(\"$PrimaryKey$\");").Append("\r\n");
            rvl.Append("        }").Append("\r\n");
            rvl.Append("        #region Model").Append("\r\n");

            string PrimaryKey = string.Empty;
            foreach (DataRow dr in dt.Rows)
            {
                string column_name = dr["ColumnName"] + "";
                string column_type = dr["TypeName"] + "";
                string column_length = dr["Length"] + "";
                string column_describ = dr["Describ"] + "";
                string column_isnull = dr["CisNull"] + "";
                string column_identify = string.Empty;
                string column_attrdescrib = string.Empty;

                switch (column_type)
                {
                    case "char":
                    case "nchar":
                    case "varchar":
                    case "nvarchar":
                    case "text":
                    case "ntext":
                        column_type = "string";
                        break;
                    case "real":
                    case "money":
                    case "decimal":
                        column_type = "decimal";
                        break;
                    case "date":
                    case "smalldate":
                        column_type = "string";
                        column_attrdescrib = "Type::Sdt";
                        break;
                    case "datetime":
                    case "timestamp with time zone":
                        column_type = "DateTime";
                        break;
                    case "long":
                    case "integer":
                        column_type = "Int64";
                        break;
                    case "image":
                    case "byte[]":
                        column_type = "byte[]";
                        break;
                    case "int":
                    case "int16":
                    case "int32":
                        column_type = "int";
                        break;
                    case "double":
                        column_type = "double";
                        break;
                    case "float":
                        column_type = "float";
                        break;
                    case "guid":
                    case "uuid":
                        column_type = "Guid?";
                        break;
                    case "character":
                        column_type = "char";
                        break;
                    case "bit":
                    case "bit(1)":
                        column_attrdescrib = "Type::Bit";
                        column_type = "bool";
                        break;
                    case "bool":
                        column_type = "bool";
                        break;
                    default:
                        column_type = "string";
                        break;
                }

                if (column_isnull.Contains("pk"))
                {
                    PrimaryKey = column_name;
                }

                if ((column_isnull.Contains("identify") || column_isnull.Contains("pk")) && (column_type == "Int64" || column_type == "int"))
                {
                    column_identify = "[AtttribDescrib(\"Identify\")]";
                }

                //rvl.Append("        private " + column_type + " _" + column_name.ToLower() + ";").Append("\r\n");
                rvl.Append("        /// <summary>").Append("\r\n");
                rvl.Append("        /// " + column_describ + "").Append("\r\n");
                rvl.Append("        /// </summary>").Append("\r\n");
                if (!string.IsNullOrEmpty(column_identify))
                {
                    rvl.Append("        [Describe(\"Identify\")]").Append("\r\n");
                }
                if (!string.IsNullOrEmpty(column_attrdescrib))
                {
                    rvl.Append("        [Describe(\"" + column_attrdescrib + "\")]").Append("\r\n");
                }
                rvl.Append("        public " + column_type + " " + column_name + "").Append("\r\n");
                rvl.Append("        {").Append("\r\n");
                //rvl.Append("            get { return _" + column_name.ToLower() + "; }").Append("\r\n");
                //rvl.Append("            set { _" + column_name.ToLower() + " = value; }").Append("\r\n");
                rvl.Append("              get{ return getProperty<" + column_type + ">(\"" + column_name + "\");}").Append("\r\n");
                rvl.Append("              set{ setProperty(\"" + column_name + "\", value);}").Append("\r\n");
                rvl.Append("        }").Append("\r\n");

            }

            rvl.Append("        #endregion Model").Append("\r\n");
            rvl.Append("    }").Append("\r\n");
            rvl.Append("}").Append("\r\n");

            result = rvl.Value.Replace("$PrimaryKey$", PrimaryKey);

        }

        public static void BuildModel(DataBase db, string name, string spacename, string snamespace, out string result, DataBaseSchema schema = DataBaseSchema.Table)
        {
            DataTable dt = db.GetTableObject(name, schema);

            string entityType = string.Empty;
            switch (schema)
            {
                case DataBaseSchema.Table: entityType = "Table"; break;
                case DataBaseSchema.View: entityType = "View"; break;
                default: break;
            }

            StringPlus rvl = new StringPlus();
            rvl.Append("using System;").Append("\r\n").Append("\r\n");
            //rvl.Append("using NORM.Common;").Append("\r\n");
            //rvl.Append("using NORM.Entity;").Append("\r\n").Append("\r\n");
            rvl.Append("namespace " + spacename + "."+snamespace+".Models").Append("\r\n");
            rvl.Append("{").Append("\r\n");
            rvl.Append("    ///名称：" + name + "").Append("\r\n");
            rvl.Append("    ///作者：wxdong").Append("\r\n");
            rvl.Append("    ///创建时间：" + DateTime.Now.ToString("yyyy-MM-dd HH:mm") + " ").Append("\r\n");
            rvl.Append("    [Serializable]").Append("\r\n");
            //rvl.Append("    [AtttribDescrib(\"" + entityType + "\")]").Append("\r\n");
            rvl.Append("    public class " + name + "").Append("\r\n");
            rvl.Append("    {").Append("\r\n");
            rvl.Append("        public " + name + "()").Append("\r\n");
            rvl.Append("        {").Append("\r\n");
            //rvl.Append("            TableName = \"" + name + "\";").Append("\r\n");
            //rvl.Append("            PrimaryKey.Add(\"$PrimaryKey$\");").Append("\r\n");
            rvl.Append("        }").Append("\r\n");
            rvl.Append("        #region Model").Append("\r\n");

            string PrimaryKey = string.Empty;
            foreach (DataRow dr in dt.Rows)
            {
                string column_name = dr["ColumnName"] + "";
                string column_type = dr["TypeName"] + "";
                string column_length = dr["Length"] + "";
                string column_describ = dr["Describ"] + "";
                string column_isnull = dr["CisNull"] + "";
                string column_identify = string.Empty;
                string column_attrdescrib = string.Empty;

                switch (column_type)
                {
                    case "char":
                    case "nchar":
                    case "varchar":
                    case "nvarchar":
                    case "text":
                    case "ntext":
                        column_type = "string";
                        break;
                    case "real":
                    case "money":
                    case "decimal":
                        column_type = "decimal";
                        break;
                    case "date":
                    case "smalldate":
                        column_type = "string";
                        column_attrdescrib = "Type::Sdt";
                        break;
                    case "datetime":
                    case "timestamp with time zone":
                        column_type = "DateTime";
                        break;
                    case "long":
                    case "integer":
                        column_type = "Int64";
                        break;
                    case "image":
                    case "byte[]":
                        column_type = "byte[]";
                        break;
                    case "int":
                    case "int16":
                    case "int32":
                        column_type = "int";
                        break;
                    case "double":
                        column_type = "double";
                        break;
                    case "float":
                        column_type = "float";
                        break;
                    case "guid":
                    case "uuid":
                        column_type = "Guid?";
                        break;
                    case "character":
                        column_type = "char";
                        break;
                    case "bit":
                    case "bit(1)":
                        column_attrdescrib = "Type::Bit";
                        column_type = "bool";
                        break;
                    case "bool":
                        column_type = "bool";
                        break;
                    default:
                        column_type = "string";
                        break;
                }

                if (column_isnull.Contains("pk"))
                {
                    PrimaryKey = column_name;
                }

                if ((column_isnull.Contains("identify") || column_isnull.Contains("pk")) && (column_type == "Int64" || column_type == "int"))
                {
                    column_identify = "[AtttribDescrib(\"Identify\")]";
                }
                 
                rvl.Append("        private " + column_type + " _" + column_name.ToLower() + ";").Append("\r\n");
                rvl.Append("        /// <summary>").Append("\r\n");
                rvl.Append("        /// " + column_describ + "").Append("\r\n");
                rvl.Append("        /// </summary>").Append("\r\n");
                //if (!string.IsNullOrEmpty(column_identify))
                //{
                //    rvl.Append("        [AtttribDescrib(\"Identify\")]").Append("\r\n");
                //}
                //if (!string.IsNullOrEmpty(column_attrdescrib))
                //{
                //    rvl.Append("        [AtttribDescrib(\"" + column_attrdescrib + "\")]").Append("\r\n");
                //}                
                rvl.Append("        public " + column_type + " " + column_name + "").Append("\r\n");
                rvl.Append("        {").Append("\r\n");
                //rvl.Append("            get { return _" + column_name.ToLower() + "; }").Append("\r\n");
                //rvl.Append("            set { _" + column_name.ToLower() + " = value; }").Append("\r\n");
                rvl.Append("              get{ return _" + column_name.ToLower() + ";}").Append("\r\n");
                rvl.Append("              set{ _" + column_name.ToLower() + "=value;}").Append("\r\n");
                rvl.Append("        }").Append("\r\n");

            }

            rvl.Append("        #endregion Model").Append("\r\n");
            rvl.Append("    }").Append("\r\n");
            rvl.Append("}").Append("\r\n");

            result = rvl.Value.Replace("$PrimaryKey$", PrimaryKey);

        }

        public static void BuildEntityDAL(DataBase db, string name, string spacename,string snamespace, out string result, DataBaseSchema schema = DataBaseSchema.Table)
        {
            DataTable dt = db.GetTableObject(name, schema);

            string coltype = string.Empty;
            string colname = string.Empty;
            foreach (DataRow row in dt.Rows)
            {
                string CisNull = row["CisNull"] + "";
                if (CisNull.Contains("identity"))
                {
                    colname = row["ColumnName"] + "";
                    coltype = row["TypeName"] + "";
                }
                if (CisNull.Contains("pk"))
                {
                    colname = row["ColumnName"] + "";
                    coltype = row["TypeName"] + "";
                }
            }

            switch (coltype)
            {
                case "number":              
                case "integer": coltype = "int"; break;
                case "guid":
                default: coltype = "string"; break;                 
            }

            string entityType = string.Empty;
            switch (schema)
            {
                case DataBaseSchema.Table: entityType = "Table"; break;
                case DataBaseSchema.View: entityType = "View"; break;
                default: break;
            }

            StringPlus rvl = new StringPlus();
            rvl.Append("using System;").Append("\r\n");
            rvl.Append("using NORM.Entity;").Append("\r\n");            
            rvl.Append("using NORM.DataBase;").Append("\r\n").Append("\r\n");
            rvl.Append("using " + spacename + "."+snamespace+".Models;").Append("\r\n").Append("\r\n");

            rvl.Append("namespace " + spacename + "." + snamespace + ".DAL").Append("\r\n");
            rvl.Append("{").Append("\r\n");
            rvl.Append("    public class " + name + "DAL").Append("\r\n");
            rvl.Append("    {").Append("\r\n");

            rvl.Append("        /// <summary>").Append("\r\n");
            rvl.Append("        ///获取实体").Append("\r\n");
            rvl.Append("        /// </summary>").Append("\r\n");
            rvl.Append("        public " + spacename + "." + snamespace + ".Models." + name + " GetModel(" + coltype + " " + colname + ")").Append("\r\n");
            rvl.Append("        {").Append("\r\n");
            rvl.Append("            using(var db = DataBaseFactory.Create())").Append("\r\n");
            rvl.Append("            {").Append("\r\n");
            rvl.Append("                " + spacename + "." + snamespace + ".Models." + name + " model = new " + spacename + "." + snamespace + ".Models." + name + "();").Append("\r\n");
            rvl.Append("                OQL oql = OQL.From(model).Where(w => w.Compare(model." + colname + ", \"=\", " + colname + "))").Append("\r\n");
            rvl.Append("                .Select().End;").Append("\r\n");
            //rvl.Append("                model = db.QueryObject<Models." + name + ">(oql);").Append("\r\n");
            rvl.Append("                EntityQuery<Models.T_Accounts>.Instance.QueryObject(oql,db);").Append("\r\n");
            rvl.Append("                return model;").Append("\r\n");
            rvl.Append("            }").Append("\r\n");
            rvl.Append("        }").Append("\r\n");

            rvl.Append("        /// <summary>").Append("\r\n");
            rvl.Append("        ///修改数据").Append("\r\n");
            rvl.Append("        /// </summary>").Append("\r\n");
            rvl.Append("        public void Update(" + spacename + "." + snamespace + ".Models." + name + " model)").Append("\r\n");
            rvl.Append("        {").Append("\r\n");
            rvl.Append("            using(var db = DataBaseFactory.Create())").Append("\r\n");
            rvl.Append("            {").Append("\r\n");
            rvl.Append("                db.BeginTransaction();").Append("\r\n");
            rvl.Append("                try{").Append("\r\n");
            rvl.Append("                    EntityQuery<" + spacename + "." + snamespace + ".Models." + name + ">.Instance.Update(model, db);").Append("\r\n");
            rvl.Append("                    db.Commit();").Append("\r\n");
            rvl.Append("                }").Append("\r\n");
            rvl.Append("                catch(Exception ex){").Append("\r\n");
            rvl.Append("                    db.RollBack();").Append("\r\n");
            rvl.Append("                    throw ex;").Append("\r\n");
            rvl.Append("                }").Append("\r\n");
            rvl.Append("            }").Append("\r\n");
            rvl.Append("        }").Append("\r\n");

            rvl.Append("        /// <summary>").Append("\r\n");
            rvl.Append("        ///添加数据").Append("\r\n");
            rvl.Append("        /// </summary>").Append("\r\n");
            rvl.Append("        public void Add(" + spacename + "." + snamespace + ".Models." + name + " model)").Append("\r\n");
            rvl.Append("        {").Append("\r\n");
            rvl.Append("            using(var db = DataBaseFactory.Create())").Append("\r\n");
            rvl.Append("            {").Append("\r\n");
            rvl.Append("                db.BeginTransaction();").Append("\r\n");
            rvl.Append("                try{").Append("\r\n");
            rvl.Append("                    EntityQuery<" + spacename + "." + snamespace + ".Models." + name + ">.Instance.Insert(model, db);").Append("\r\n");
            rvl.Append("                    db.Commit();").Append("\r\n");
            rvl.Append("                }").Append("\r\n");
            rvl.Append("                catch(Exception ex){").Append("\r\n");
            rvl.Append("                    db.RollBack();").Append("\r\n");
            rvl.Append("                    throw ex;").Append("\r\n");
            rvl.Append("                }").Append("\r\n");
            rvl.Append("            }").Append("\r\n");
            rvl.Append("        }").Append("\r\n");

            rvl.Append("        /// <summary>").Append("\r\n");
            rvl.Append("        ///获取数据").Append("\r\n");
            rvl.Append("        /// </summary>").Append("\r\n");
            rvl.Append("        public List<" + spacename + "." + snamespace + ".Models." + name + "> GetList(List<string> fields, List<Condition> conditions,List<string> orderby)").Append("\r\n");
            rvl.Append("        {").Append("\r\n");
            rvl.Append("            using(var db = DataBaseFactory.Create())").Append("\r\n");
            rvl.Append("            {").Append("\r\n");
            rvl.Append("                OQL oql = OQL.From(model)").Append("\r\n");
            rvl.Append("                 .Where(conditions.ToArray())").Append("\r\n");
            rvl.Append("                 .OrderBy(orderby.ToArray())").Append("\r\n");
            rvl.Append("                 .Select(fields.ToArray(), true)").Append("\r\n");
            rvl.Append("                 .End;").Append("\r\n");
            rvl.Append("                var list = EntityQuery<" + spacename + "." + snamespace + ".Models." + name + ">.QueryToList(oql,db);").Append("\r\n");
            rvl.Append("                return list;").Append("\r\n");
            rvl.Append("            }").Append("\r\n");
            rvl.Append("        }").Append("\r\n");


            rvl.Append("        /// <summary>").Append("\r\n");
            rvl.Append("        ///分页获取数据").Append("\r\n");
            rvl.Append("        ///PageLimit pager = new PageLimit();").Append("\r\n");
            rvl.Append("        ///pager.PageIndex = 1; //当前第1页").Append("\r\n");
            rvl.Append("        ///pager.PageSize = 5;  //每页显示5条数据").Append("\r\n");
            rvl.Append("        /// </summary>").Append("\r\n");
            rvl.Append("        public List<" + spacename + "." + snamespace + ".Models." + name + "> GetList(List<string> fields, List<Condition> conditions,List<string> orderby, PageLimit pager)").Append("\r\n");
            rvl.Append("        {").Append("\r\n");
            rvl.Append("            using(var db = DataBaseFactory.Create())").Append("\r\n");
            rvl.Append("            {").Append("\r\n");
            rvl.Append("                OQL oql = OQL.From(model)").Append("\r\n");
            rvl.Append("                 .Where(conditions.ToArray())").Append("\r\n");
            rvl.Append("                 .OrderBy(orderby.ToArray())").Append("\r\n");
            rvl.Append("                 .Select(fields.ToArray(), true)").Append("\r\n");
            rvl.Append("                 .End;").Append("\r\n");
            rvl.Append("                var list = EntityQuery<" + spacename + "." + snamespace + ".Models." + name + ">.QueryToList(oql,pager,db);").Append("\r\n");
            rvl.Append("                return list;").Append("\r\n");
            rvl.Append("            }").Append("\r\n");
            rvl.Append("        }").Append("\r\n");


            rvl.Append("    }").Append("\r\n");
            rvl.Append("}").Append("\r\n");
            result = rvl.Value;

        }

        public static void BuildEntityIDAL(DataBase db, string name, string spacename, string snamespace, out string result, DataBaseSchema schema = DataBaseSchema.Table)
        {
            StringPlus sp = new StringPlus();

            DataTable dt = db.GetTableObject(name, schema);

            string coltype = string.Empty;
            string colname = string.Empty;
            foreach (DataRow row in dt.Rows)
            {
                string CisNull = row["CisNull"] + "";
                if (CisNull.Contains("identity"))
                {
                    colname = row["ColumnName"] + "";
                    coltype = row["TypeName"] + "";
                }
                if (CisNull.Contains("pk"))
                {
                    colname = row["ColumnName"] + "";
                    coltype = row["TypeName"] + "";
                }
            }

            switch (coltype)
            {
                case "number":
                case "integer": coltype = "int"; break;
                case "guid":
                default: coltype = "string"; break;
            }

            sp.Append("using System;").Append("\r\n");
            sp.Append("using NORM.Entity;").Append("\r\n");
            sp.Append("using " + spacename + "."+snamespace+".Models;").Append("\r\n");
            sp.Append("\r\n");
            sp.Append("namespace " + spacename + "." + snamespace + ".DAL").Append("\r\n");
            sp.Append("{").Append("\r\n");
            sp.Append("     public interface " + name + "IDAL").Append("\r\n");
            sp.Append("     {").Append("\r\n");
            sp.Append("         /// <summary>").Append("\r\n");
            sp.Append("         /// 获取实体").Append("\r\n");
            sp.Append("         /// </summary>").Append("\r\n");
            sp.Append("         " + spacename + "." + snamespace + "." + name + " GetModel(" + coltype + " "+colname+");").Append("\r\n");
            sp.Append("         /// <summary>").Append("\r\n");
            sp.Append("         /// 修改数据").Append("\r\n");
            sp.Append("         /// </summary>").Append("\r\n");
            sp.Append("         void Update(" + spacename + "." + snamespace + "." + name + " model);").Append("\r\n");
            sp.Append("         /// <summary>").Append("\r\n");
            sp.Append("         /// 添加数据").Append("\r\n");
            sp.Append("         /// </summary>").Append("\r\n");
            sp.Append("         void Add(" + spacename + "." + snamespace + "." + name + " model);").Append("\r\n");
            sp.Append("         /// <summary>").Append("\r\n");
            sp.Append("         /// 获取数据").Append("\r\n");
            sp.Append("         /// </summary>").Append("\r\n");
            sp.Append("         List<" + spacename + "." + snamespace + "." + name + "> GetList(List<string> fields, List<Condition> conditions,List<string> orderby);").Append("\r\n");
            sp.Append("         /// <summary>").Append("\r\n");
            sp.Append("         /// 分页获取数据").Append("\r\n");
            sp.Append("         /// </summary>").Append("\r\n");
            sp.Append("         List<" + spacename + "." + snamespace + "." + name + "> GetList(List<string> fields, List<Condition> conditions,List<string> orderby,PageLimit pager);").Append("\r\n");
            sp.Append("     }").Append("\r\n");
            sp.Append("}").Append("\r\n");

            result = sp.Value;
        }

        public static void BuildEntityBLL(DataBase db, string name, string spacename, string snamespace, out string result, DataBaseSchema schema = DataBaseSchema.Table)
        {
            StringPlus sp = new StringPlus();

            DataTable dt = db.GetTableObject(name, schema);

            string coltype = string.Empty;
            string colname = string.Empty;
            foreach (DataRow row in dt.Rows)
            {
                string CisNull = row["CisNull"] + "";
                if (CisNull.Contains("identity"))
                {
                    colname = row["ColumnName"] + "";
                    coltype = row["TypeName"] + "";
                }
                if (CisNull.Contains("pk"))
                {
                    colname = row["ColumnName"] + "";
                    coltype = row["TypeName"] + "";
                }
            }

            switch (coltype)
            {
                case "number":
                case "integer": coltype = "int"; break;
                case "guid":
                default: coltype = "string"; break;
            }

            sp.Append("using System;").Append("\r\n");
            sp.Append("using "+spacename+"."+snamespace+".Models;").Append("\r\n");
            sp.Append("using " + spacename + "." + snamespace + ".IDAL;").Append("\r\n");
            sp.Append("\r\n").Append("\r\n");
            sp.Append("namespace " + spacename + "." + snamespace + ".BLL").Append("\r\n");
            sp.Append("{").Append("\r\n");
            sp.Append("     public class " + name + "BLL").Append("\r\n");
            sp.Append("     {").Append("\r\n");
            sp.Append("         private "+name+"IDAL dal;").Append("\r\n");
            
            
            sp.Append("         public " + name + "()").Append("\r\n");
            sp.Append("         {").Append("\r\n");
            sp.Append("             dal=new " + spacename + "." + snamespace + "." + name + "DAL();").Append("\r\n");
            sp.Append("         }").Append("\r\n").Append("\r\n");


            sp.Append("         /// <summary>").Append("\r\n");
            sp.Append("         /// 获取对象").Append("\r\n");
            sp.Append("         /// </summary>").Append("\r\n");
            sp.Append("         public " + spacename + "." + snamespace + "." + name + " GetModel(" + coltype + " "+colname+")").Append("\r\n");
            sp.Append("         {").Append("\r\n");
            sp.Append("             return dal.GetModel("+colname+");").Append("\r\n");
            sp.Append("         }").Append("\r\n").Append("\r\n");


            sp.Append("         /// <summary>").Append("\r\n");
            sp.Append("         /// 修改数据").Append("\r\n");
            sp.Append("         /// </summary>").Append("\r\n");
            sp.Append("         public void Update(" + spacename + "." + snamespace + "." + name + "  model)").Append("\r\n");
            sp.Append("         {").Append("\r\n");
            sp.Append("             dal.Update(model);").Append("\r\n");
            sp.Append("         }").Append("\r\n").Append("\r\n");

            
            sp.Append("         /// <summary>").Append("\r\n");
            sp.Append("         /// 添加数据").Append("\r\n");
            sp.Append("         /// </summary>").Append("\r\n");
            sp.Append("         public void Add(" + spacename + "." + snamespace + "." + name + "  model)").Append("\r\n");
            sp.Append("         {").Append("\r\n");
            sp.Append("             dal.Add(model);").Append("\r\n");
            sp.Append("         }").Append("\r\n").Append("\r\n");


            sp.Append("         /// <summary>").Append("\r\n");
            sp.Append("         /// 获取数据").Append("\r\n");
            sp.Append("         /// </summary>").Append("\r\n");
            sp.Append("         public List<" + spacename + "." + snamespace + "." + name + "> GetList(List<string> fields, List<Condition> conditions,List<string> orderby)").Append("\r\n");
            sp.Append("         {").Append("\r\n");
            sp.Append("             return dal.List(fields,conditions,orderby);").Append("\r\n");
            sp.Append("         }").Append("\r\n").Append("\r\n");


            sp.Append("         /// <summary>").Append("\r\n");
            sp.Append("         /// 分页获取数据").Append("\r\n");
            sp.Append("         /// </summary>").Append("\r\n");
            sp.Append("         /// <param name=\"fields\">筛选字段</param>").Append("\r\n");
            sp.Append("         /// <param name=\"conditions\">查询条件</param>").Append("\r\n");
            sp.Append("         /// <param name=\"orderby\">排序字段 如：ID ASC</param>").Append("\r\n");
            sp.Append("         /// <param name=\"pager\">pager.PageIndex = 1; //当前第1页 ///pager.PageSize = 5;  //每页显示5条数据 pager.RecordCount //总记录数 </param>").Append("\r\n");
            sp.Append("         public List<" + spacename + "." + snamespace + "." + name + "> GetList(List<string> fields, List<Condition> conditions,List<string> orderby,PageLimit pager)").Append("\r\n");
            sp.Append("         {").Append("\r\n");
            sp.Append("             return dal.List(fields,conditions,orderby,pager);").Append("\r\n");
            sp.Append("         }").Append("\r\n");


            sp.Append("     }").Append("\r\n");
            sp.Append("}").Append("\r\n");

            result = sp.Value;
        }

    }
}
