﻿using System;
using System.Collections.Generic;
using System.Text;

using NORM.Common;
using NORM.Entity; 

namespace NORM.SQLObject
{
    using NORM.DataBase;

    public class T : EntityBase { }

    public class EntityQuery<T> : List<T> where T : EntityBase, new()
    {
        private static EntityQuery<T> _instance = null;
        private static object lockobject = new object();

        public static EntityQuery<T> Instance
        {
            get
            {
                if (_instance == null)
                {
                    lock (lockobject)
                    {
                        if (_instance == null)
                        {
                            _instance = new EntityQuery<T>();
                        }
                    }
                }
                //_instance._dba.sqlstatementclear();
                return _instance;
            }
        }

        //private DataBase _dba;

        //private string _nam;
        //public string EntityName
        //{
        //    get
        //    {
        //        return _nam;
        //    }
        //}

        private EntityQuery()
        {
            //_dba = DataBaseFactory.Create();
            //_nam = ((EntityBase)Activator.CreateInstance(typeof(T))).TableName;
        }

        private List<T> QueryList(OQL oql, DataBase db)
        {
            return db.QueryList<T>(oql);
        }

        private List<T> QueryList(OQL oql, PageLimit page, DataBase db)
        {
            return db.QueryList<T>(oql, page);
        }

        private List<T> QueryList(string sql, DataBase db)
        {
            var dt = db.QueryTable(sql, null);
            return NORM.Common.ConvertTo.ConvertToList<T>(dt);
        }

        private List<T> QueryList(string sql, OsqlParameter[] parameters, DataBase db)
        {
            var dt = db.QueryTable(sql, parameters);
            return NORM.Common.ConvertTo.ConvertToList<T>(dt);
        }
                
        private T QueryObject(OQL oql, DataBase db)
        {
            return db.QueryObject<T>(oql);
        }

        private System.Data.DataTable QueryTable(OQL oql, DataBase db)
        {
            return db.QueryTable(oql);
        }

        private System.Data.DataTable QueryTable(OQL oql, PageLimit page, DataBase db)
        {
            return db.QueryTable(oql, page);
        }

        private System.Data.DataTable QueryTable(string sql, DataBase db)
        {
            return db.QueryTable(sql, null);
        }

        private System.Data.DataTable QueryTable(string sql, OsqlParameter[] parameters, DataBase db)
        {
            return db.QueryTable(sql, parameters);
        }

        private T QueryModel(object PrimaryKeyValue, DataBase db)
        {
            return db.QueryModel<T>(PrimaryKeyValue);
        }

        public bool Insert(T model, DataBase db)
        {
            return db.Insert<T>(model);
        }

        public bool Update(T model, DataBase db)
        {
            return db.Update<T>(model);
        }

        public bool Delete(T model, DataBase db)
        {
            return db.Delete<T>(model);
        }

        public bool Delete(object PrimaryKeyValue, DataBase db)
        {
            return db.Delete<T>(PrimaryKeyValue);
        }

        public bool Excute(OQL oql, DataBase db)
        {
            return db.Execute(oql) > 0 ? true : false;
        }

        public bool Excute(string sql, DataBase db)
        {
            return db.Execute(System.Data.CommandType.Text, sql, null) > 0 ? true : false;
        }

        public bool Excute(string sql, OsqlParameter[] parameters, DataBase db)
        {
            return db.Execute(System.Data.CommandType.Text, sql, parameters) > 0 ? true : false;
        }

        #region  不再使用的代码
        ///// <summary>
        ///// 只能默认数据库连接对象 不推荐使用 
        ///// </summary>
        ///// <param name="oql"></param>
        ///// <returns></returns>
        //public T QueryObject(OQL oql)
        //{
        //    return _dba.QueryObject<T>(oql);
        //}       

        ///// <summary>
        ///// 只能默认数据库连接对象 不推荐使用
        ///// </summary>
        ///// <param name="PrimaryKeyValue"></param>
        ///// <returns></returns>
        //public T QueryModel(object PrimaryKeyValue)
        //{
        //    return _dba.QueryModel<T>(PrimaryKeyValue);
        //}       

        ///// <summary>
        ///// 只能默认数据库连接对象 不推荐使用
        ///// </summary>
        ///// <param name="oql"></param>
        ///// <returns></returns>
        //public List<T> QueryList(OQL oql)
        //{
        //    return _dba.QueryList<T>(oql);
        //}       

        ///// <summary>
        ///// 只能默认数据库连接对象 不推荐使用
        ///// </summary>
        ///// <param name="oql"></param>
        ///// <param name="page"></param>
        ///// <returns></returns>
        //public List<T> QueryList(OQL oql, PageLimit page)
        //{
        //    return _dba.QueryList<T>(oql, page);
        //}
        #endregion
                
        public List<T> QueryToList(OQL oql, PageLimit page, DataBase db)
        {
            return Instance.QueryList(oql, page, db);
        }

        public List<T> QueryToList(OQL oql, DataBase db)
        {
            return Instance.QueryList(oql, db);
        }

        public List<T> QueryToList(string sql, DataBase db)
        {
            return Instance.QueryList(sql, db);
        }

        public List<T> QueryToList(string sql, OsqlParameter[] parameters, DataBase db)
        {
            return Instance.QueryList(sql, parameters, db);
        }

        public List<DTO> QueryToList<DTO>(OQL oql, DataBase db)
        {
            return DTLConvert<DTO>.ToList(Instance.QueryTable(oql, db));
        }

        public List<DTO> QueryToList<DTO>(OQL oql, PageLimit page, DataBase db)
        {
            var dt = Instance.QueryTable(oql, page, db);
            page.DataSourceView = dt.DefaultView;
            return DTLConvert<DTO>.ToList(dt);
        }

        public T QueryToModel(object PrimaryKeyValue, DataBase db)
        {
            return Instance.QueryModel(PrimaryKeyValue, db);
        }

        public T QueryToObject(OQL oql, DataBase db)
        {
            return Instance.QueryObject(oql, db);
        }
             
        public System.Data.DataTable QueryToTable(OQL oql, PageLimit page, DataBase db)
        {
            return Instance.QueryTable(oql, page, db);
        }

        public System.Data.DataTable QueryToTable(OQL oql, DataBase db)
        {
            return Instance.QueryTable(oql, db);
        }

        public System.Data.DataTable QueryToTable(string sql, DataBase db)
        {
            return Instance.QueryTable(sql, null);
        }

        public System.Data.DataTable QueryToTable(string sql, OsqlParameter[] parameters, DataBase db)
        {
            return Instance.QueryTable(sql, parameters, db);
        }

    }
        
}
