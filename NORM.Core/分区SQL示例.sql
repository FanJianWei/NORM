
--添加文件组
alter database DevelopFramework_Base add filegroup JIANGXI

--添加文件应用到文件组
alter database DevelopFramework_Base add file 
(name=N'JIANGXI',filename=N'F:\数据库文件\JIANGXI.ndf',size=3Mb,filegrowth=1mb)
to filegroup JIANGXI


--创建分区函数 
CREATE PARTITION FUNCTION [partitionFunArea](nvarchar(50)) AS RANGE LEFT FOR VALUES (N'山东', N'四川', N'浙江','贵州')
GO


--创建分区方案语法 --创建分区方案,所有分区在一个组里面
CREATE PARTITION SCHEME [partitionSchemeArea] AS PARTITION [partitionFunArea] TO ([SICHUAN], [SHANDONG], [ZHEJIANG], [GUIZHOU],[JIANGXI])
GO
 

--删除分区方案 
drop partition scheme [partitionSchemeArea]
GO

--删除分区方法 
drop partition function [partitionFunArea]
GO


CREATE TABLE [dbo].[T_BigData](
	[NodeID] [int] NULL,
	[DateTime] [datetime] NULL,
	[Sync] [nvarchar](20) NULL,
	[SampleMode] [nchar](10) NULL,
	[Unit] [nvarchar](10) NULL,
	[ParaID] [int] NULL,
	[SampleFreq] [decimal](12, 3) NULL,
	[SampleLength] [int] NULL,
	[Speed] [decimal](7, 2) NULL,
	[Condition1] [decimal](7, 2) NULL,
	[Condition2] [decimal](7, 2) NULL,
	[Condition3] [decimal](7, 2) NULL,
	[Condition4] [decimal](7, 2) NULL,
	[Condition5] [decimal](7, 2) NULL,
	[Condition6] [decimal](7, 2) NULL,
	[Condition7] [decimal](7, 2) NULL,
	[Condition8] [decimal](7, 2) NULL,
	[WaveData] [image] NULL,
	[speed1] [decimal](7, 2) NULL,
	[speed2] [decimal](7, 2) NULL,
	[Area] [nvarchar](50) NULL
) ON [partitionSchemeArea]([Area]);   --指定使用的分区方案



INSERT INTO  DevelopFramework_Base.dbo.T_BigData 
SELECT TOP 100 [NodeID]
      ,[DateTime]
      ,[Sync]
      ,[SampleMode]
      ,[Unit]
      ,[ParaID]
      ,[SampleFreq]
      ,[SampleLength]
      ,[Speed]
      ,[Condition1]
      ,[Condition2]
      ,[Condition3]
      ,[Condition4]
      ,[Condition5]
      ,[Condition6]
      ,[Condition7]
      ,[Condition8]
      ,[WaveData]
      ,[speed1]
      ,[speed2]      
      ,'浙江'
  FROM [CMSMonitordata].[dbo].[WaveData]
  
  
  INSERT INTO  DevelopFramework_Base.dbo.T_BigData 
SELECT TOP 100 [NodeID]
      ,[DateTime]
      ,[Sync]
      ,[SampleMode]
      ,[Unit]
      ,[ParaID]
      ,[SampleFreq]
      ,[SampleLength]
      ,[Speed]
      ,[Condition1]
      ,[Condition2]
      ,[Condition3]
      ,[Condition4]
      ,[Condition5]
      ,[Condition6]
      ,[Condition7]
      ,[Condition8]
      ,[WaveData]
      ,[speed1]
      ,[speed2]      
      ,'四川'
  FROM [CMSMonitordata].[dbo].[WaveData]



select $PARTITION.partitionFunArea([Area]) as 分区编号,count(NodeID) as 记录数 
from dbo.T_BigData group by $PARTITION.partitionFunArea([Area])

select $PARTITION.partitionFunArea([Area]),* 
from dbo.T_BigData

--查询分区内数据
select * from dbo.T_BigData where $PARTITION.partitionFunArea([Area])=2


--创建分区索引
CREATE CLUSTERED INDEX [ClusteredIndex_on_bgPartitionSchema_635342971076448165] ON [dbo].T_BigData 
(
    [NodeID],
    [DateTime],
    [Area]
)WITH (SORT_IN_TEMPDB = OFF, IGNORE_DUP_KEY = OFF, DROP_EXISTING = OFF, ONLINE = OFF) ON [partitionSchemeArea]([Area])

--使用分区索引查询，可以避免多个cpu操作多个磁盘时产生的冲突。


select $partition.[partitionFunArea]('浙江')  --返回值是2，表示此值存在第2个分区 
