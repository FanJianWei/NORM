﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using NORM.Entity;
using NORM.SQLObject;

namespace NORM.Demo
{
    public partial class Lamada测试 : Form
    {
        public Lamada测试()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var db=DataBase.DataBaseFactory.Create();

            try
            {
                string guid = Guid.NewGuid().ToString();

                Models.T_Category model = new Models.T_Category();
                OQL oql = OQL.From(model).Where(w => w.Compare(model.Code, "!=", "")
                    & w.Compare(model.DeleteSign, "=", 0)
                    & w.Compare(model.Name, "IS NOT", DataValue.DBNull)
                    & w.Compare(model.ID, ">", 1, "OR"))
                    .Select(new List<TableField>(){                   
                   model.produceField("Name"),
                   model.produceField("Code"),
                   model.produceField("DeleteSign")
                }.ToArray()).End;

                DataTable dt = db.QueryTable(oql);

                dataGridView1.DataSource = dt.DefaultView;

                OQL oql2 = new OQL(model).Select(() =>
                {
                    return "DeleteSign,Name";
                })
                    .Where(w => w.Compare(model.DeleteSign, "=", 0)
                    & w.IsNotNull(model.Code))
                    .OrderBy(o => o.Asc(model.ID) & o.Desc(model.Name))
                    .End;

                dt = db.QueryTable(oql2);

                OQL oql3 = new OQL(model).Select(() =>
                {
                    return new
                    {
                        Name = model.getPropertyItem(0),
                        DeleteSign = model.getPropertyItem(1)
                    };
                })
                   .Where(w => w.Compare(model.DeleteSign, "=", 0)
                   & w.IsNotNull(model.Code))
                   .End;

                dt = db.QueryTable(oql3);

                OQL oql4 = new OQL(model).Select(() => new
                {
                    Name = model.Name,
                    DeleteSign = model.DeleteSign
                })
                  .Where(w => w.Compare(model.DeleteSign, "=", 0)
                  & w.IsNotNull(model.Code))
                  .End;

                dt = db.QueryTable(oql4);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message
                    );
            }         

        }
    }
}
