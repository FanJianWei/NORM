﻿using NORM.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace NORM.Models
{
    ///名称：BearingFactory
    ///作者：wxdong 
    ///创建时间：2016-08-31 10:57 
    ///功能描述：BearingFactory	实体类
    [Serializable]
    [Describe("Table")]
    public class BearingFactory : EntityBase
    {
        public BearingFactory()
        {
            TableName = "BearingFactory";
            PrimaryKey.Add("FactoryID");
        }

        #region Model
        private int _factoryid;
        /// <summary>
        /// 
        /// </summary>
        [Describe("Identify")]
        public int FactoryID
        {
            set
            {
                _factoryid = value;
            }
            get
            {
                return _factoryid;
            }
        }
        private string _factoryname;
        /// <summary>
        /// 
        /// </summary>
        public string FactoryName
        {
            set
            {
                _factoryname = value;
            }
            get
            {
                return _factoryname;
            }
        }
        private string _remark;
        /// <summary>
        /// 
        /// </summary>
        public string Remark
        {
            set
            {
                _remark = value;
            }
            get
            {
                return _remark;
            }
        }
        #endregion Model
    }
}
