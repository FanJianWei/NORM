﻿using System;

using NORM.Entity;

namespace Net.Pro.Models
{
    ///名称：MeasureValues
    ///作者：wxdong
    ///创建时间：2016-07-14 12:19 
    [Serializable]
    [Describe("Table")]
    public class MeasureValues : EntityBase
    {
        public MeasureValues()
        {
            TableName = "MeasureValues";
            PrimaryKey.Add("NodeID");
        }
        #region Model
        /// <summary>
        /// 
        /// </summary>
        public int NodeID
        {
            get { return getProperty<int>("NodeID"); }
            set { setProperty("NodeID", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime Datetime
        {
            get { return getProperty<DateTime>("Datetime"); }
            set { setProperty("Datetime", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int ParaID
        {
            get { return getProperty<int>("ParaID"); }
            set { setProperty("ParaID", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal SampleFreq
        {
            get { return getProperty<decimal>("SampleFreq"); }
            set { setProperty("SampleFreq", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int SampleLength
        {
            get { return getProperty<int>("SampleLength"); }
            set { setProperty("SampleLength", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public float Ave
        {
            get { return getProperty<float>("Ave"); }
            set { setProperty("Ave", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public float RMS
        {
            get { return getProperty<float>("RMS"); }
            set { setProperty("RMS", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public float Vpp
        {
            get { return getProperty<float>("Vpp"); }
            set { setProperty("Vpp", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public float p1X
        {
            get { return getProperty<float>("p1X"); }
            set { setProperty("p1X", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public float Phase1X
        {
            get { return getProperty<float>("Phase1X"); }
            set { setProperty("Phase1X", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public float Mean
        {
            get { return getProperty<float>("Mean"); }
            set { setProperty("Mean", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public float WaveFormFactor
        {
            get { return getProperty<float>("WaveFormFactor"); }
            set { setProperty("WaveFormFactor", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public float WaveImpactFactor
        {
            get { return getProperty<float>("WaveImpactFactor"); }
            set { setProperty("WaveImpactFactor", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public float WaveKurtosis
        {
            get { return getProperty<float>("WaveKurtosis"); }
            set { setProperty("WaveKurtosis", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public float WaveSkewness
        {
            get { return getProperty<float>("WaveSkewness"); }
            set { setProperty("WaveSkewness", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public float WaveRMSAMP
        {
            get { return getProperty<float>("WaveRMSAMP"); }
            set { setProperty("WaveRMSAMP", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public float WaveYudu
        {
            get { return getProperty<float>("WaveYudu"); }
            set { setProperty("WaveYudu", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AmpUnit
        {
            get { return getProperty<string>("AmpUnit"); }
            set { setProperty("AmpUnit", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AngelUnit
        {
            get { return getProperty<string>("AngelUnit"); }
            set { setProperty("AngelUnit", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public float ConditionValue
        {
            get { return getProperty<float>("ConditionValue"); }
            set { setProperty("ConditionValue", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int AlarmState
        {
            get { return getProperty<int>("AlarmState"); }
            set { setProperty("AlarmState", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public float AlarmType
        {
            get { return getProperty<float>("AlarmType"); }
            set { setProperty("AlarmType", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public float AlarmValue1
        {
            get { return getProperty<float>("AlarmValue1"); }
            set { setProperty("AlarmValue1", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public float AlarmValue2
        {
            get { return getProperty<float>("AlarmValue2"); }
            set { setProperty("AlarmValue2", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public float SpeedStamp
        {
            get { return getProperty<float>("SpeedStamp"); }
            set { setProperty("SpeedStamp", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public float? ConditionValue1
        {
            get { return getProperty<float?>("ConditionValue1"); }
            set { setProperty("ConditionValue1", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public float? ConditionValue2
        {
            get { return getProperty<float?>("ConditionValue2"); }
            set { setProperty("ConditionValue2", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public float? ConditionValue3
        {
            get { return getProperty<float?>("ConditionValue3"); }
            set { setProperty("ConditionValue3", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public float ConditionValue4
        {
            get { return getProperty<float>("ConditionValue4"); }
            set { setProperty("ConditionValue4", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public float ConditionValue5
        {
            get { return getProperty<float>("ConditionValue5"); }
            set { setProperty("ConditionValue5", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public float ConditionValue6
        {
            get { return getProperty<float>("ConditionValue6"); }
            set { setProperty("ConditionValue6", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public float ConditionValue7
        {
            get { return getProperty<float>("ConditionValue7"); }
            set { setProperty("ConditionValue7", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public float ConditionValue8
        {
            get { return getProperty<float>("ConditionValue8"); }
            set { setProperty("ConditionValue8", value); }
        }
        #endregion Model
    }

    ///名称：WaveData
    ///作者：wxdong
    ///创建时间：2016-07-14 12:38 
    [Serializable]
    [Describe("Table")]
    public class MWaveData : EntityBase
    {
        public MWaveData()
        {
            TableName = "WaveData";
            PrimaryKey.Add("Key1");
        }
        #region Model
        /// <summary>
        /// 
        /// </summary>
        public int NodeID
        {
            get { return getProperty<int>("NodeID"); }
            set { setProperty("NodeID", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime DateTime
        {
            get { return getProperty<DateTime>("DateTime"); }
            set { setProperty("DateTime", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Sync
        {
            get { return getProperty<string>("Sync"); }
            set { setProperty("Sync", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string SampleMode
        {
            get { return getProperty<string>("SampleMode"); }
            set { setProperty("SampleMode", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Unit
        {
            get { return getProperty<string>("Unit"); }
            set { setProperty("Unit", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int ParaID
        {
            get { return getProperty<int>("ParaID"); }
            set { setProperty("ParaID", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal SampleFreq
        {
            get { return getProperty<decimal>("SampleFreq"); }
            set { setProperty("SampleFreq", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int SampleLength
        {
            get { return getProperty<int>("SampleLength"); }
            set { setProperty("SampleLength", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal Speed
        {
            get { return getProperty<decimal>("Speed"); }
            set { setProperty("Speed", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? Condition1
        {
            get { return getProperty<decimal?>("Condition1"); }
            set { setProperty("Condition1", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal Condition2
        {
            get { return getProperty<decimal>("Condition2"); }
            set { setProperty("Condition2", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal Condition3
        {
            get { return getProperty<decimal>("Condition3"); }
            set { setProperty("Condition3", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal Condition4
        {
            get { return getProperty<decimal>("Condition4"); }
            set { setProperty("Condition4", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal Condition5
        {
            get { return getProperty<decimal>("Condition5"); }
            set { setProperty("Condition5", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal Condition6
        {
            get { return getProperty<decimal>("Condition6"); }
            set { setProperty("Condition6", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal Condition7
        {
            get { return getProperty<decimal>("Condition7"); }
            set { setProperty("Condition7", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal Condition8
        {
            get { return getProperty<decimal>("Condition8"); }
            set { setProperty("Condition8", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public byte[] WaveData
        {
            get { return getProperty<byte[]>("WaveData"); }
            set { setProperty("WaveData", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? speed1
        {
            get { return getProperty<decimal?>("speed1"); }
            set { setProperty("speed1", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal? speed2
        {
            get { return getProperty<decimal?>("speed2"); }
            set { setProperty("speed2", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Key1
        {
            get { return getProperty<string>("Key1"); }
            set { setProperty("Key1", value); }
        }
        #endregion Model
    }
}
