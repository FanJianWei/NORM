﻿using System;

using NORM.Common;
using NORM.Entity;

namespace NORM.Models
{
    ///名称：T_Accounts
    ///作者：wxdong
    ///创建时间：2015-12-28 23:45 
    [Serializable]
    [AtttribDescrib("Table")]
    public class T_Accounts : EntityBase
    {
        public T_Accounts()
        {
            TableName = "T_Accounts";
            PrimaryKey.Add("ID");
        }
        #region Model
        /// <summary>
        /// 
        /// </summary>
        [AtttribDescrib("Identify")]
        public Int64 ID
        {
            get { return getProperty<Int64>("ID"); }
            set { setProperty("ID", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string YM
        {
            get { return getProperty<string>("YM"); }
            set { setProperty("YM", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Describ
        {
            get { return getProperty<string>("Describ"); }
            set { setProperty("Describ", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Category
        {
            get { return getProperty<string>("Category"); }
            set { setProperty("Category", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int AccountType
        {
            get { return getProperty<int>("AccountType"); }
            set { setProperty("AccountType", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal Amount
        {
            get { return getProperty<decimal>("Amount"); }
            set { setProperty("Amount", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        [AtttribDescrib("Type::Sdt")]
        public string AccountTime
        {
            get { return getProperty<string>("AccountTime"); }
            set { setProperty("AccountTime", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int CreateBy
        {
            get { return getProperty<int>("CreateBy"); }
            set { setProperty("CreateBy", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime CreateDate
        {
            get { return getProperty<DateTime>("CreateDate"); }
            set { setProperty("CreateDate", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string NoteText
        {
            get { return getProperty<string>("NoteText"); }
            set { setProperty("NoteText", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int EncryptSign
        {
            get { return getProperty<int>("EncryptSign"); }
            set { setProperty("EncryptSign", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int DeleteSign
        {
            get { return getProperty<int>("DeleteSign"); }
            set { setProperty("DeleteSign", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int Department
        {
            get { return getProperty<int>("Department"); }
            set { setProperty("Department", value); }
        }
        #endregion Model
    }
}
