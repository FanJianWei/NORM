﻿using System;

using NORM.Common;
using NORM.Entity;

namespace NORM.Models
{
    ///名称：T_Category
    ///作者：wxdong
    ///创建时间：2015-12-28 23:46 
    [Serializable]
    [Describe("Table")]
    public class T_Category : EntityBase
    {
        public T_Category()
        {
            TableName = "T_Category";
            PrimaryKey.Add("ID");
        }
        #region Model
        /// <summary>
        /// 
        /// </summary>
        [Describe("Identify")]
        public Int64 ID
        {
            get { return getProperty<Int64>("ID"); }
            set { setProperty("ID", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get { return getProperty<string>("Name"); }
            set { setProperty("Name", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Code
        {
            get { return getProperty<string>("Code"); }
            set { setProperty("Code", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Describ
        {
            get { return getProperty<string>("Describ"); }
            set { setProperty("Describ", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int OrderIndex
        {
            get { return getProperty<int>("OrderIndex"); }
            set { setProperty("OrderIndex", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int DeleteSign
        {
            get { return getProperty<int>("DeleteSign"); }
            set { setProperty("DeleteSign", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        [Column("SelectedDown")]
        public string Selected
        {
            get { return getProperty<string>("Selected"); }
            set { setProperty("Selected", value); }
        }
        #endregion Model
    }
}
