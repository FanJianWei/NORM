﻿using NORM.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace NORM.Models
{
    [Serializable]
    [Describe("Table")]
    public class T_Login : EntityBase
    {
        public T_Login()
        {
            TableName = "t_login";
            PrimaryKey.Add("ID");
        }

        /// <summary>
        /// 
        /// </summary>
        [Describe("Identify")]
        public int ID
        {
            get { return getProperty<int>("ID"); }
            set { setProperty("ID", value); }
        }

        /// <summary>
        /// 
        /// </summary>       
        public string Name
        {
            get { return getProperty<string>("Name"); }
            set { setProperty("Name", value); }
        }


        /// <summary>
        /// 
        /// </summary>       
        public DateTime Datetime
        {
            get { return getProperty<DateTime>("Datetime"); }
            set { setProperty("Datetime", value); }
        }

        /// <summary>
        /// 
        /// </summary>       
        public int OrderIndex
        {
            get { return getProperty<int>("OrderIndex"); }
            set { setProperty("OrderIndex", value); }
        }


    }
}
