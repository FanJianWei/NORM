﻿using NORM.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace NORM.Models
{
    ///名称：T_Province
    ///作者：wxdong
    ///创建时间：2018-05-12 14:31 
    [Serializable]
    [Describe("Table")]
    public class T_Province : EntityBase
    {
        public T_Province()
        {
            TableName = "T_Province";
            PrimaryKey.Add("ID");
        }
        #region Model
        /// <summary>
        /// 
        /// </summary>
        [Describe("Identify")]
        public int ID
        {
            get { return getProperty<int>("ID"); }
            set { setProperty("ID", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get { return getProperty<string>("Name"); }
            set { setProperty("Name", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Describe
        {
            get { return getProperty<string>("Describe"); }
            set { setProperty("Describe", value); }
        }
        #endregion Model
    }
}
