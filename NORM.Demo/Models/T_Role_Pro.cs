﻿using NORM.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace NORM.Models
{
    [Serializable]
    public class V_Role : EntityBase
    {
        public V_Role()
        {
            TableName = "V_Role";
            PrimaryKey.Add("ID");
        }

        #region Model
        private string _id;
        /// <summary>
        /// 
        /// </summary>
        public string ID
        {
            set
            {
                _id = value;
            }
            get
            {
                return _id;
            }
        }
        private string _nodename;
        /// <summary>
        /// 
        /// </summary>
        public string NodeName
        {
            set
            {
                _nodename = value;
            }
            get
            {
                return _nodename;
            }
        }
        private string _name;
        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            set
            {
                _name = value;
            }
            get
            {
                return _name;
            }
        }
        private string _describtion;
        /// <summary>
        /// 
        /// </summary>
        public string Describtion
        {
            set
            {
                _describtion = value;
            }
            get
            {
                return _describtion;
            }
        }
        private int? _department;
        /// <summary>
        /// 
        /// </summary>
        public int? Department
        {
            set
            {
                _department = value;
            }
            get
            {
                return _department;
            }
        }
        private string _nodecode;
        /// <summary>
        /// 
        /// </summary>
        public string NodeCode
        {
            get { return _nodecode; }
            set { _nodecode = value; }
        }
        private int? _level;
        /// <summary>
        /// 
        /// </summary>
        public int? Level
        {
            set
            {
                _level = value;
            }
            get
            {
                return _level;
            }
        }
        private string _menus;
        /// <summary>
        /// 
        /// </summary>
        public string Menus
        {
            set
            {
                _menus = value;
            }
            get
            {
                return _menus;
            }
        }
        private string _operate;
        /// <summary>
        /// 
        /// </summary>
        public string Operate
        {
            set
            {
                _operate = value;
            }
            get
            {
                return _operate;
            }
        }
        private string _visits;
        /// <summary>
        /// 
        /// </summary>
        public string Visits
        {
            set
            {
                _visits = value;
            }
            get
            {
                return _visits;
            }
        }
        private int? _deletesign;
        /// <summary>
        /// 
        /// </summary>
        public int? DeleteSign
        {
            set
            {
                _deletesign = value;
            }
            get
            {
                return _deletesign;
            }
        }
        private string _note;
        /// <summary>
        /// 
        /// </summary>
        public string Note
        {
            set
            {
                _note = value;
            }
            get
            {
                return _note;
            }
        }
        #endregion Model
    }
}
