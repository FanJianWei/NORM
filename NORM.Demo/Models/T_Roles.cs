﻿using System;

using NORM.Common;
using NORM.Entity;

namespace NORM.Models
{
    ///名称：T_Roles
    ///作者：wxdong
    ///创建时间：2015-12-28 21:12 
    [Serializable]
    [Describe("Table")]
    public class T_Roles : EntityBase
    {
        public T_Roles()
        {
            TableName = "T_Roles";
            PrimaryKey.Add("ID");
        }
        #region Model
        /// <summary>
        /// 
        /// </summary>
        public Int64 ID
        {
            get { return getProperty<Int64>("ID"); }
            set { setProperty("ID", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get { return getProperty<string>("Name"); }
            set { setProperty("Name", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Code
        {
            get { return getProperty<string>("Code"); }
            set { setProperty("Code", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public Int64 ParentID
        {
            get { return getProperty<Int64>("ParentID"); }
            set { setProperty("ParentID", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Describ
        {
            get { return getProperty<string>("Describ"); }
            set { setProperty("Describ", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int DeleteSign
        {
            get { return getProperty<int>("DeleteSign"); }
            set { setProperty("DeleteSign", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int Department
        {
            get { return getProperty<int>("Department"); }
            set { setProperty("Department", value); }
        }
        #endregion Model
    }
}
