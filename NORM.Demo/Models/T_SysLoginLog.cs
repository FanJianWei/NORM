﻿using System;

using NORM.Entity;

namespace NORM.Models
{
    ///名称：T_SysLoginLog
    ///作者：wxdong
    ///创建时间：2016-01-29 13:09 
    [Serializable]
    [Describe("Table")]
    public class T_SysLoginLog : EntityBase
    {
        public T_SysLoginLog()
        {
            TableName = "T_SysLoginLog";
            PrimaryKey.Add("LoginLogID");
        }
        #region Model
        /// <summary>
        /// 
        /// </summary>
        [Describe("Identify")]
        public int LoginLogID
        {
            get { return getProperty<int>("LoginLogID"); }
            set { setProperty("LoginLogID", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LoginAccount
        {
            get { return getProperty<string>("LoginAccount"); }
            set { setProperty("LoginAccount", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime LoginDate
        {
            get { return getProperty<DateTime>("LoginDate"); }
            set { setProperty("LoginDate", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LoginMac
        {
            get { return getProperty<string>("LoginMac"); }
            set { setProperty("LoginMac", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LoginIP
        {
            get { return getProperty<string>("LoginIP"); }
            set { setProperty("LoginIP", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string LoginType
        {
            get { return getProperty<string>("LoginType"); }
            set { setProperty("LoginType", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string IsSecc
        {
            get { return getProperty<string>("IsSecc"); }
            set { setProperty("IsSecc", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Status
        {
            get { return getProperty<string>("Status"); }
            set { setProperty("Status", value); }
        }
        #endregion Model
    }
}
