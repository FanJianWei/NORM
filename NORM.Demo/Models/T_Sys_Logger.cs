﻿using NORM.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace NORM.Models
{
    ///名称：T_Sys_Logger
    ///作者：wxdong
    ///创建时间：2017-11-13 09:29 
    [Serializable]
    [Describe("Table")]
    public class T_Sys_Logger : EntityBase
    {
        public T_Sys_Logger()
        {
            TableName = "T_Sys_Logger";
            PrimaryKey.Add("ID");
        }
        #region Model
        /// <summary>
        /// 
        /// </summary>
        [Describe("Identify")]
        public int ID
        {
            get { return getProperty<int>("ID"); }
            set { setProperty("ID", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Content
        {
            get { return getProperty<string>("Content"); }
            set { setProperty("Content", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string StateCode
        {
            get { return getProperty<string>("StateCode"); }
            set { setProperty("StateCode", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int Type
        {
            get { return getProperty<int>("Type"); }
            set { setProperty("Type", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string SourceUrl
        {
            get { return getProperty<string>("SourceUrl"); }
            set { setProperty("SourceUrl", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int ObjectID
        {
            get { return getProperty<int>("ObjectID"); }
            set { setProperty("ObjectID", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        [Column("Describtion", "StateCode")]
        public string Describtion
        {
            get { return getProperty<string>("Describtion"); }
            set { setProperty("Describtion", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string IpAddress
        {
            get { return getProperty<string>("IpAddress"); }
            set { setProperty("IpAddress", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        [Column("OperationBy")]
        public int OperationBy
        {
            get { return getProperty<int>("OperationBy"); }
            set { setProperty("OperationBy", value); }
        }
        /// <summary>
        /// 记录事情发生的时间
        /// </summary>
        public DateTime Datetime
        {
            get { return getProperty<DateTime>("Datetime"); }
            set { setProperty("Datetime", value); }
        }
        /// <summary>
        /// 创建人 默认 0 系统
        /// </summary>
        public int CreateBy
        {
            get { return getProperty<int>("CreateBy"); }
            set { setProperty("CreateBy", value); }
        }
        /// <summary>
        /// 创建时间
        /// </summary>
        public DateTime CreateDatetime
        {
            get { return getProperty<DateTime>("CreateDatetime"); }
            set { setProperty("CreateDatetime", value); }
        }
        #endregion Model
    }
}
