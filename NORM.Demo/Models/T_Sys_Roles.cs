﻿using NORM.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace NORM.Models
{
    ///名称：T_Sys_Roles
    ///作者：wxdong
    ///创建时间：2018-05-12 13:54 
    [Serializable]
    [Describe("Table")]
    public class T_Sys_Roles : EntityBase
    {
        public T_Sys_Roles()
        {
            TableName = "T_Sys_Roles";
            PrimaryKey.Add("ID");
        }
        #region Model
        /// <summary>
        /// 
        /// </summary>
        [Describe("Identify")]
        public int ID
        {
            get { return getProperty<int>("ID"); }
            set { setProperty("ID", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Code
        {
            get { return getProperty<string>("Code"); }
            set { setProperty("Code", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get { return getProperty<string>("Name"); }
            set { setProperty("Name", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Label
        {
            get { return getProperty<string>("Label"); }
            set { setProperty("Label", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int DeleteSign
        {
            get { return getProperty<int>("DeleteSign"); }
            set { setProperty("DeleteSign", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int DepartmentID
        {
            get { return getProperty<int>("DepartmentID"); }
            set { setProperty("DepartmentID", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Departments
        {
            get { return getProperty<string>("Departments"); }
            set { setProperty("Departments", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string MenuItems
        {
            get { return getProperty<string>("MenuItems"); }
            set { setProperty("MenuItems", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Buttons
        {
            get { return getProperty<string>("Buttons"); }
            set { setProperty("Buttons", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int Level
        {
            get { return getProperty<int>("Level"); }
            set { setProperty("Level", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Note
        {
            get { return getProperty<string>("Note"); }
            set { setProperty("Note", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int CreateBy
        {
            get { return getProperty<int>("CreateBy"); }
            set { setProperty("CreateBy", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime CreateDate
        {
            get { return getProperty<DateTime>("CreateDate"); }
            set { setProperty("CreateDate", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Describtion
        {
            get { return getProperty<string>("Describtion"); }
            set { setProperty("Describtion", value); }
        }
        #endregion Model
    }
}
