﻿using NORM.Entity;
using System;
using System.Collections.Generic;
using System.Text;

namespace NORM.Models
{
    ///名称：T_Sys_Users
    ///作者：wxdong
    ///创建时间：2018-05-12 13:55 
    [Serializable]
    [Describe("Table")]
    public class T_Sys_Users : EntityBase
    {
        public T_Sys_Users()
        {
            TableName = "T_Sys_Users";
            PrimaryKey.Add("ID");
        }
        #region Model
        /// <summary>
        /// 
        /// </summary>
        [Describe("Identify")]
        public int ID
        {
            get { return getProperty<int>("ID"); }
            set { setProperty("ID", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string UserName
        {
            get { return getProperty<string>("UserName"); }
            set { setProperty("UserName", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get { return getProperty<string>("Name"); }
            set { setProperty("Name", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Photo
        {
            get { return getProperty<string>("Photo"); }
            set { setProperty("Photo", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Password
        {
            get { return getProperty<string>("Password"); }
            set { setProperty("Password", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int DepartmentID
        {
            get { return getProperty<int>("DepartmentID"); }
            set { setProperty("DepartmentID", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Tel
        {
            get { return getProperty<string>("Tel"); }
            set { setProperty("Tel", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Email
        {
            get { return getProperty<string>("Email"); }
            set { setProperty("Email", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int Level
        {
            get { return getProperty<int>("Level"); }
            set { setProperty("Level", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int RoleID
        {
            get { return getProperty<int>("RoleID"); }
            set { setProperty("RoleID", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int GroupID
        {
            get { return getProperty<int>("GroupID"); }
            set { setProperty("GroupID", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Theme
        {
            get { return getProperty<string>("Theme"); }
            set { setProperty("Theme", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int DeleteSign
        {
            get { return getProperty<int>("DeleteSign"); }
            set { setProperty("DeleteSign", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime CreateDate
        {
            get { return getProperty<DateTime>("CreateDate"); }
            set { setProperty("CreateDate", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CreateBy
        {
            get { return getProperty<string>("CreateBy"); }
            set { setProperty("CreateBy", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime LastLoginDate
        {
            get { return getProperty<DateTime>("LastLoginDate"); }
            set { setProperty("LastLoginDate", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Note
        {
            get { return getProperty<string>("Note"); }
            set { setProperty("Note", value); }
        }
        #endregion Model
    }
}
