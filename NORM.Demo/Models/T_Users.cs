﻿using System;

using NORM.Common;
using NORM.Entity;

namespace NORM.Models
{
    ///名称：T_Users
    ///作者：wxdong
    ///创建时间：2015-12-28 23:47 
    [Serializable]
    [Describe("Table")]
    public class T_Users : EntityBase
    {
        public T_Users()
        {
            TableName = "T_Users";
            PrimaryKey.Add("UserID");
        }
        #region Model
        /// <summary>
        /// 
        /// </summary>
        [Column("UserID")]
        [Describe("Identify")]
        public Int64 UserID
        {
            get { return getProperty<Int64>("UserID"); }
            set { setProperty("UserID", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string UserName
        {
            get { return getProperty<string>("UserName"); }
            set { setProperty("UserName", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get { return getProperty<string>("Name"); }
            set { setProperty("Name", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Passwords
        {
            get { return getProperty<string>("Passwords"); }
            set { setProperty("Passwords", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int Role
        {
            get { return getProperty<int>("Role"); }
            set { setProperty("Role", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int Department
        {
            get { return getProperty<int>("Department"); }
            set { setProperty("Department", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string MobilePhone
        {
            get { return getProperty<string>("MobilePhone"); }
            set { setProperty("MobilePhone", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Note
        {
            get { return getProperty<string>("Note"); }
            set { setProperty("Note", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int CreateBy
        {
            get { return getProperty<int>("CreateBy"); }
            set { setProperty("CreateBy", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime CreateDate
        {
            get { return getProperty<DateTime>("CreateDate"); }
            set { setProperty("CreateDate", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int DeleteSign
        {
            get { return getProperty<int>("DeleteSign"); }
            set { setProperty("DeleteSign", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Status
        {
            get { return getProperty<string>("Status"); }
            set { setProperty("Status", value); }
        }
        #endregion Model
    }
}
