﻿using System;

using NORM.Common;
using NORM.Entity;

namespace NORM.Models
{
    ///名称：V_Accounts
    ///作者：wxdong
    ///创建时间：2015-12-28 21:49 
    [Serializable]
    [Describe("View")]
    public class V_Accounts : EntityBase
    {
        public V_Accounts()
        {
            TableName = "V_Accounts";
            PrimaryKey.Add("");
        }
        #region Model
        /// <summary>
        /// 
        /// </summary>
        public Int64 ID
        {
            get { return getProperty<Int64>("ID"); }
            set { setProperty("ID", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string YM
        {
            get { return getProperty<string>("YM"); }
            set { setProperty("YM", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string DText
        {
            get { return getProperty<string>("DText"); }
            set { setProperty("DText", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Describ
        {
            get { return getProperty<string>("Describ"); }
            set { setProperty("Describ", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Category
        {
            get { return getProperty<string>("Category"); }
            set { setProperty("Category", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int AccountType
        {
            get { return getProperty<int>("AccountType"); }
            set { setProperty("AccountType", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AccountTypeName
        {
            get { return getProperty<string>("AccountTypeName"); }
            set { setProperty("AccountTypeName", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string CategoryName
        {
            get { return getProperty<string>("CategoryName"); }
            set { setProperty("CategoryName", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal Amount
        {
            get { return getProperty<decimal>("Amount"); }
            set { setProperty("Amount", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string AmountText
        {
            get { return getProperty<string>("AmountText"); }
            set { setProperty("AmountText", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime AccountTime
        {
            get { return getProperty<DateTime>("AccountTime"); }
            set { setProperty("AccountTime", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int CreateBy
        {
            get { return getProperty<int>("CreateBy"); }
            set { setProperty("CreateBy", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public DateTime CreateDate
        {
            get { return getProperty<DateTime>("CreateDate"); }
            set { setProperty("CreateDate", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int EncryptSign
        {
            get { return getProperty<int>("EncryptSign"); }
            set { setProperty("EncryptSign", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string EncryptSignText
        {
            get { return getProperty<string>("EncryptSignText"); }
            set { setProperty("EncryptSignText", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int DeleteSign
        {
            get { return getProperty<int>("DeleteSign"); }
            set { setProperty("DeleteSign", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string UserName
        {
            get { return getProperty<string>("UserName"); }
            set { setProperty("UserName", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get { return getProperty<string>("Name"); }
            set { setProperty("Name", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int Role
        {
            get { return getProperty<int>("Role"); }
            set { setProperty("Role", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string MobilePhone
        {
            get { return getProperty<string>("MobilePhone"); }
            set { setProperty("MobilePhone", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string NoteText
        {
            get { return getProperty<string>("NoteText"); }
            set { setProperty("NoteText", value); }
        }
        #endregion Model
    }
}
