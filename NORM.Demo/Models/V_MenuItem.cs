﻿using System;

using NORM.Entity;

namespace NORM.Models
{
    ///名称：V_MenuItem
    ///作者：wxdong
    ///创建时间：2016-01-16 16:22 
    [Serializable]
    [Describe("View")]
    public class V_MenuItem : EntityBase
    {
        public V_MenuItem()
        {
            TableName = "V_MenuItem";
            PrimaryKey.Add("");
        }
        #region Model
        /// <summary>
        /// 
        /// </summary>
        public int MenuItemID
        {
            get { return getProperty<int>("MenuItemID"); }
            set { setProperty("MenuItemID", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get { return getProperty<string>("Name"); }
            set { setProperty("Name", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Icon
        {
            get { return getProperty<string>("Icon"); }
            set { setProperty("Icon", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ActionEvent
        {
            get { return getProperty<string>("ActionEvent"); }
            set { setProperty("ActionEvent", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Label
        {
            get { return getProperty<string>("Label"); }
            set { setProperty("Label", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int OperationType
        {
            get { return getProperty<int>("OperationType"); }
            set { setProperty("OperationType", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int IsShow
        {
            get { return getProperty<int>("IsShow"); }
            set { setProperty("IsShow", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int MenuItemType
        {
            get { return getProperty<int>("MenuItemType"); }
            set { setProperty("MenuItemType", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string menutypename
        {
            get { return getProperty<string>("menutypename"); }
            set { setProperty("menutypename", value); }
        }
        #endregion Model
    }
}
