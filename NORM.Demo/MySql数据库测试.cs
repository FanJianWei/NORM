﻿using NORM.SQLObject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace NORM.Demo
{
    public partial class MySql数据库测试 : Form
    {
        public MySql数据库测试()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string connectionString = "Server=192.168.1.156;Port=3306;Database=demo;Uid=sa;Pwd=123456;";//Port=3306;
            using (var db = NORM.DataBase.DataBaseFactory.Create(connectionString, DataBase.DataBaseTypes.MySqlDataBase))
            {
                string message=string.Empty;
                if (db.TestConnect(out message))
                {
                    NORM.Entity.PageLimit pager = new Entity.PageLimit();
                    pager.PageSize = 10;
                    pager.PageIndex = 1;

                    string sql = "select  *  FROM t_login ORDER BY ID ASC  LIMIT 10 OFFSET 10";
                    var dt = db.QueryTable(sql);

                    Models.T_Login model = new Models.T_Login();
                    OQL oql = OQL.From(model).Where(w => w.Compare(model.ID, ">", 2))
                        .OrderBy(o => o.Asc(model.Datetime))
                        .Select().End;

                    var list = EntityQuery<Models.T_Login>.Instance.QueryToList(oql, pager, db);

                    var data = db.GetDataBaseObject();

                    var columns = db.GetTableObject("t_login");

                    var dlist = db.GetDataBaseObject("u");

                    
                }
            }
        }
    }
}
