﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NORM.Demo
{
    public partial class PostgreSQLUpdateForm : Form
    {
        public PostgreSQLUpdateForm()
        {
            InitializeComponent();
        }

        private void PostgreSQLUpdateForm_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var ConnectionString = "User ID=postgres; Password=1234; Host=192.168.43.67; Port=5432; Database=mySpringboot; Pooling=true;";
            using (var db = NORM.DataBase.DataBaseFactory.Create(ConnectionString, "System.Data.PostgreSql"))
            {
                try
                {
                    //string message = "";
                    //db.TestConnect(out message);
                    //Console.WriteLine(message);

                    string sql = @"
SELECT id
    ,other_id
    ,title
    ,status
    ,date
FROM project 
WHERE id in ('my000000000000000000000000000000122','my000000000000000000000000000000123')
LIMIT 2
                    ";

                    var ds = db.QueryDataSet(CommandType.Text, sql, null);

                    
                    var dt = db.QueryTable(CommandType.Text, sql, null);

                    //ds.Tables[0].Rows[1]["title"] = "project11";

                    //Console.WriteLine(ds.Tables[0].Rows.Count.ToString());

                    //foreach (DataTable dt in ds.Tables)
                    //{
                    //    Console.WriteLine(dt.TableName);
                    //}

                    //db.UpdateDataSet(ds, "table");

                    this.dataGridView1.DataSource = ds;
                    this.dataGridView1.DataMember = "Table";

                    Console.WriteLine("查询成功");
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    MessageBox.Show(ex.Message);
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var ConnectionString = "User ID=postgres; Password=1234; Host=192.168.43.67; Port=5432; Database=mySpringboot; Pooling=true;";
            using (var db = NORM.DataBase.DataBaseFactory.Create(ConnectionString, "System.Data.PostgreSql"))
            {
                try
                {
                    var strSql = @"
SELECT id
    ,other_id
    ,title
    ,status
    to_char(date, 'yyyy-MM-dd HH24:mi:ss') as date
FROM project LIMIT 0
";  //,to_char(date, 'yyyy-MM-dd HH24:mi:ss') as date
                    var datasource = (DataSet)this.dataGridView1.DataSource;
                    db.UpdateDataSet(datasource, strSql);
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}
