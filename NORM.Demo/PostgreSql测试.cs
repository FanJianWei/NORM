﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace NORM.Demo
{
    public partial class PostgreSql测试 : Form
    {
        public PostgreSql测试()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string connectionString = "Server=192.168.43.67;Port=5432;User Id=postgres;Password=1234;Database=mySpringboot;CommandTimeout=0;";//ConnectionLifeTime=0;
            using (var db = NORM.DataBase.DataBaseFactory.Create(connectionString, NORM.DataBase.DataBaseTypes.PostgreSqlDataBase))
            {
                try
                {
                    string message = "";
                    db.TestConnect(out message);
                    MessageBox.Show(message);

                    string sql = @"
SELECT id
    ,other_id
    ,title
    ,status
    ,date
    ,create_by
    ,create_date
    ,update_by
    ,update_date
FROM project 
WHERE id=@id 
LIMIT 100 
";

                    NORM.DataBase.OsqlParameter[] parameters = new DataBase.OsqlParameter[] {
                         new DataBase.OsqlParameter("@status","02"),
                         new DataBase.OsqlParameter("@id","my000000000000000000000000000000126")
                    };

                    var dt = db.QueryTable(CommandType.Text, sql, parameters);

                    string sql2 = @"
update project set status=@status where id=@id 
";
                    var result = db.Execute(CommandType.Text, sql2, parameters);

                }
                catch(Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
    }
}
