﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using NORM.DataBase;
using NORM.Entity;
using NORM.SQLObject;

namespace NORM.Demo
{
    public partial class SQL数据库测试 : Form
    {
        public SQL数据库测试()
        {
            InitializeComponent();

            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                string sb = string.Empty;
                var db = NORM.DataBase.DataBaseFactory.Create("mssql");
                db = NORM.DataBase.DataBaseFactory.Create("Server=127.0.0.1;Initial Catalog=DevelopFramework_Base;Integrated Security=SSPI;", DataBaseTypes.SqlDataBase);
                 
                //Generator.BuildEntity(db, "T_SysLoginLog", "NORM.Models", out sb, DataBaseSchema.Table);
                Generator.BuildEntity(db, "V_MenuItem", "NORM", "V3", out sb);

                string strsql = "select count(*) from [T_Sys_MenuItemType]";
                var ds = db.QueryDataSet(CommandType.Text, strsql, null);

                var dt = db.GetDependencies("T_Sys_MenuItemType", 1);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void Query()
        {
            try
            {
                DateTime startTime = DateTime.Now;

                var db = NORM.DataBase.DataBaseFactory.Create("mssql");

                Models.T_SysLoginLog loginlog = new Models.T_SysLoginLog();
                OQL loginlogoql = new OQL(loginlog)
                    //.Where(w => w.Compare(loginlog.Status, "=", "Yes"))
                    .Select()
                    .OrderBy(o => o.Desc(loginlog.LoginDate))
                    //.Limit(6)
                    .End;

                var list = EntityQuery<Models.T_SysLoginLog>.Instance.QueryToList(loginlogoql, db);
                dataGridView1.DataSource = list;

                var li = EntityQuery<Models.T_SysLoginLog>.Instance.QueryToObject(loginlogoql, db);

                DateTime endTime = DateTime.Now;

                TimeSpan ts = endTime.Subtract(startTime);

                label1.Text = "共" + list.Count + "条记录 用时:" + ts.TotalMilliseconds * 1.0 / 1000 + "秒";

                db.Dispose();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            Query();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var db = NORM.DataBase.DataBaseFactory.Create("mssql");

            db.BeginTransaction();

            try
            {
                Models.T_SysLoginLog loginlog = new Models.T_SysLoginLog();

                loginlog.LoginMac = "23eab1-132c-4342de-34ebc";
                loginlog.LoginDate = DateTime.Now;
                loginlog.LoginIP = "127.0.0.1";
                loginlog.LoginAccount = "dong";
                loginlog.IsSecc = "1";
                loginlog.Status = "Yes";

                EntityQuery<Models.T_SysLoginLog>.Instance.Insert(loginlog, db);

                //int c = 0;
                //int a = 3 / c;

                db.Commit();

            }
            catch (Exception ex)
            {
                db.RollBack();
                MessageBox.Show(ex.Message);
            }

            Query();

        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (dataGridView1.SelectedRows.Count > 0)
            {
                var value = dataGridView1.SelectedRows[0].Cells[0].Value;
                if (value != null && value != DBNull.Value)
                {
                    var db = NORM.DataBase.DataBaseFactory.Create("mssql");

                    db.BeginTransaction();

                    try
                    {
                        Models.T_SysLoginLog loginlog = new Models.T_SysLoginLog();
                        OQL oql = OQL.From(loginlog).Where(w => w.Compare(loginlog.LoginLogID, "=", value))
                            .Select()
                            .Limit(23)
                            .End;

                        loginlog = EntityQuery<Models.T_SysLoginLog>.Instance.QueryToObject(oql, db);

                        loginlog.LoginAccount = "修改于" + DateTime.Now.ToString("MMddHHmm");
                        loginlog.LoginType = "登陆日志";
                        loginlog.LoginMac = "23eab1-132c-4342de-34ebc";

                        EntityQuery<Models.T_SysLoginLog>.Instance.Update(loginlog, db);


                        db.Commit();                       

                    }
                    catch (Exception ex)
                    {
                        db.RollBack();
                        MessageBox.Show(ex.Message);
                    }

                    Query();                   

                }
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            DateTime startTime = DateTime.Now;

            var db = DataBaseFactory.Create("mssql");

            try
            {
                Models.V_MenuItem model = new Models.V_MenuItem();
                OQL oql = new OQL(model).Where(w => w.Compare(model.IsShow, "=", 1))
                    .OrderBy(o => o.Desc(model.MenuItemID)
                        & o.Asc(model.MenuItemType))
                    .Select()
                    //.Limit(1)
                    .End;

                PageLimit pager = new PageLimit();
                pager.PageIndex = 1;
                pager.PageSize = 10;
                //var list = EntityQuery<Models.V_MenuItem>.QueryToList(oql, db);
                var list = EntityQuery<Models.V_MenuItem>.Instance.QueryToList(oql, pager, db);

                dataGridView1.DataSource = list;

                DateTime endTime = DateTime.Now;

                TimeSpan ts = endTime.Subtract(startTime);

                label1.Text = "每页"+pager.PageSize+"条  共 "+pager.PageCount + " 页, " + pager.RecordCount + "条记录 用时:" + ts.TotalMilliseconds * 1.0 / 1000 + "秒";

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
    }
}
