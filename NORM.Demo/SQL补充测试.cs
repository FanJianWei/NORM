﻿using NORM.Entity;
using NORM.SQLObject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace NORM.Demo
{
    public partial class SQL补充测试 : Form
    {
        public SQL补充测试()
        {
            InitializeComponent();
        }

        public NORM.DataBase.DataBase GetAdoHelper()
        {
            return NORM.DataBase.DataBaseFactory.Create("Server=192.168.0.116;Initial Catalog=CMSGroupSet;User ID=sa;Pwd=YHPych920,.;", NORM.DataBase.DataBaseTypes.SqlDataBase);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            PageLimit Pager = new PageLimit();
            List<Condition> Conditions = new List<Condition>();
            List<string> OrderBy = new List<string>();

            Pager.PageSize = 10;
            Pager.PageIndex = 1;

            Conditions.Add(new Condition() { Field = "DeleteSign", Comparison = "=", Value = 0 });

            OrderBy.Add("ID ASC");

            using (var db = GetAdoHelper())
            {
                Models.V_Role model = new Models.V_Role();
                OQL oql = OQL.From(model).Where(Conditions.ToArray()).OrderBy(OrderBy.ToArray()).Select().End;
                var data= db.QueryTable(oql, Pager);
            }
        }
    }
}
