﻿using NORM.DataBase;
using NORM.SQLObject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace NORM.Demo
{
    public partial class 修改对象 : Form
    {
        public 修改对象()
        {
            InitializeComponent();
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            var db = DataBaseFactory.Create();
            db.BeginTransaction();

            try
            {
                Models.T_Accounts model = EntityQuery<Models.T_Accounts>
              .Instance.QueryToModel(388,db);                

                if (model != null)
                {
                    model.Describ = "天天向上";
                    model.NoteCustomer = "修改于2014";
                    EntityQuery<Models.T_Accounts>.Instance.Update(model, db);                   
                }

                model = EntityQuery<Models.T_Accounts>
            .Instance.QueryToModel(388,db);

                db.Commit();
            }
            catch (Exception ex)
            {
                db.RollBack();
                MessageBox.Show(ex.Message);
            }         
            
        }
    }
}
