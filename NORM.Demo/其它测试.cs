﻿using NORM.SQLObject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using NORM.Entity;
using NORM.DataBase;
using System.Diagnostics;

namespace NORM.Demo
{
    public partial class 其它测试 : Form
    {       

        public 其它测试()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var db = NORM.DataBase.DataBaseFactory.Create();

            var context = DataBaseContext.Instance;

            Stopwatch sw = new Stopwatch();
            sw.Start();

            try
            {
                Models.T_Accounts model = new Models.T_Accounts();

                OQL oql = OQL.From(model).Where(new Condition[]{ 
                new Condition(){ Field="DeleteSign" , Comparison="=",Value=0 },
                new Condition(){ Field="Category" , Comparison="IN",Value=" (2,3,4) " },
                new Condition(){ Field="AccountTime" , Comparison=">=",Value=Convert.ToDateTime("2015-12-01") },
                new Condition(){ Field="AccountTime",Comparison="<=",Value=DateTime.Now ,Relation="AND" },
                new Condition(){ Field="EncryptSign",Comparison="IS NOT",Value=DataValue.DBNull }
            })
                .OrderBy(new string[] { "AccountTime ASC" })
                .Select()
                .End;

                var list = EntityQuery<Models.T_Accounts>.Instance.QueryToList(oql, db);

                Guid ui = Guid.NewGuid();

                //Models.T_Category category = new Models.T_Category();   
                //OQL oql2 = new OQL(model).LeftJoin(category).ON(model.Category, category.ID)
                //    .Where(model.DeleteSign, 0).And(model.Category, 2)
                //    .And(model.AccountTime, ">=", Convert.ToDateTime("2015-12-01"))
                //    .And(model.AccountTime, "<=", Convert.ToDateTime("2015-12-30"))
                //    .Select(delegate()
                //{
                //    return new List<TableField>() { 
                //       new TableField(){ Entity=model, Field ="AccountTime" },
                //       new TableField(){ Entity=model, Field ="DeleteSign" },
                //       new TableField(){ Entity=model, Field ="Describ" },
                //       new TableField(){ Entity=category, Field ="Describ" },
                //    };
                //})
                //    .End;

                //DataTable dt = db.QueryTable(oql2);

                //Models.T_Category category = new Models.T_Category();
                //OQL oql2 = new OQL(model).LeftJoin(category).ON(model.Category, category.ID)
                //    .Where(model.DeleteSign, 0)
                //    .AndIn(model.Category, "( 2,3,5,6 )")
                //    .And(model.AccountTime, ">=", Convert.ToDateTime("2015-12-01"))
                //    .And(model.AccountTime, "<=", Convert.ToDateTime("2015-12-30"))
                //    .And(model.EncryptSign, "IS NOT", DataValue.DBNull )
                //    .And(model.Describ, "IS NOT", DataValue.DBNull )
                //    .Select(delegate()
                //{
                //    return new List<TableField>() { 
                //       new TableField(){ Entity=model, Field ="AccountTime" },
                //       new TableField(){ Entity=model, Field ="DeleteSign" },
                //       new TableField(){ Entity=model, Field ="Describ" },
                //       new TableField(){ Entity=category, Field ="Describ" },
                //    };
                //})
                //    .End;

                Models.T_Category category = new Models.T_Category();
                OQL oql2 = new OQL(model).LeftJoin(category).ON(model.Category, category.ID)
                    .Where(model.DeleteSign, 0)
                    .AndIn(model.Category, "( 2,3,5,6 )")
                    .And(model.AccountTime, ">=", Convert.ToDateTime("2015-12-01"))
                    .And(model.AccountTime, "<=", Convert.ToDateTime("2015-12-30"))
                    .And(model.EncryptSign, "IS NOT", DataValue.DBNull)
                    .And(model.Describ, "IS NOT", DataValue.DBNull)
                    .Select(delegate()
                    {
                        return new List<TableField>() { 
                   new TableField(model){ Name ="AccountTime" },
                   new TableField(model){  Name ="DeleteSign" },
                   new TableField(model){ Name ="Describ" },
                   //category.toTableField("Describ"),
                   new TableField(category){ Name ="Describ" }
                };
                    })
                    .End;              

                OsqlParameter p = new OsqlParameter();

                OsqlParameter[] parameters = new OsqlParameter[]
                {
                    new OsqlParameter("@DeleteSign",0)
                };

                DataTable dt = context.QueryToTable("select * from T_Category where DeleteSign=@DeleteSign limit 12", parameters, db);
                var list2 = context.Query<Models.T_Category>("select * from T_Category limit 12", db);

                Models.T_Users user = db.QueryModel<Models.T_Users>(2);

                List<TableField> fiels = new List<TableField>();
                fiels.Add(new TableField(user) { Name = "MobilePhone", Value = "13000000000" });
                fiels.Add(new TableField(user) { Name = "Note", Value = "修改于" });

                OQL updateOql = OQL.Update(user).Set(fiels.ToArray())
                    .Where(w => w.Compare(user.UserID, "=", 2)).End;

                db.Execute(updateOql);

                sw.Reset();
                sw.Start();

                user.Note = "ddst";
                user.UserName = "wxdong ss";

                OQL updateOql2 =　OQL.Update(user)
                    .Set(()=> new { user.Status, 
                        user.UserName ,
                        user.Note })                         
                    .Where(w => w.Compare(user.UserID, "=", 1)).End;

                db.Execute(updateOql2);

                Models.T_Users user3 = EntityQuery<Models.T_Users>.Instance.QueryToModel(1, db);

                sw.Stop();
                TimeSpan ts2 = sw.Elapsed;
                var times = ts2.TotalMilliseconds;

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }            

        }
    }
}
