﻿using NORM.Entity;
using NORM.SQLObject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace NORM.Demo
{
    public partial class 分页显示数据 : Form
    {
        public 分页显示数据()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnApply_Click(object sender, EventArgs e)
        {
            var db = NORM.DataBase.DataBaseFactory.Create();

            Models.V_Accounts model = new Models.V_Accounts();

            List<string> fields = new List<string>();
            fields.Add("ID As AccountId");
            fields.Add("CreateBy");
            fields.Add("DeleteSign");
            fields.Add("MobilePhone");
            fields.Add("AccountTime");
            fields.Add("Describ");

            List<Condition> conditions = new List<Condition>();
            conditions.Add(new Condition() { Field = "DeleteSign", Value = 0, Comparison = "=" });
            //conditions.Add(new Condition() { Field = "AccountTime", Value = DateTime.Now.AddDays(-20), Comparison = ">=" });

            List<string> orderby = new List<string>();
            orderby.Add("AccountTime desc");

            //OQL oql = OQL.From(model).Select(fields.ToArray(),true)
            //    .Where(conditions.ToArray())
            //    .OrderBy(orderby.ToArray())
            //    .End;

            OQL oql = OQL.From(model)
               .Where(conditions.ToArray())
               .OrderBy(orderby.ToArray())
               .Select(fields.ToArray(), true)
               .End;

            PageLimit pager = new PageLimit();
            pager.PageIndex = 1;
            pager.PageSize = 5;
            var list = EntityQuery<Models.V_Accounts>.Instance.QueryToList(oql, pager, db); //db.QueryList<Models.V_Accounts>(oql, pager);
            //EntityQuery<Models.V_Accounts>.QueryToList(oql,pager, db);

            dataGridView1.DataSource = pager.DataSourceView;


        }
    }
}
