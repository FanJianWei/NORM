﻿using NORM.DataBase;
using NORM.SQLObject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using NORM.Entity;

namespace NORM.Demo
{
    public partial class 删除对象 : Form
    {
        public 删除对象()
        {
            InitializeComponent();
            DBind();
            dataGridView1.SelectionMode = DataGridViewSelectionMode.FullRowSelect;
        }

        private void DBind()
        {
            var db = NORM.DataBase.DataBaseFactory.Create();

            Models.T_Accounts model = new Models.T_Accounts();
            OQL oql = new OQL(model).Select().End;

            var list = EntityQuery<Models.T_Accounts>.Instance.QueryToList(oql, db);
            dataGridView1.DataSource = list;
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            var db = DataBaseFactory.Create();
            var context = DataBaseContext.Instance;

            db.BeginTransaction();
            try
            {
                Models.T_Accounts model = new Models.T_Accounts();
                model.ID = 140;
                model.AccountType = 1;

                OQL oql = OQL.Delete(model).Where(new Condition[] { new Condition() { Field = "ID", Value = 140 } }).End;

                //db.Delete<Models.T_Accounts>(model, db);
                //db.Delete<Models.T_Accounts>(model.ID, db);
                //EntityQuery<Models.T_Accounts>.Instance.Delete(oql, db);

                //db.Execute(oql);

                var id = this.dataGridView1.SelectedRows[0].Cells[0].Value;

                OQL oql2 = OQL.Delete(model).Where(w => w.Compare(model.ID, "=", id)).End;
                var d = context.Excute("delete from T_Accounts where ID='" + id + "'", db);



                //object Value = dataGridView1.SelectedRows[0].Cells[0].Value;

                //EntityQuery<Models.T_Accounts>.Instance.Delete(Value, db);

                ////Models.T_Accounts model = new Models.T_Accounts();
                //OQL deleteOql = new OQL(model).Delete().Where(w => w.Compare(model.Category,"IS",DataValue.DBNull)).End;
                //db.Execute(deleteOql);

                db.Commit();

                DBind();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
                db.RollBack();
            }          

        }
    }
}
