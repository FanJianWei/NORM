﻿using NORM.Common;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Net;
using System.Text;
using System.Windows.Forms;

namespace NORM.Demo
{
    public partial class 加密解密 : Form
    {
        public 加密解密()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string result = DEncrypt.HmacSha1("王mquabe_wxdong@1126.com", "wxdong2017");

            var datetimestr = NORM.Common.Network.ConvertTimestamp(DateTime.Parse("2288-05-29 10:45:59.231"), false);

            var datetimedt = NORM.Common.Network.ConvertDatetime(long.Parse(datetimestr), false);

            string datetimestr2 = datetimedt.ToString("yyyy-MM-dd HH:mm:ss.fff");

            string input_data = "20170530";
            byte[] input_crc_daa = System.Text.Encoding.ASCII.GetBytes(input_data);
            var result_crc = NORM.Common.Network.CRC32(input_crc_daa);
            string result_crc_str = result_crc.ToString("X2");
            
            int u = 367328153;  // 原始数据，8位16进制为15 E4 FB 99 大端
            byte[] bytes;       // u从低地址到高地址的四个字节

            // 获取
            bytes = System.BitConverter.GetBytes(u);

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < bytes.Length; i++)
            {
                sb.Append(bytes[i].ToString("X"));
            }

            result = sb.ToString(); //99 FB E4 15 小端

            int n1 = BitConverter.ToInt32(bytes, 0);


            Array.Reverse(bytes);

            StringBuilder sb2 = new StringBuilder();
            for (int i = 0; i < bytes.Length; i++)
            {
                sb2.Append(bytes[i].ToString("X"));
            }

            result = sb2.ToString(); //15 E4 FB 99 大端


            int n2 = BitConverter.ToInt32(bytes, 0);

            int z1 = IPAddress.HostToNetworkOrder(u); //本机到网络转换
            int z2 = IPAddress.NetworkToHostOrder(u);//网络字节转成本机

            long zk = reversebytes_uint64t(u * u);//1902234937


            string input = "yp jgh 一个测试";
            byte[] text_bytes = System.Text.Encoding.UTF8.GetBytes(input);

        }

        long reversebytes_int32(int value)
        {
            return (value & 0x000000FFU) << 24 | (value & 0x0000FF00U) << 8 |
                (value & 0x00FF0000U) >> 8 | (value & 0xFF000000U) >> 24;
        }

        // 先将64位的低32位转成小端模式，再将64位的高32位转成小端模式  
        // 在将原来的低32位放置到高32位，原来的高32位放置到低32位  
        long reversebytes_uint64t(long value)
        {
            long high_uint64 = (long)(reversebytes_int32((int)value));         // 低32位转成小端  
            long low_uint64 = (long)reversebytes_int32((int)(value >> 32));    // 高32位转成小端  
            return (high_uint64 << 32) + low_uint64;
        }  
    }
}
