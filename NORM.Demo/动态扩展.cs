﻿using NORM.SQLObject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using NORM.Entity;

namespace NORM.Demo
{
    public partial class 动态扩展 : Form
    {
        public 动态扩展()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var db = NORM.DataBase.DataBaseFactory.Create();

            Models.T_Users user = new Models.T_Users();          
            user.UserName = "wxd";

            OQL userOql = new OQL(user).Where(new Condition[] { user.produceItem<Condition>("UserName") })
                .Select().End;

            int departmentId = user.Department;

            var list = EntityQuery<Models.T_Users>.Instance.QueryToList(userOql, db);

        }
    }
}
