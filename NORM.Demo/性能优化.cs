﻿using NORM.DataBase;
using NORM.Entity;
using NORM.SQLObject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace NORM.Demo
{
    public partial class 性能优化 : Form
    {
        public 性能优化()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DateTime startTime = DateTime.Now;
            var db = DataBaseFactory.Create("mssql");

            button1.Enabled = false;

            db.BeginTransaction();

            try
            {
                //优化前 800-600 毫秒  分页 200毫秒

                Models.T_SysLoginLog model = new Models.T_SysLoginLog();

                OQL oql = new OQL(model)
                    //.Where(w => w.Compare(model.IsSecc, "=", 0))
                    .OrderBy(o => o.Asc(model.LoginDate))
                    .Select()
                    //.Limit(20000)
                    .End;


                //PageLimit pager = new PageLimit();
                //pager.PageIndex = 1000;
                //pager.PageSize = 100;
                //var list = EntityQuery<Models.T_SysLoginLog>.QueryToList(oql, pager, db);
                //dataGridView1.DataSource = list;

                var list = EntityQuery<Models.T_SysLoginLog>.Instance.QueryToList(oql, db);
                dataGridView1.DataSource = list;

                db.Commit();

                DateTime endTime = DateTime.Now;

                TimeSpan ts = endTime - startTime;
                double i = ts.TotalMilliseconds;

                label1.Text = "共"+oql.RecordCount +"行数据 "+" 共用时" + i.ToString("000,000,000,000.000") + "毫秒";

                textBox1.Text +="共"+oql.RecordCount +"行数据 "+ " 共用时" + i.ToString("000,000,000,000.000") + "毫秒" + "\r\n";

                label2.Text ="共"+oql.RecordCount +"行数据 "+ i / 1000 + "秒";

               

            }
            catch (Exception ex)
            {
                db.RollBack();
                MessageBox.Show(ex.Message);
            }            

            button1.Enabled = true;

        }
    }
}
