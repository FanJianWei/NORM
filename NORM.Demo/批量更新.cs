﻿using NORM.DataBase;
using NORM.SQLObject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace NORM.Demo
{
    public partial class 批量更新 : Form
    {
        public 批量更新()
        {
            InitializeComponent();
        }

        private void button2_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            using (var db = DataBaseFactory.Create("default"))
            {
                //db.BeginTransaction();
                try
                {
                    Models.T_Users model = new Models.T_Users();
                    OQL oql = new OQL(model).Select().Limit(0).End;
                    DataTable dt = (dataGridView1.DataSource as DataView).Table;
                    db.UpdateDataSet(dt.DataSet, oql.ToString());
                    //db.Commit();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                    //db.RollBack();
                }
            }    
            //using (var db = DataBaseFactory.Create("mssql"))
            //{
            //    db.BeginTransaction();
            //    try
            //    {
            //        Models.T_SysLoginLog model = new Models.T_SysLoginLog();
            //        OQL oql = new OQL(model).Select().End;
            //        DataTable dt = (dataGridView1.DataSource as DataView).Table;
            //        db.UpdateDataSet(dt.DataSet, oql.ToString());
            //        db.Commit();
            //    }
            //    catch (Exception ex)
            //    {
            //        MessageBox.Show(ex.Message);
            //        db.RollBack();
            //    }
            //}       
        }

        private void 批量更新_Load(object sender, EventArgs e)
        {
            using (var db = DataBaseFactory.Create("default"))
            {
                Models.T_Users model = new Models.T_Users();
                OQL oql = new OQL(model).Select().End;
                DataTable dt = db.QueryTable(oql);
                dataGridView1.DataSource = dt.DefaultView;
            }

            //using (var db = DataBaseFactory.Create("mssql"))
            //{
            //    Models.T_SysLoginLog model = new Models.T_SysLoginLog();
            //    OQL oql = new OQL(model).Select().End;
            //    DataTable dt = db.QueryTable(oql);
            //    dataGridView1.DataSource = dt.DefaultView;
            //}
        }
    }
}
