﻿using NORM.DataBase;
using NORM.SQLObject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using NORM.Entity;

namespace NORM.Demo
{
    public partial class 插入数据 : Form
    {
        public 插入数据()
        {
            InitializeComponent();
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            var db = DataBaseFactory.Create();
            db.BeginTransaction();

            try
            {
                Models.T_Accounts model = new Models.T_Accounts();
                model.Describ = "测试数据";
                model.YM = DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString("00");
                model.Category = "6";
                model.DeleteSign = 0;
                model.CreateDate = DateTime.Now;
                model.CreateBy = 0;
                model.Amount = 1212;
                model.AccountTime = DateTime.Now.ToString("yyyy-MM-dd");
                model.AccountType = 1;
                model.Department = 12;
                model.EncryptSign = 1;
                model.NoteCustomer = "测试数据4";

                EntityQuery<Models.T_Accounts>.Instance.Insert(model, db);

                OQL oql = OQL.From(model).Where(new Condition[] { 
                   new Condition() { Field ="NoteText",Value="测试数据4" }
                }).OrderBy(new string[] { "Department desc" })
             .Select()
             .Limit(1).End;

                Models.T_Accounts m = EntityQuery<Models.T_Accounts>.Instance.QueryToObject(oql, db);

                m.NoteCustomer = "323";

                EntityQuery<Models.T_Accounts>.Instance.Update(m, db);

                db.Commit();              

            }
            catch (Exception ex)
            {
                db.RollBack();
                MessageBox.Show(ex.Message);
            }
        }
    }
}
