﻿using NORM.Common;
using NORM.DataBase;
using NORM.SQLObject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace NORM.Demo
{
    public partial class 测试空对象 : Form
    {
        public 测试空对象()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            var db = DataBaseFactory.Default;

            try
            {
                Models.T_Accounts model = new Models.T_Accounts();
                OQL oql = OQL.From(model).Where(w => w.Compare(model.ID, "=", 0)).Select().End;//388

                model = EntityQuery<Models.T_Accounts>.Instance.QueryToObject(oql, db);

                string input = "System.Security.Cryptography.MD5CryptoServiceProvider check; <a>cms</a> 版本：v2016.01.20";

                string code = DEncrypt.Encrypt(input, "#@20!1451&");
                string text = DEncrypt.Decrypt(code, "#@20!1451&");

                var list = typeof(Models.T_Accounts).GetProperties(BindingFlags.Instance | BindingFlags.Public);
                var pi = typeof(Models.T_Accounts).GetProperty("Properties", BindingFlags.Instance | BindingFlags.Public);


            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }          

        }

       
    }
}
