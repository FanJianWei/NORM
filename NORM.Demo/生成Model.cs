﻿using NORM.SQLObject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using NORM.DataBase;

namespace NORM.Demo
{
    public partial class 生成Model : Form
    {
        public 生成Model()
        {
            InitializeComponent();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void button1_Click(object sender, EventArgs e)
        {
            try
            {               
                string sb = string.Empty;
                var db = DataBaseFactory.Create();
                Generator.BuildEntity(db, "T_Accounts", "NORM", "V3", out sb, DataBaseSchema.View);
                Generator.BuildModel(db, "T_Accounts", "NORM", "V3", out sb);
                Generator.BuildEntityDAL(db, "T_Accounts", "NORM", "V3", out sb);
                Generator.BuildEntityIDAL(db, "T_Accounts", "NORM", "V3", out sb);
                Generator.BuildEntityBLL(db, "T_Accounts", "NORM", "V3", out sb);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        }
    }
}
