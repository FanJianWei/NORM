﻿namespace NORM.Demo
{
    partial class 获取对象
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnGetEntity = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnGetEntity
            // 
            this.btnGetEntity.Location = new System.Drawing.Point(177, 202);
            this.btnGetEntity.Name = "btnGetEntity";
            this.btnGetEntity.Size = new System.Drawing.Size(75, 23);
            this.btnGetEntity.TabIndex = 0;
            this.btnGetEntity.Text = "button1";
            this.btnGetEntity.UseVisualStyleBackColor = true;
            this.btnGetEntity.Click += new System.EventHandler(this.btnGetEntity_Click);
            // 
            // 获取对象
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.btnGetEntity);
            this.Name = "获取对象";
            this.Text = "获取对象";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnGetEntity;
    }
}