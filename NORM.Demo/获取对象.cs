﻿using NORM.DataBase;
using NORM.SQLObject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;
using NORM.Entity;

namespace NORM.Demo
{
    public partial class 获取对象 : Form
    {
        public 获取对象()
        {
            InitializeComponent();
        }

        private void btnGetEntity_Click(object sender, EventArgs e)
        {
            var db = DataBaseFactory.Create();

            try
            {
                List<Condition> conditions = new List<Condition>();
                conditions.Add(new Condition() { Field = "Category", Value = 4 });

                Models.T_Accounts model = new Models.T_Accounts();
                OQL oql = OQL.From(model).Where(conditions.ToArray()).Select().End;

                OQL oql2 = OQL.From(model).Where(w => w.Compare(model.ID, "=", 12)).Select().End;//.Limit(1)


                model = db.QueryObject<Models.T_Accounts>(oql2);

                Models.T_Accounts m = EntityQuery<Models.T_Accounts>.Instance.QueryToModel(140, db);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }           
        }
    }
}
