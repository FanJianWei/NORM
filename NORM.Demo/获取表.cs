﻿using NORM.DataBase;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Text;
using System.Windows.Forms;

namespace NORM.Demo
{
    public partial class 获取表 : Form
    {
        public 获取表()
        {
            InitializeComponent();
        }

        private void 获取表_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var db = NORM.DataBase.DataBaseFactory.Create();
            string sql = "SELECT [ID] ,[Name] ,[Code] ,[Describ] ,[OrderIndex] ,[DeleteSign] ,[SelectedDown] FROM [T_Category] ";
            try
            {
                sql += "WHERE [Name] LIKE @Name ";

                OsqlParameter[] parameters ={
                     new OsqlParameter("@Name","交%")                         
                                                             };
                parameters[0].Value = "健%";
                parameters[0].Direction = ParameterDirection.Input;

                //this.dataGridView1.DataSource = db.QueryTable(sql);
                this.dataGridView1.DataSource = db.QueryTable(sql, parameters);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
           
        }
    }
}
