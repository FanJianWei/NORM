﻿
using NORM.Entity;
using NORM.SQLObject;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace NORM.Demo
{
    public partial class 起始页 : Form
    {
        public 起始页()
        {
            InitializeComponent();
        }
              
        private void btnApply_Click(object sender, EventArgs e)
        {
            //java:5d2fe75694f7a9e7bf77f9773bdfbfac
            //c#:5d2fe75694f7a9e7bf77f9773bdfbfac
            //绝对值 c#:5d2fe75694f7a9e7bf77f9773bdfbfac
            //tony84520

            string str = "tony84520";

            char[] charArray = str.ToCharArray();
            byte[] byteArray = Encoding.Default.GetBytes(str);
            byte[] _byteArray = new byte[charArray.Length];
            for (int i = 0; i < _byteArray.Length; i++)
            {
                _byteArray[i] = (byte)charArray[i];
            }

            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] output = md5.ComputeHash(_byteArray);

            StringBuilder sb = new StringBuilder();


            //int c = 170;
            //string xx = c.ToString("x");

            for (int i = 0; i < output.Length; i++)
            {
                int val = ((int)output[i]) & 0xff;
                if (val < 16)
                {
                    sb.Append("0");
                }
                //sb.Append(val.ToString("x"));
                sb.Append(Math.Abs(val).ToString("x"));                
            }

            string result = sb.ToString();
             
        }

        private void btnPager_Click(object sender, EventArgs e)
        {
            分页显示数据 form = new 分页显示数据();
            form.ShowDialog(this);
        }

        private void btnInsert_Click(object sender, EventArgs e)
        {
            插入数据 form = new 插入数据();
            form.ShowDialog(this);
        }

        private void btnGett_Click(object sender, EventArgs e)
        {
            获取对象 form = new 获取对象();
            form.ShowDialog(this);
        }

        private void btnUpdate_Click(object sender, EventArgs e)
        {
            修改对象 form = new 修改对象();
            form.ShowDialog(this);
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            删除对象 form = new 删除对象();
            form.ShowDialog(this);
        }

        private void btnAbse_Click(object sender, EventArgs e)
        {
            高级用法 form = new 高级用法();
            form.ShowDialog(this);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            生成Model form = new 生成Model();
            form.ShowDialog(this);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            其它测试 form = new 其它测试();
            form.ShowDialog(this);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            Lamada测试 form = new Lamada测试();
            form.ShowDialog(this);
        }

        private void button4_Click(object sender, EventArgs e)
        {
            NORM.Demo.SQL数据库测试 form = new Demo.SQL数据库测试();
            form.ShowDialog(this);
        }

        private void button5_Click(object sender, EventArgs e)
        {
            NORM.Demo.附加测试 form = new Demo.附加测试();
            form.ShowDialog(this);
        }

        private void button6_Click(object sender, EventArgs e)
        {
            测试空对象 form = new 测试空对象();
            form.ShowDialog(this);
        }

        private void button7_Click(object sender, EventArgs e)
        {
            性能优化 form = new 性能优化();
            form.ShowDialog(this);
        }

        private void button8_Click(object sender, EventArgs e)
        {
            批量更新 form = new 批量更新();
            form.ShowDialog(this);
            //格式化 form = new 格式化();
            //form.ShowDialog(this);
        }

        private void button9_Click(object sender, EventArgs e)
        {
            获取表 form = new 获取表();
            form.ShowDialog(this);
        }

        private void button10_Click(object sender, EventArgs e)
        {
            测试连接 form = new 测试连接();
            form.ShowDialog(this);
        }

        private void button11_Click(object sender, EventArgs e)
        {
            连接字符串 form = new 连接字符串();
            form.ShowDialog(this);
        }

        private void button12_Click(object sender, EventArgs e)
        {
            SQL补充测试 form = new SQL补充测试();
            form.ShowDialog(this);
        }

        private void button13_Click(object sender, EventArgs e)
        {
            加密解密 form = new 加密解密();
            form.ShowDialog(this);
        }

        private void button14_Click(object sender, EventArgs e)
        {
            创建数据库 form = new 创建数据库();
            form.ShowDialog(this);
        }

        private void button15_Click(object sender, EventArgs e)
        {
            SQL超时执行 form = new SQL超时执行();
            form.ShowDialog(this);
        }

        private void button16_Click(object sender, EventArgs e)
        {
            迸发测试 form = new 迸发测试();
            form.ShowDialog(this);
        }

        private void button17_Click(object sender, EventArgs e)
        {
            MySql数据库测试 form = new MySql数据库测试();
            form.ShowDialog(this);
        }

        private void button18_Click(object sender, EventArgs e)
        {
            Sqlite迸发测试 form = new Sqlite迸发测试();
            form.ShowDialog(this);
        }

        private void button19_Click(object sender, EventArgs e)
        {
            Sqlite迸发测试2 form = new Sqlite迸发测试2();
            form.ShowDialog(this);
        }

        private void button20_Click(object sender, EventArgs e)
        {
            PostgreSql测试 form = new PostgreSql测试();
            form.ShowDialog(this);
        }
    }
}
