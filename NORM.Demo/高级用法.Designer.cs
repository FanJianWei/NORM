﻿namespace NORM.Demo
{
    partial class 高级用法
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.btnApply = new System.Windows.Forms.Button();
            this.btnApply2 = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // btnApply
            // 
            this.btnApply.Location = new System.Drawing.Point(171, 74);
            this.btnApply.Name = "btnApply";
            this.btnApply.Size = new System.Drawing.Size(75, 23);
            this.btnApply.TabIndex = 0;
            this.btnApply.Text = "高级查询";
            this.btnApply.UseVisualStyleBackColor = true;
            this.btnApply.Click += new System.EventHandler(this.btnApply_Click);
            // 
            // btnApply2
            // 
            this.btnApply2.Location = new System.Drawing.Point(171, 133);
            this.btnApply2.Name = "btnApply2";
            this.btnApply2.Size = new System.Drawing.Size(75, 23);
            this.btnApply2.TabIndex = 1;
            this.btnApply2.Text = "多层嵌套查询";
            this.btnApply2.UseVisualStyleBackColor = true;
            this.btnApply2.Click += new System.EventHandler(this.btnApply2_Click);
            // 
            // 高级用法
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.btnApply2);
            this.Controls.Add(this.btnApply);
            this.Name = "高级用法";
            this.Text = "高级用法";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btnApply;
        private System.Windows.Forms.Button btnApply2;
    }
}