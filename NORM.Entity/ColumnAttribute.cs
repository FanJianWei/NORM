﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NORM.Entity
{
    public class ColumnAttribute : Attribute
    {
        private string _describ;
        public string Describ
        {
            get { return _describ; }
        }

        public string[] ColumnNames { get; set; }

        public ColumnAttribute(params string[] ColumnNames)
        {
            this.ColumnNames = ColumnNames;
            this._describ = "Name::" + string.Join(",", ColumnNames);
        }

        public override string ToString()
        {
            return this._describ;
        }
    }
}
