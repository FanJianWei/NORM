﻿using System;
using System.Collections.Generic;
using System.Text; 

namespace NORM.Entity
{
    /// <summary>
    /// 查询条件
    /// </summary>
    public class Condition  
    {
        private string _field;
        public string Field
        {
            set
            {
                _parametername = _field = value; 
            }
            get { return _field; }
        }

        private string _parametername;
        public string ParameterName
        {
            set { _parametername = value; }
            get { return _parametername; }
        }

        private string _comparison = "=";
        /// <summary>
        /// 比较符
        /// </summary>
        public string Comparison
        {
            set { _comparison = value; }
            get { return _comparison; }
        }

        private string _relation = "AND";
        /// <summary>
        /// 条件间的逻辑关系
        /// </summary>
        public string Relation
        {
            set { _join = _relation = value; }
            get { return _relation; }
        }

        private string _join;
        /// <summary>
        /// 条件间的逻辑关系
        /// </summary>
        public string Join
        {
            set { _relation = _join = value; }
            get { return _join; }
        }

        private object _value;
        /// <summary>
        /// 值
        /// </summary>
        public object Value
        {
            set { _value = value; }
            get { return _value; }
        }

        private Condition _subcondition;
        public Condition SubCondition
        {
            set { _subcondition = value; }
            get { return _subcondition; }
        }

        public Condition()
        {             
        }

        public Condition(string Field, string Comparison, object Value)
        {
            this.Field = Field;
            this.Comparison = Comparison;
            this.Value = Value;
        }

        public override string ToString()
        {
            string str = string.Empty;
            str = string.IsNullOrEmpty(_relation) ? "" : " " + _relation + " [" + _field + "] " + _comparison + " " + _value;
            return str;
        }

    } 
}
