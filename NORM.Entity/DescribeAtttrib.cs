﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NORM.Entity
{
    /// <summary>
    /// 属性描述
    /// </summary>
    [Serializable]
    public class DescribeAttribute : Attribute
    {
        private string _describ;
        /// <summary>
        /// Type::Sdt 短时间类型 如:yyyy-MM-dd
        /// Type::Ldt 长时间类型 如:yyyy-MM-dd HH:mm:ss.fff
        /// Type::Bit 布尔类型
        /// Type::Box Postgresql 中的一种类型
        /// </summary>
        public string Describ
        {
            get { return _describ; }
        }

        public DescribeAttribute(string Describ)
        {
            this._describ = Describ;
        }

        public override string ToString()
        {
            return this._describ;
        }
    }
}
