﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NORM.Entity
{
    public class EntityPropertyArgs : EventArgs
    {
        /// <summary>
        /// 字段名称
        /// </summary>
        public string Field { get; set; }

        /// <summary>
        /// 数据类型
        /// </summary>
        public Type Type { get; set; }

        /// <summary>
        /// 1：Get 2:Set
        /// </summary>
        public int PropertyArgsType { get; set; }
       
        public EntityPropertyArgs(string Field, Type Type, int PropertyArgsType)
        {
            this.Field = Field;
            this.Type = Type;
            this.PropertyArgsType = PropertyArgsType;
        }
    }
}
