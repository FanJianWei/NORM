﻿using System;
using System.Collections.Generic;
using System.Text;

namespace NORM.Entity
{
    public class FormatAttribute : Attribute
    {
        private string _describ;
        public string Describ
        {
            get { return _describ; }
        }

        public Type Type { get; set; }
        public string Formatting { get;set; }

        public FormatAttribute(Type type, string formatting)
        {
            this.Type = type;
            this.Formatting = formatting;
            this._describ = "Format::" + type.Name + "," + formatting;
        }

        public override string ToString()
        {
            return this._describ;
        }
    }
}
