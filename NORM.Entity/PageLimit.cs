﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Text;

namespace NORM.Entity
{
    /// <summary>
    /// 分页
    /// </summary>
    public class PageLimit
    {
        /// <summary>
        /// 当前页码 从1开始
        /// </summary>
        public int PageIndex { set; get; }
        /// <summary>
        /// 每页显示数
        /// </summary>
        public int PageSize { set; get; }
        /// <summary>
        /// 总页数
        /// </summary>
        public int PageCount { set; get; }
        /// <summary>
        /// 总计录数
        /// </summary>
        public int RecordCount { set; get; }
        /// <summary>
        /// 数据视图
        /// </summary>
        public DataView DataSourceView { set; get; }

        public PageLimit()
        {
            this.PageIndex = 1;
        }

        public PageLimit(Int32 pageSize, Int32 RecordCount)
            : this()
        {
            this.PageSize = pageSize;
            this.RecordCount = RecordCount;
            if (this.PageSize == 0)
                throw new DivideByZeroException("PageSize 不能为零");
            this.PageCount = (Int32)Math.Ceiling(this.RecordCount * 1.0d / this.PageSize);
        }
    }
}
