﻿using System;
using System.Collections.Generic;
using System.Text; 

namespace NORM.Entity
{
    public class TableField
    {
        private string _name = string.Empty;
        public string Name
        {
            get { return _name; }
            set { _name = value; }
        }

        private Type _type;
        public Type Type
        {
            get { return _type; }
            set { _type = value; }
        }

        private object _value;
        public object Value
        {
            set
            {                
                if (value != null)
                {
                    _type = value.GetType();
                }
                _value = value;
            }
            get { return _value; }
        }

        private EntityBase _entity;
        public EntityBase Entity
        {
            get { return _entity; }
            set { _entity = value; }
        }

        public TableField()
        {           
            this.Entity = new EntityBase();
        }

        public TableField(EntityBase Entity)
        {
            this.Entity = Entity;
        }

        public override string ToString()
        {
            return this.Entity.TableName + "." + this.Name;
        }


    }
}
