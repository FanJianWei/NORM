﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text; 
using System.Xml;

namespace NORM.UnitTest
{
    public class ConnectionStringFactory
    {
        private static object lockone = new object();
        private static DataBaseConnection conn = null;
        public static DataBaseConnection Default
        {
            get
            {
                if (conn == null)
                {
                    lock (lockone)
                    {
                        if (conn == null)
                        {
                            conn = new DataBaseConnection();
                            XmlDocument xmldoc = new XmlDocument();
                            xmldoc.Load(AppDomain.CurrentDomain.BaseDirectory + "NORM.UnitTest.exe.config");
                            var section = xmldoc.GetElementsByTagName("connectionStrings")[0].ChildNodes[0];
                            conn.ConnectionString = section.Attributes["connectionString"].Value;
                            conn.ProviderName= section.Attributes["providerName"].Value;
                        }
                    }
                }            
                
                return conn;
            }
        } 
    }

    public class DataBaseConnection
    {
        public string ConnectionString;
        public string ProviderName;
    }
}
