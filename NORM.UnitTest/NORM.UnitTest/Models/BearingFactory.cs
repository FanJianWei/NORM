﻿using NORM.Entity;
using System; 

namespace NORM.UnitTest.Models
{
    ///名称：BearingFactory
    ///作者：wxdong
    ///创建时间：2018-05-18 14:02 
    [Serializable]
    [Describe("Table")]
    public class BearingFactory : EntityBase
    {
        public BearingFactory()
        {
            TableName = "BearingFactory";
            PrimaryKey.Add("FactoryID");
        }
        #region Model
        /// <summary>
        /// 
        /// </summary>
        [Describe("Identify")]
        public int FactoryID
        {
            get { return getProperty<int>("FactoryID"); }
            set { setProperty("FactoryID", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string FactoryName
        {
            get { return getProperty<string>("FactoryName"); }
            set { setProperty("FactoryName", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Remark
        {
            get { return getProperty<string>("Remark"); }
            set { setProperty("Remark", value); }
        }
        #endregion Model
    }
}
