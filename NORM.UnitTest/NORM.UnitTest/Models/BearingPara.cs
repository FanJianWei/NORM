﻿using NORM.Entity;
using System;

namespace NORM.UnitTest.Models
{
    ///名称：BearingPara
    ///作者：wxdong
    ///创建时间：2018-05-18 12:47 
    [Serializable]
    [Describe("Table")]
    public class BearingPara : EntityBase
    {
        public BearingPara()
        {
            TableName = "BearingPara";
            PrimaryKey.Add("ModelID");
        }
        #region Model
        /// <summary>
        /// 
        /// </summary>
        [Describe("Identify")]
        public int ModelID
        {
            get { return getProperty<int>("ModelID"); }
            set { setProperty("ModelID", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string ModelName
        {
            get { return getProperty<string>("ModelName"); }
            set { setProperty("ModelName", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string FactoryName
        {
            get { return getProperty<string>("FactoryName"); }
            set { setProperty("FactoryName", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int NB
        {
            get { return getProperty<int>("NB"); }
            set { setProperty("NB", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal BPFO
        {
            get { return getProperty<decimal>("BPFO"); }
            set { setProperty("BPFO", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal BPFI
        {
            get { return getProperty<decimal>("BPFI"); }
            set { setProperty("BPFI", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal FTFI
        {
            get { return getProperty<decimal>("FTFI"); }
            set { setProperty("FTFI", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal BSF
        {
            get { return getProperty<decimal>("BSF"); }
            set { setProperty("BSF", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int ContactAngle
        {
            get { return getProperty<int>("ContactAngle"); }
            set { setProperty("ContactAngle", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal BallDiameter
        {
            get { return getProperty<decimal>("BallDiameter"); }
            set { setProperty("BallDiameter", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public decimal Pitch
        {
            get { return getProperty<decimal>("Pitch"); }
            set { setProperty("Pitch", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int FactoryID
        {
            get { return getProperty<int>("FactoryID"); }
            set { setProperty("FactoryID", value); }
        }
        #endregion Model
    }
}
