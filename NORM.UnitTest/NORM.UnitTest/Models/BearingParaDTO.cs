﻿using System;


namespace NORM.UnitTest.Models
{
    public class BearingParaDTO
    {
        #region Model
        private int _modelid;
        /// <summary>
        /// 
        /// </summary>
        public int ModelID
        {
            set
            {
                _modelid = value;
            }
            get
            {
                return _modelid;
            }
        }
        private string _modelname;
        /// <summary>
        /// 
        /// </summary>
        public string ModelName
        {
            set
            {
                _modelname = value;
            }
            get
            {
                return _modelname;
            }
        }
        private string _factoryname;
        /// <summary>
        /// 
        /// </summary>
        public string FactoryName
        {
            set
            {
                _factoryname = value;
            }
            get
            {
                return _factoryname;
            }
        }
        private int? _nb;
        /// <summary>
        /// 
        /// </summary>
        public int? NB
        {
            set
            {
                _nb = value;
            }
            get
            {
                return _nb;
            }
        }
        private decimal _bpfo;
        /// <summary>
        /// 
        /// </summary>
        public decimal BPFO
        {
            set
            {
                _bpfo = value;
            }
            get
            {
                return _bpfo;
            }
        }
        private decimal _bpfi;
        /// <summary>
        /// 
        /// </summary>
        public decimal BPFI
        {
            set
            {
                _bpfi = value;
            }
            get
            {
                return _bpfi;
            }
        }
        private decimal _ftfi;
        /// <summary>
        /// 
        /// </summary>
        public decimal FTFI
        {
            set
            {
                _ftfi = value;
            }
            get
            {
                return _ftfi;
            }
        }
        private decimal _bsf;
        /// <summary>
        /// 
        /// </summary>
        public decimal BSF
        {
            set
            {
                _bsf = value;
            }
            get
            {
                return _bsf;
            }
        }
        private int? _contactangle;
        /// <summary>
        /// 
        /// </summary>
        public int? ContactAngle
        {
            set
            {
                _contactangle = value;
            }
            get
            {
                return _contactangle;
            }
        }
        private decimal? _balldiameter;
        /// <summary>
        /// 
        /// </summary>
        public decimal? BallDiameter
        {
            set
            {
                _balldiameter = value;
            }
            get
            {
                return _balldiameter;
            }
        }
        private decimal? _pitch;
        /// <summary>
        /// 
        /// </summary>
        public decimal? Pitch
        {
            set
            {
                _pitch = value;
            }
            get
            {
                return _pitch;
            }
        }
        private int? _factoryid;
        /// <summary>
        /// 
        /// </summary>
        public int? FactoryID
        {
            set
            {
                _factoryid = value;
            }
            get
            {
                return _factoryid;
            }
        }
        #endregion Model
    } 
}
