﻿using NORM.Entity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NORM.UnitTest.Models
{
    ///名称：T_City
    ///作者：wxdong
    ///创建时间：2018-05-19 18:01 
    [Serializable]
    [Describe("Table")]
    public class T_City : EntityBase
    {
        public T_City()
        {
            TableName = "T_City";
            PrimaryKey.Add("ID");
        }
        #region Model
        /// <summary>
        /// 
        /// </summary>
        [Describe("Identify")]
        public int ID
        {
            get { return getProperty<int>("ID"); }
            set { setProperty("ID", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Code
        {
            get { return getProperty<string>("Code"); }
            set { setProperty("Code", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Name
        {
            get { return getProperty<string>("Name"); }
            set { setProperty("Name", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public int ProvinceId
        {
            get { return getProperty<int>("ProvinceId"); }
            set { setProperty("ProvinceId", value); }
        }
        /// <summary>
        /// 
        /// </summary>
        public string Describe
        {
            get { return getProperty<string>("Describe"); }
            set { setProperty("Describe", value); }
        }
        #endregion Model
    }
}
