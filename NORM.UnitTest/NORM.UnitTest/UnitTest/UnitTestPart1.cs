﻿ 
using NORM.DataBase;
using NORM.Entity;
using NORM.SQLObject;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NORM.UnitTest
{
    [TestFixture(Description ="NORM 查询测试")]
    public class UnitTestPart1
    {
        DataBaseContext context = DataBaseContext.Instance;


        [Test]
        public void QueryTest0()
        {
            using (var db = DataBaseFactory.Create(ConnectionStringFactory.Default.ConnectionString, ConnectionStringFactory.Default.ProviderName))
            {
                string message = string.Empty;
                db.TestConnect(out message);
            }
        }


        [Test]
        public void QueryTest1()
        {
            using (var db = DataBaseFactory.Create(ConnectionStringFactory.Default.ConnectionString, ConnectionStringFactory.Default.ProviderName))
            {
                string sql = "select ModelID,ModelName,NB,FTFI,BPFO,FactoryName from BearingPara where NB='36' and FactoryID='13' order by ModelID desc ";

                var data = context.QueryToTable(sql, db);

                Assert.Pass("查询：共 " + data.Rows.Count + " 条记录");
            }
        }

        [Test]
        public void QueryTest2()
        {
            using (var db = DataBaseFactory.Create(ConnectionStringFactory.Default.ConnectionString, ConnectionStringFactory.Default.ProviderName))
            {
                string sql = "select ModelID,ModelName,NB,FTFI,BPFO,FactoryName from BearingPara where NB='36' and FactoryID='13' order by ModelID desc ";

                var list = (List<Models.BearingPara>)context.Query<Models.BearingPara>(sql, db);

                Assert.Pass("查询：共 " + list.Count + " 条记录");
            }
        }

        [Test]
        public void QueryTest3()
        {
            using (var db = DataBaseFactory.Create(ConnectionStringFactory.Default.ConnectionString, ConnectionStringFactory.Default.ProviderName))
            {
                Models.BearingPara model = new Models.BearingPara();
                OQL oql1 = OQL.From(model).Where(w => w.Compare(model.NB, "=", 36)
                  && w.Compare(model.FactoryID, "=", 13))
                .OrderBy(p => p.Desc(model.ModelID))
                .Select(() => new
                {
                    model.ModelID,
                    model.ModelName,
                    model.NB,
                    model.FTFI,
                    model.BPFO,
                    model.FactoryName
                }).End;

                var list = EntityQuery<Models.BearingPara>.Instance.QueryToList(oql1, db); //context.Query<Models.BearingPara>(oql1, db);

                Assert.Pass("查询：共 " + list.Count + " 条记录");
            }
        }

        [Test]
        public void QueryTest4()
        {
            using (var db = DataBaseFactory.Create(ConnectionStringFactory.Default.ConnectionString, ConnectionStringFactory.Default.ProviderName))
            {
                Models.BearingPara model = new Models.BearingPara();

                string[] selects = new string[]{ "ModelID", "ModelName", "NB", "FTFI", "BPFO", "FactoryName" };
                Condition[] conditions = new Condition[] {
                    new Condition(){ Field="NB",Comparison="=",Value=36 },
                    new Condition(){ Relation="AND", Field="FactoryID",Comparison="=",Value=13 }
                };

                string[] orderbies = new string[] { "ModelID desc" };

                OQL oql1 = OQL.From(model).Where(conditions)
                .OrderBy(orderbies)
                .Select(selects).End;

                var list = EntityQuery<Models.BearingPara>.Instance.QueryToList(oql1, db); //context.Query<Models.BearingPara>(oql1, db);

                Assert.Pass("查询：共 " + list.Count + " 条记录");
            }
        }

        [Test]
        public void QueryTest5()
        {
            using (var db = DataBaseFactory.Create(ConnectionStringFactory.Default.ConnectionString, ConnectionStringFactory.Default.ProviderName))
            {
                PageLimit pager = new PageLimit();
                pager.PageIndex = 2;
                pager.PageSize = 20;

                Models.BearingPara model1 = new Models.BearingPara();
                Models.BearingFactory model2 = new Models.BearingFactory();

                OQL oql = OQL.From(model1)
                    .Join(model2).ON(model1.FactoryID, model2.FactoryID)
                    .Where(model1.FactoryID, "=", 1)
                    .And(model2.Remark, "IS", "NULL")
                    .Select(() => new
                    {
                        model1.ModelID,
                        model1.ModelName,
                        model1.NB,
                        model1.BPFI,
                        model2.FactoryName,
                        model2.Remark
                    }).End;

                var data = EntityQuery<T>.Instance.QueryToTable(oql, pager, db);

                var d = NORM.Common.DTLConvert<Models.BearingParaDTO>.ToList(data);

                var list = EntityQuery<T>.Instance.QueryToList<Models.BearingParaDTO>(oql, pager, db);

                Assert.Pass("查询：共 " + data.Rows.Count + " 条记录");
            }
        }

    }
}
