﻿using NORM.DataBase;
using NORM.SQLObject;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace NORM.UnitTest
{
    [TestFixture(Description = "NORM 非查询测试")]
    public class UnitTestPart2
    {
        [Test]
        public void InsertTest0()
        {
            using (var db = DataBaseFactory.Create(ConnectionStringFactory.Default.ConnectionString, ConnectionStringFactory.Default.ProviderName))
            {
                Models.BearingFactory model = new Models.BearingFactory();
                model.FactoryID = 0;
                model.FactoryName = "test1";
                model.Remark = "";
                EntityQuery<Models.BearingFactory>.Instance.Insert(model,db);
            }
        }

        [Test]
        public void InsertTest1()
        {
            using (var db = DataBaseFactory.Create(ConnectionStringFactory.Default.ConnectionString, ConnectionStringFactory.Default.ProviderName))
            {
                Models.BearingFactory model = new Models.BearingFactory();
                model.FactoryName = "wxdong";
                model.Remark = "1234";
                OQL oql = OQL.Insert(model).Values(() => new {
                     model.FactoryName,
                     model.Remark
                }).End;
                EntityQuery<Models.BearingFactory>.Instance.Excute(oql, db);
            }
        }

        [Test]
        public void InsertTest2()
        {
            using (var db = DataBaseFactory.Create(ConnectionStringFactory.Default.ConnectionString, ConnectionStringFactory.Default.ProviderName))
            {
                Models.BearingFactory model = new Models.BearingFactory();
                model.FactoryName = "wxdong";
                model.Remark = "1234";
                OQL oql = OQL.Insert(model).Values().End;
                EntityQuery<Models.BearingFactory>.Instance.Excute(oql, db);
            }
        }

        [Test]
        public void DeleteTest1()
        {
            using (var db = DataBaseFactory.Create(ConnectionStringFactory.Default.ConnectionString, ConnectionStringFactory.Default.ProviderName))
            {
                Models.BearingFactory model = new Models.BearingFactory();              
                OQL oql = OQL.Delete(model).Where(w => w.Compare(model.FactoryName, "=", "test1")).End;
                EntityQuery<Models.BearingFactory>.Instance.Excute(oql, db);
            }
        }

    }
}
