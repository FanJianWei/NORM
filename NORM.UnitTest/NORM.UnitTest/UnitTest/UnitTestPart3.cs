﻿using NORM.DataBase;
using NORM.Entity;
using NORM.SQLObject;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NORM.UnitTest
{
    [TestFixture(Description = "NORM 迸发测试")]
    public class UnitTestPart3
    {
        string ConnectionString = ConnectionStringFactory.Default.ConnectionString,
        ProviderName = ConnectionStringFactory.Default.ProviderName;

        private void Dotask(object threadId)
        {
            string connectString = "Server=(Local);Initial Catalog=DevelopFramework_Demo;User ID=sa;Pwd=123456;";
            string providerName="System.Data.Sql";
            using (var db = NORM.DataBase.DataBaseFactory.Create(connectString, providerName))
            {
                try
                {
                    Models.T_City model = new Models.T_City();

                    model.Code = "TestCode";
                    model.Name = "名称" + threadId;
                    model.ProvinceId = 3;
                    model.Describe = "测试" + threadId + "";

                    OQL oql = OQL.Insert(model).Values(() => new
                    {
                        model.Name,
                        model.Code,
                        model.ProvinceId,
                        model.Describe
                    }).End;

                    //EntityQuery<Models.BearingFactory>.Instance.Excute(oql, db);

                    //Console.WriteLine(threadId + " 执行成功");
                    //NORM.Logger.LoggerHelper.Debug(threadId + " 执行成功", "UnitTestPart3.Dotask");

                }
                catch (Exception ex)
                {
                    NORM.Logger.LoggerHelper.Error(threadId + "任务" + ex.Message, "UnitTestPart3.Dotask", ex.Source);
                }
            }          
        }

        private void Dotask2(object threadId)
        {
            //string connectString = "Server=(Local);Initial Catalog=DevelopFramework_Demo;User ID=sa;Pwd=123456;";
            //string providerName = "System.Data.Sql";
            using (var db = DataBaseFactory.Create(ConnectionString, ProviderName))
            {
                try
                {
                    PageLimit pager = new PageLimit();
                    pager.PageIndex = 2;
                    pager.PageSize = 20;

                    Models.BearingPara model1 = new Models.BearingPara();
                    Models.BearingFactory model2 = new Models.BearingFactory();

                    OQL oql = OQL.From(model1)
                        .Join(model2).ON(model1.FactoryID, model2.FactoryID)
                        .Where(model1.FactoryID, "=", 1)
                        .And(model2.Remark, "IS", "NULL")
                        //.OrderBy(O => O.Desc(model1.ModelID))
                        .Select(() => new
                        {
                            model1.ModelID,
                            model1.ModelName,
                            model1.NB,
                            model1.BPFI,
                            model2.FactoryName,
                            model2.Remark
                        }).End;

                    //var list = EntityQuery<T>.Instance.QueryToList<Models.BearingParaDTO>(oql, pager, db);

                    //NORM.Logger.LoggerHelper.Debug(threadId + " 执行成功", "UnitTestPart3.Dotask");

                }
                catch (Exception ex)
                {
                    NORM.Logger.LoggerHelper.Error(threadId + "任务" + ex.Message, "UnitTestPart3.Dotask", ex.Source);
                }
            }

        }


        [Test]
        public void ThreadsTest()
        {

            //Dotask2(1);

            System.Threading.Thread[] ths = new System.Threading.Thread[400];
            for (int i = 0; i < ths.Length; i++)
            {
                if (i % 2 == 0)
                {
                    ths[i] = new System.Threading.Thread(
                   new System.Threading.ParameterizedThreadStart(Dotask));
                }
                else
                {
                    ths[i] = new System.Threading.Thread(
                  new System.Threading.ParameterizedThreadStart(Dotask2));
                }
                ths[i].IsBackground = true;
            }

            Console.WriteLine("starting ...");

            for (int i = 0; i < ths.Length; i++)
            {
                ths[i].Start(i);
            }

            Console.ReadKey();

        }
    }
}
