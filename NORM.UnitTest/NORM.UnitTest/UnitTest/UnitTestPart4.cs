﻿using NUnit.Framework;
using System;
using System.Data;

namespace NORM.UnitTest
{
    [TestFixture(Description = "NORM DataSet Update 测试")]
    public class UnitTestPart4
    {
        [Test]
        public void DataSetUpdateTest()
        {
            var ConnectionString = ConnectionStringFactory.Default;
            using (var db = NORM.DataBase.DataBaseFactory.Create(ConnectionString.ConnectionString, ConnectionString.ProviderName))
            {
                try
                {
                    //string message = "";
                    //db.TestConnect(out message);
                    //Console.WriteLine(message);

                    string sql = @"
SELECT id
	,other_id
	,title
	,STATUS
	,DATE
FROM project LIMIT 100
                    ";

                    var ds = db.QueryDataSet(CommandType.Text, sql, null);

                    ds.Tables[0].Rows[1]["title"] = "project11";

                    Console.WriteLine(ds.Tables[0].Rows.Count.ToString());

                    foreach(DataTable dt in ds.Tables)
                    {
                        Console.WriteLine(dt.TableName);
                    }

                    db.UpdateDataSet(ds,"table");

                    Console.WriteLine("查询成功");
                }
                catch(Exception ex)
                {
                    Console.WriteLine(ex.Message);
                }
            }

            Console.ReadKey();
        }
    }
}
